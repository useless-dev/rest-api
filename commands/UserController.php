<?php 
	namespace app\commands;

	use yii\console\Controller;
	use yii\console\ExitCode;
	use app\models\User;
	/**
	 * 
	 */
	class UserController extends Controller
	{
		public function actionCreate()
		{
			$model = new User();
			$model->username = 'rootAPIs';
			$model->setPassword('pass<>M3tro');
			$model->generateAuthKey();
			$model->email = 'a@email.com';
			$model->status = 10;
			if ($model->save(false)) {
				echo "Succes";
			}
			else {
				echo "Faild";
			}
		}
	}
?>