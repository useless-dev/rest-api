<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormYudicium;

/**
 * FormYudiciumSearch represents the model behind the search form of `app\models\FormYudicium`.
 */
class FormYudiciumSearch extends FormYudicium
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdydsm'], 'integer'],
            [['npm', 'kdprodi', 'tahun', 'jns_semester', 'tgl_daftar', 'jam_daftar', 'tgl_lulus', 'judul_ta', 'ipk', 'predikat'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormYudicium::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdydsm' => $this->kdydsm,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'tgl_daftar', $this->tgl_daftar])
            ->andFilterWhere(['like', 'jam_daftar', $this->jam_daftar])
            ->andFilterWhere(['like', 'tgl_lulus', $this->tgl_lulus])
            ->andFilterWhere(['like', 'judul_ta', $this->judul_ta])
            ->andFilterWhere(['like', 'ipk', $this->ipk])
            ->andFilterWhere(['like', 'predikat', $this->predikat]);

        return $dataProvider;
    }
}
