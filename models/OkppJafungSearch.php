<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OkppJafung;

/**
 * OkppJafungSearch represents the model behind the search form of `app\models\OkppJafung`.
 */
class OkppJafungSearch extends OkppJafung
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['kddosen', 'jns_jafung', 'fotosm1', 'linkdrive', 'petugas', 'tgl', 'jam', 'ip', 'deteksi'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OkppJafung::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jam' => $this->jam,
        ]);

        $query->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'jns_jafung', $this->jns_jafung])
            ->andFilterWhere(['like', 'fotosm1', $this->fotosm1])
            ->andFilterWhere(['like', 'linkdrive', $this->linkdrive])
            ->andFilterWhere(['like', 'petugas', $this->petugas])
            ->andFilterWhere(['like', 'tgl', $this->tgl])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'deteksi', $this->deteksi]);

        return $dataProvider;
    }
}
