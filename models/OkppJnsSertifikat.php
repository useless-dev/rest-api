<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "okpp_jns_sertifikat".
 *
 * @property int $Id
 * @property string|null $namasertifikat
 */
class OkppJnsSertifikat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'okpp_jns_sertifikat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namasertifikat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'namasertifikat' => 'Namasertifikat',
        ];
    }
}
