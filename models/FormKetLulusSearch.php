<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormKetLulus;

/**
 * FormKetLulusSearch represents the model behind the search form of `app\models\FormKetLulus`.
 */
class FormKetLulusSearch extends FormKetLulus
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdlulus'], 'integer'],
            [['npm', 'nama', 'smt', 'kdjur', 'kdprodi', 'ipk', 'predikat', 'untuk_keperluan', 'tgl_lulus', 'no_surat', 'tgl_surat', 'tgl_daftar', 'tahun', 'jns_semester', 'thn', 'nama_waka1', 'nip_waka1'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKetLulus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdlulus' => $this->kdlulus,
            'tgl_lulus' => $this->tgl_lulus,
            'tgl_surat' => $this->tgl_surat,
            'tgl_daftar' => $this->tgl_daftar,
            'thn' => $this->thn,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'smt', $this->smt])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'ipk', $this->ipk])
            ->andFilterWhere(['like', 'predikat', $this->predikat])
            ->andFilterWhere(['like', 'untuk_keperluan', $this->untuk_keperluan])
            ->andFilterWhere(['like', 'no_surat', $this->no_surat])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'nama_waka1', $this->nama_waka1])
            ->andFilterWhere(['like', 'nip_waka1', $this->nip_waka1]);

        return $dataProvider;
    }
}
