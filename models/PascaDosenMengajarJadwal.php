<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_dosen_mengajar_jadwal".
 *
 * @property int $kdjadwal
 * @property string|null $kdmk
 * @property string|null $kurikulum
 * @property string|null $namamk
 * @property string|null $kddosen
 * @property string|null $namadosen
 * @property string|null $hari
 * @property string|null $jam_mulai
 * @property string|null $jam_selesai
 * @property string|null $tahun_akademik
 * @property string|null $jns_semester
 * @property string $kdprodi
 * @property string $kdkelas
 * @property string $kelas
 */
class PascaDosenMengajarJadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_dosen_mengajar_jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jam_mulai', 'jam_selesai'], 'safe'],
            [['kdmk', 'kurikulum', 'kddosen', 'hari', 'tahun_akademik', 'jns_semester'], 'string', 'max' => 35],
            [['namamk', 'namadosen'], 'string', 'max' => 100],
            [['kdprodi'], 'string', 'max' => 15],
            [['kdkelas', 'kelas'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdjadwal' => 'Kdjadwal',
            'kdmk' => 'Kdmk',
            'kurikulum' => 'Kurikulum',
            'namamk' => 'Namamk',
            'kddosen' => 'Kddosen',
            'namadosen' => 'Namadosen',
            'hari' => 'Hari',
            'jam_mulai' => 'Jam Mulai',
            'jam_selesai' => 'Jam Selesai',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'kdprodi' => 'Kdprodi',
            'kdkelas' => 'Kdkelas',
            'kelas' => 'Kelas',
        ];
    }
}
