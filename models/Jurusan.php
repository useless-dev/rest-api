<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jurusan".
 *
 * @property int $kdjur
 * @property string|null $namajur
 * @property string|null $kdfakultas
 */
class Jurusan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jurusan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namajur'], 'string', 'max' => 255],
            [['kdfakultas'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdjur' => 'Kdjur',
            'namajur' => 'Namajur',
            'kdfakultas' => 'Kdfakultas',
        ];
    }
}
