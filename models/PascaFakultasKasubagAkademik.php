<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_fakultas_kasubag_akademik".
 *
 * @property int $id
 * @property string|null $kdfakultas
 * @property string|null $tahun_akademik
 * @property string|null $jns_semester
 * @property string|null $kasubag
 * @property string|null $nip
 */
class PascaFakultasKasubagAkademik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_fakultas_kasubag_akademik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdfakultas'], 'string', 'max' => 35],
            [['tahun_akademik'], 'string', 'max' => 15],
            [['jns_semester'], 'string', 'max' => 10],
            [['kasubag'], 'string', 'max' => 100],
            [['nip'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kdfakultas' => 'Kdfakultas',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'kasubag' => 'Kasubag',
            'nip' => 'Nip',
        ];
    }
}
