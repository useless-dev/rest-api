<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dosen_mengajar_lama".
 *
 * @property int $kdmengajar
 * @property string|null $kddosen
 * @property string|null $kdmk
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property int|null $max_mhs
 * @property string|null $kdprodi
 * @property string|null $kelas
 */
class DosenMengajarLama extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dosen_mengajar_lama';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['max_mhs'], 'integer'],
            [['kddosen', 'kdmk'], 'string', 'max' => 30],
            [['tahun'], 'string', 'max' => 255],
            [['jns_semester', 'kdprodi'], 'string', 'max' => 20],
            [['kelas'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdmengajar' => 'Kdmengajar',
            'kddosen' => 'Kddosen',
            'kdmk' => 'Kdmk',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'max_mhs' => 'Max Mhs',
            'kdprodi' => 'Kdprodi',
            'kelas' => 'Kelas',
        ];
    }
}
