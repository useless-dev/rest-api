<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KrsKelas;

/**
 * KrsKelasSearch represents the model behind the search form of `app\models\KrsKelas`.
 */
class KrsKelasSearch extends KrsKelas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['krskelas', 'jumlah', 'kdfakultas'], 'integer'],
            [['kdmk', 'kdprodi', 'tahun', 'jns_semester', 'kelas'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KrsKelas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'krskelas' => $this->krskelas,
            'jumlah' => $this->jumlah,
            'kdfakultas' => $this->kdfakultas,
        ]);

        $query->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kelas', $this->kelas]);

        return $dataProvider;
    }
}
