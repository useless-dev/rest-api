<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fakultas".
 *
 * @property int $idjur
 * @property string|null $kdjur
 * @property string|null $namajur
 * @property string|null $kdfakultas
 * @property string|null $singkatan
 */
class Fakultas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fakultas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdjur'], 'string', 'max' => 5],
            [['namajur'], 'string', 'max' => 255],
            [['kdfakultas'], 'string', 'max' => 35],
            [['singkatan'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idjur' => 'Idjur',
            'kdjur' => 'Kdjur',
            'namajur' => 'Namajur',
            'kdfakultas' => 'Kdfakultas',
            'singkatan' => 'Singkatan',
        ];
    }
}
