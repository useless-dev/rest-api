<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa_prestasi".
 *
 * @property int $kdbeasiswa
 * @property string|null $npm
 * @property int|null $validasi
 * @property int|null $semester
 * @property string $jns_beasiswa
 * @property string $tahun
 * @property string $jns_semester
 * @property float|null $jumlah
 * @property string|null $tgl
 * @property string|null $wkt
 */
class MahasiswaPrestasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa_prestasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['validasi', 'semester'], 'integer'],
            [['jns_beasiswa', 'tahun', 'jns_semester'], 'required'],
            [['jumlah'], 'number'],
            [['tgl', 'wkt'], 'safe'],
            [['npm'], 'string', 'max' => 30],
            [['jns_beasiswa', 'tahun', 'jns_semester'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdbeasiswa' => 'Kdbeasiswa',
            'npm' => 'Npm',
            'validasi' => 'Validasi',
            'semester' => 'Semester',
            'jns_beasiswa' => 'Jns Beasiswa',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'jumlah' => 'Jumlah',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
        ];
    }
}
