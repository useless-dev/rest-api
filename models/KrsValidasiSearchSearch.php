<?php

namespace app;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KrsValidasiSearch;

/**
 * KrsValidasiSearchSearch represents the model behind the search form of `app\models\KrsValidasiSearch`.
 */
class KrsValidasiSearchSearch extends KrsValidasiSearch
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdkrsvalidasi', 'validasi'], 'integer'],
            [['npm', 'semester', 'jns_validasi', 'tahun', 'jns_semester', 'tgl', 'wkt', 'kdpa'], 'safe'],
            [['jumlah'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KrsValidasiSearch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdkrsvalidasi' => $this->kdkrsvalidasi,
            'validasi' => $this->validasi,
            'jumlah' => $this->jumlah,
            'tgl' => $this->tgl,
            'wkt' => $this->wkt,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'jns_validasi', $this->jns_validasi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kdpa', $this->kdpa]);

        return $dataProvider;
    }
}
