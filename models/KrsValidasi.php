<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs_validasi".
 *
 * @property int $kdkrsvalidasi
 * @property string|null $npm
 * @property int|null $validasi
 * @property string|null $semester
 * @property string $jns_validasi
 * @property string $tahun
 * @property string $jns_semester
 * @property float|null $jumlah
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string|null $kdpa
 */
class KrsValidasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs_validasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['validasi'], 'integer'],
            [['jns_validasi', 'tahun', 'jns_semester'], 'required'],
            [['jumlah'], 'number'],
            [['tgl', 'wkt'], 'safe'],
            [['npm'], 'string', 'max' => 30],
            [['semester'], 'string', 'max' => 10],
            [['jns_validasi', 'tahun', 'jns_semester'], 'string', 'max' => 20],
            [['kdpa'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkrsvalidasi' => 'Kdkrsvalidasi',
            'npm' => 'Npm',
            'validasi' => 'Validasi',
            'semester' => 'Semester',
            'jns_validasi' => 'Jns Validasi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'jumlah' => 'Jumlah',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'kdpa' => 'Kdpa',
        ];
    }
}
