<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs_status_antara".
 *
 * @property int $kdkrsstatus
 * @property string|null $npm
 * @property string|null $semester
 * @property string|null $sks
 * @property string|null $tahun
 * @property string|null $ip
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string $jns_semester
 */
class KrsStatusAntara extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs_status_antara';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jns_semester'], 'required'],
            [['npm', 'sks', 'ip'], 'string', 'max' => 30],
            [['semester', 'tahun', 'jns_semester'], 'string', 'max' => 20],
            [['tgl', 'wkt'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkrsstatus' => 'Kdkrsstatus',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'sks' => 'Sks',
            'tahun' => 'Tahun',
            'ip' => 'Ip',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
