<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kd_status_mhs".
 *
 * @property int $status_mhs
 * @property string|null $status_mahasiswa
 */
class KdStatusMhs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kd_status_mhs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_mahasiswa'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status_mhs' => 'Status Mhs',
            'status_mahasiswa' => 'Status Mahasiswa',
        ];
    }
}
