<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_dosen_mengajar_kelas".
 *
 * @property int $kdmengajarkelas
 * @property string|null $kddosen
 * @property string|null $kdmk
 * @property string|null $kdprodi
 * @property string|null $kelas
 * @property string|null $tahun
 * @property string $jns_semester
 */
class PascaDosenMengajarKelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_dosen_mengajar_kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jns_semester'], 'required'],
            [['kddosen', 'kdmk'], 'string', 'max' => 30],
            [['kdprodi', 'jns_semester'], 'string', 'max' => 20],
            [['kelas'], 'string', 'max' => 10],
            [['tahun'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdmengajarkelas' => 'Kdmengajarkelas',
            'kddosen' => 'Kddosen',
            'kdmk' => 'Kdmk',
            'kdprodi' => 'Kdprodi',
            'kelas' => 'Kelas',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
