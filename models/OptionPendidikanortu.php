<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "option_pendidikanortu".
 *
 * @property string $kdpenortu
 * @property string|null $pendidikan
 */
class OptionPendidikanortu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'option_pendidikanortu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdpenortu'], 'required'],
            [['kdpenortu'], 'string', 'max' => 20],
            [['pendidikan'], 'string', 'max' => 35],
            [['kdpenortu'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdpenortu' => 'Kdpenortu',
            'pendidikan' => 'Pendidikan',
        ];
    }
}
