<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emis_master_jenjang".
 *
 * @property int $kdjenjang
 * @property string|null $jenjang
 */
class EmisMasterJenjang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emis_master_jenjang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenjang'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdjenjang' => 'Kdjenjang',
            'jenjang' => 'Jenjang',
        ];
    }
}
