<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_skpi_capaian_id".
 *
 * @property int $kdakses
 * @property string $tahun_wisuda
 * @property string $periode_wisuda
 * @property string $tahun_akademik
 * @property string $jns_semester
 * @property string $kdfakultas
 * @property string $kdprodi
 * @property string|null $bidang_kerja
 * @property string|null $pengetahuan
 * @property string|null $sikap_khusus
 */
class FormSkpiCapaianId extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_skpi_capaian_id';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bidang_kerja', 'pengetahuan', 'sikap_khusus'], 'string'],
            [['tahun_wisuda'], 'string', 'max' => 10],
            [['periode_wisuda', 'kdfakultas'], 'string', 'max' => 5],
            [['tahun_akademik', 'jns_semester'], 'string', 'max' => 15],
            [['kdprodi'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'tahun_wisuda' => 'Tahun Wisuda',
            'periode_wisuda' => 'Periode Wisuda',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'kdfakultas' => 'Kdfakultas',
            'kdprodi' => 'Kdprodi',
            'bidang_kerja' => 'Bidang Kerja',
            'pengetahuan' => 'Pengetahuan',
            'sikap_khusus' => 'Sikap Khusus',
        ];
    }
}
