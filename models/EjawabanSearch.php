<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ejawaban;

/**
 * EjawabanSearch represents the model behind the search form of `app\models\Ejawaban`.
 */
class EjawabanSearch extends Ejawaban
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id'], 'integer'],
            [['id_group', 'group', 'semester', 'ans', 'idans', 'createdate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ejawaban::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Id' => $this->Id,
        ]);

        $query->andFilterWhere(['like', 'id_group', $this->id_group])
            ->andFilterWhere(['like', 'group', $this->group])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'ans', $this->ans])
            ->andFilterWhere(['like', 'idans', $this->idans])
            ->andFilterWhere(['like', 'createdate', $this->createdate]);

        return $dataProvider;
    }
}
