<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matakuliah_kurikulum".
 *
 * @property int $kdkurikulum
 * @property string|null $kurikulum
 */
class MatakuliahKurikulum extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matakuliah_kurikulum';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kurikulum'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkurikulum' => 'Kdkurikulum',
            'kurikulum' => 'Kurikulum',
        ];
    }
}
