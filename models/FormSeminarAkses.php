<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_seminar_akses".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string $thn_akademik
 * @property string $jns_semester
 * @property string $periode_bulan
 * @property string $kdjur
 * @property string $kdprodi
 */
class FormSeminarAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_seminar_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai'], 'safe'],
            [['thn_akademik'], 'string', 'max' => 30],
            [['jns_semester'], 'string', 'max' => 15],
            [['periode_bulan'], 'string', 'max' => 20],
            [['kdjur'], 'string', 'max' => 5],
            [['kdprodi'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'thn_akademik' => 'Thn Akademik',
            'jns_semester' => 'Jns Semester',
            'periode_bulan' => 'Periode Bulan',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
        ];
    }
}
