<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KdStatusMhs;

/**
 * KdStatusMhsSearch represents the model behind the search form of `app\models\KdStatusMhs`.
 */
class KdStatusMhsSearch extends KdStatusMhs
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_mhs'], 'integer'],
            [['status_mahasiswa'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KdStatusMhs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status_mhs' => $this->status_mhs,
        ]);

        $query->andFilterWhere(['like', 'status_mahasiswa', $this->status_mahasiswa]);

        return $dataProvider;
    }
}
