<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "batasan_perkelas".
 *
 * @property int $kdbatasan
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property int|null $jml
 * @property string|null $jns_semester
 * @property string|null $tahun
 * @property string|null $kdfakultas
 */
class BatasanPerkelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'batasan_perkelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jml'], 'integer'],
            [['kdjur', 'jns_semester'], 'string', 'max' => 20],
            [['kdprodi', 'tahun'], 'string', 'max' => 30],
            [['kdfakultas'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdbatasan' => 'Kdbatasan',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'jml' => 'Jml',
            'jns_semester' => 'Jns Semester',
            'tahun' => 'Tahun',
            'kdfakultas' => 'Kdfakultas',
        ];
    }
}
