<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edom".
 *
 * @property int $Id
 * @property string|null $npm
 * @property string|null $tahun
 * @property string|null $semester
 * @property string|null $kdmk
 * @property string|null $kdprodi
 * @property string|null $kddosen
 * @property string|null $jns_semester
 * @property string|null $kurikulum
 * @property string|null $groupid
 * @property string|null $pertanyaanid
 * @property string|null $jawaban
 * @property string|null $jawabanA
 * @property string|null $jawabanB
 * @property string|null $jawabanC
 * @property string|null $jawabanD
 * @property string|null $jawabanE
 */
class Edom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'edom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm', 'semester', 'kddosen', 'jns_semester'], 'string', 'max' => 20],
            [['tahun'], 'string', 'max' => 10],
            [['kdmk'], 'string', 'max' => 35],
            [['kdprodi', 'kurikulum'], 'string', 'max' => 50],
            [['groupid', 'pertanyaanid', 'jawaban', 'jawabanA', 'jawabanB', 'jawabanC', 'jawabanD', 'jawabanE'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'npm' => 'Npm',
            'tahun' => 'Tahun',
            'semester' => 'Semester',
            'kdmk' => 'Kdmk',
            'kdprodi' => 'Kdprodi',
            'kddosen' => 'Kddosen',
            'jns_semester' => 'Jns Semester',
            'kurikulum' => 'Kurikulum',
            'groupid' => 'Groupid',
            'pertanyaanid' => 'Pertanyaanid',
            'jawaban' => 'Jawaban',
            'jawabanA' => 'Jawaban A',
            'jawabanB' => 'Jawaban B',
            'jawabanC' => 'Jawaban C',
            'jawabanD' => 'Jawaban D',
            'jawabanE' => 'Jawaban E',
        ];
    }
}
