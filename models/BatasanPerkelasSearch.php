<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BatasanPerkelas;

/**
 * BatasanPerkelasSearch represents the model behind the search form of `app\models\BatasanPerkelas`.
 */
class BatasanPerkelasSearch extends BatasanPerkelas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdbatasan', 'jml'], 'integer'],
            [['kdjur', 'kdprodi', 'jns_semester', 'tahun', 'kdfakultas'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BatasanPerkelas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdbatasan' => $this->kdbatasan,
            'jml' => $this->jml,
        ]);

        $query->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas]);

        return $dataProvider;
    }
}
