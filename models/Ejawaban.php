<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ejawaban".
 *
 * @property int $Id
 * @property string|null $id_group
 * @property string|null $group
 * @property string|null $semester
 * @property string|null $ans
 * @property string|null $idans
 * @property string|null $createdate
 */
class Ejawaban extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ejawaban';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_group', 'group', 'semester'], 'string', 'max' => 20],
            [['ans', 'idans'], 'string', 'max' => 50],
            [['createdate'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'id_group' => 'Id Group',
            'group' => 'Group',
            'semester' => 'Semester',
            'ans' => 'Ans',
            'idans' => 'Idans',
            'createdate' => 'Createdate',
        ];
    }
}
