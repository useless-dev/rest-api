<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FakultasKabag;

/**
 * FakultasKabagSearch represents the model behind the search form of `app\models\FakultasKabag`.
 */
class FakultasKabagSearch extends FakultasKabag
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjur'], 'integer'],
            [['kdfakultas', 'namajur', 'tahun_akademik', 'jns_semester', 'kabag', 'kasubag1', 'kasubag2', 'kasubag3'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FakultasKabag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idjur' => $this->idjur,
        ]);

        $query->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas])
            ->andFilterWhere(['like', 'namajur', $this->namajur])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kabag', $this->kabag])
            ->andFilterWhere(['like', 'kasubag1', $this->kasubag1])
            ->andFilterWhere(['like', 'kasubag2', $this->kasubag2])
            ->andFilterWhere(['like', 'kasubag3', $this->kasubag3]);

        return $dataProvider;
    }
}
