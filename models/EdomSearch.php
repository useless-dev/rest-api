<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Edom;

/**
 * EdomSearch represents the model behind the search form of `app\models\Edom`.
 */
class EdomSearch extends Edom
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id'], 'integer'],
            [['npm', 'tahun', 'semester', 'kdmk', 'kdprodi', 'kddosen', 'jns_semester', 'kurikulum', 'groupid', 'pertanyaanid', 'jawaban', 'jawabanA', 'jawabanB', 'jawabanC', 'jawabanD', 'jawabanE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Edom::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Id' => $this->Id,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kurikulum', $this->kurikulum])
            ->andFilterWhere(['like', 'groupid', $this->groupid])
            ->andFilterWhere(['like', 'pertanyaanid', $this->pertanyaanid])
            ->andFilterWhere(['like', 'jawaban', $this->jawaban])
            ->andFilterWhere(['like', 'jawabanA', $this->jawabanA])
            ->andFilterWhere(['like', 'jawabanB', $this->jawabanB])
            ->andFilterWhere(['like', 'jawabanC', $this->jawabanC])
            ->andFilterWhere(['like', 'jawabanD', $this->jawabanD])
            ->andFilterWhere(['like', 'jawabanE', $this->jawabanE]);

        return $dataProvider;
    }
}
