<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_tagihan".
 *
 * @property int $kdtagihan
 * @property string|null $jenis_tagihan
 */
class JenisTagihan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_tagihan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_tagihan'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdtagihan' => 'Kdtagihan',
            'jenis_tagihan' => 'Jenis Tagihan',
        ];
    }
}
