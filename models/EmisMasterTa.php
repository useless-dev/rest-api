<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emis_master_ta".
 *
 * @property int $kdta
 * @property string|null $tahun_akademik
 */
class EmisMasterTa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emis_master_ta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tahun_akademik'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdta' => 'Kdta',
            'tahun_akademik' => 'Tahun Akademik',
        ];
    }
}
