<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BeasiswaDaftar;

/**
 * BeasiswaDaftarSearch represents the model behind the search form of `app\models\BeasiswaDaftar`.
 */
class BeasiswaDaftarSearch extends BeasiswaDaftar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kddaftar', 'kdbeasiswa', 'semester', 'jml_saudara'], 'integer'],
            [['kdjur', 'kdprodi', 'tahun', 'ipk', 'npm', 'thn_masuk', 'bekerja_dimana', 'bekerja_sebagai', 'status_mhs', 'beasiswa_jenis', 'beasiswa_semester', 'tgl', 'wkt', 'jns_semester'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BeasiswaDaftar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kddaftar' => $this->kddaftar,
            'kdbeasiswa' => $this->kdbeasiswa,
            'semester' => $this->semester,
            'jml_saudara' => $this->jml_saudara,
            'tgl' => $this->tgl,
            'wkt' => $this->wkt,
        ]);

        $query->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'ipk', $this->ipk])
            ->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'thn_masuk', $this->thn_masuk])
            ->andFilterWhere(['like', 'bekerja_dimana', $this->bekerja_dimana])
            ->andFilterWhere(['like', 'bekerja_sebagai', $this->bekerja_sebagai])
            ->andFilterWhere(['like', 'status_mhs', $this->status_mhs])
            ->andFilterWhere(['like', 'beasiswa_jenis', $this->beasiswa_jenis])
            ->andFilterWhere(['like', 'beasiswa_semester', $this->beasiswa_semester])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
