<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "okpp_jns_jafung".
 *
 * @property int $Id
 * @property string|null $namajafung
 */
class OkppJnsJafung extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'okpp_jns_jafung';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namajafung'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'namajafung' => 'Namajafung',
        ];
    }
}
