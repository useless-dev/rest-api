<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kd_propinsi".
 *
 * @property int $Id
 * @property string|null $kdprop
 * @property string|null $propinsi
 */
class KdPropinsi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kd_propinsi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdprop'], 'string', 'max' => 4],
            [['propinsi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'kdprop' => 'Kdprop',
            'propinsi' => 'Propinsi',
        ];
    }
}
