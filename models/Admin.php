<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin".
 *
 * @property int $no
 * @property string $username
 * @property string $password
 * @property string $sessions
 * @property string|null $level
 * @property string|null $nama
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'string', 'max' => 15],
            [['password', 'sessions'], 'string', 'max' => 50],
            [['level'], 'string', 'max' => 5],
            [['nama'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'username' => 'Username',
            'password' => 'Password',
            'sessions' => 'Sessions',
            'level' => 'Level',
            'nama' => 'Nama',
        ];
    }
}
