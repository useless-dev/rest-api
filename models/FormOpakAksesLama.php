<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_opak_akses_lama".
 *
 * @property int $kdakses
 * @property string $tahun
 * @property string $jns_semester
 * @property string $status
 */
class FormOpakAksesLama extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_opak_akses_lama';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tahun', 'jns_semester'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'status' => 'Status',
        ];
    }
}
