<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MahasiswaBeasiswa;

/**
 * MahasiswaBeasiswaSearch represents the model behind the search form of `app\models\MahasiswaBeasiswa`.
 */
class MahasiswaBeasiswaSearch extends MahasiswaBeasiswa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdprestasi', 'validasi', 'semester'], 'integer'],
            [['npm', 'jns_prestasi', 'tahun', 'jns_semester', 'foto', 'tgl', 'wkt'], 'safe'],
            [['juara'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MahasiswaBeasiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdprestasi' => $this->kdprestasi,
            'validasi' => $this->validasi,
            'semester' => $this->semester,
            'juara' => $this->juara,
            'tgl' => $this->tgl,
            'wkt' => $this->wkt,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'jns_prestasi', $this->jns_prestasi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }
}
