<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Nilai;

/**
 * NilaiSearch represents the model behind the search form of `app\models\Nilai`.
 */
class NilaiSearch extends Nilai
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdnilai', 'semester', 'mk_urutan', 'kdfakultas'], 'integer'],
            [['kddosen', 'npm', 'kdmk', 'kdprodi', 'sks', 'huruf', 'tahun', 'jns_semester', 'keterangan', 'tgl_input', 'waktu_input', 'petugas', 'tgl_khs'], 'safe'],
            [['angka', 'jumlah'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Nilai::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdnilai' => $this->kdnilai,
            'semester' => $this->semester,
            'angka' => $this->angka,
            'jumlah' => $this->jumlah,
            'mk_urutan' => $this->mk_urutan,
            'kdfakultas' => $this->kdfakultas,
        ]);

        $query->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'sks', $this->sks])
            ->andFilterWhere(['like', 'huruf', $this->huruf])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'tgl_input', $this->tgl_input])
            ->andFilterWhere(['like', 'waktu_input', $this->waktu_input])
            ->andFilterWhere(['like', 'petugas', $this->petugas])
            ->andFilterWhere(['like', 'tgl_khs', $this->tgl_khs]);

        return $dataProvider;
    }
}
