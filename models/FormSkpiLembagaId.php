<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_skpi_lembaga_id".
 *
 * @property int $kdakses
 * @property string $nama_pt
 * @property string $pendirian_pt
 * @property string $akreditasi_pt
 * @property string $akreditasi_pt_no
 * @property string $tahun_wisuda
 * @property string $periode_wisuda
 * @property string $tahun_akademik
 * @property string $jns_semester
 * @property string $kdfakultas
 * @property string $nama_fakultas
 * @property string $kdprodi
 * @property string|null $gelar
 * @property string $nama_prodi
 * @property string $akreditasi_prodi
 * @property string $akreditasi_prodi_no
 * @property string $jenis_pendidikan
 * @property string $prog_pendidikan
 * @property string $kkni_jenjang
 * @property string $syarat_penerimaan
 * @property string $bahasa_kuliah
 * @property string $sistem_penilaian
 * @property string $lama_studi
 * @property string $jenjang_pdd_lanjut
 * @property string $status_profesi
 * @property string $nama_jabatan_ttd
 * @property string $nama_pejabat_ttd
 * @property string $nip_pejabat_ttd
 * @property string $no_skpi
 * @property string $tgl_terbit
 */
class FormSkpiLembagaId extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_skpi_lembaga_id';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_pt', 'pendirian_pt', 'nama_fakultas', 'nama_prodi', 'jenis_pendidikan', 'prog_pendidikan', 'kkni_jenjang', 'syarat_penerimaan', 'bahasa_kuliah', 'sistem_penilaian', 'lama_studi', 'jenjang_pdd_lanjut', 'status_profesi', 'no_skpi', 'tgl_terbit'], 'string', 'max' => 250],
            [['akreditasi_pt', 'akreditasi_pt_no', 'akreditasi_prodi', 'akreditasi_prodi_no'], 'string', 'max' => 100],
            [['tahun_wisuda'], 'string', 'max' => 10],
            [['periode_wisuda', 'kdfakultas'], 'string', 'max' => 5],
            [['tahun_akademik', 'jns_semester'], 'string', 'max' => 15],
            [['kdprodi', 'nama_jabatan_ttd', 'nama_pejabat_ttd'], 'string', 'max' => 50],
            [['gelar'], 'string', 'max' => 255],
            [['nip_pejabat_ttd'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'nama_pt' => 'Nama Pt',
            'pendirian_pt' => 'Pendirian Pt',
            'akreditasi_pt' => 'Akreditasi Pt',
            'akreditasi_pt_no' => 'Akreditasi Pt No',
            'tahun_wisuda' => 'Tahun Wisuda',
            'periode_wisuda' => 'Periode Wisuda',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'kdfakultas' => 'Kdfakultas',
            'nama_fakultas' => 'Nama Fakultas',
            'kdprodi' => 'Kdprodi',
            'gelar' => 'Gelar',
            'nama_prodi' => 'Nama Prodi',
            'akreditasi_prodi' => 'Akreditasi Prodi',
            'akreditasi_prodi_no' => 'Akreditasi Prodi No',
            'jenis_pendidikan' => 'Jenis Pendidikan',
            'prog_pendidikan' => 'Prog Pendidikan',
            'kkni_jenjang' => 'Kkni Jenjang',
            'syarat_penerimaan' => 'Syarat Penerimaan',
            'bahasa_kuliah' => 'Bahasa Kuliah',
            'sistem_penilaian' => 'Sistem Penilaian',
            'lama_studi' => 'Lama Studi',
            'jenjang_pdd_lanjut' => 'Jenjang Pdd Lanjut',
            'status_profesi' => 'Status Profesi',
            'nama_jabatan_ttd' => 'Nama Jabatan Ttd',
            'nama_pejabat_ttd' => 'Nama Pejabat Ttd',
            'nip_pejabat_ttd' => 'Nip Pejabat Ttd',
            'no_skpi' => 'No Skpi',
            'tgl_terbit' => 'Tgl Terbit',
        ];
    }
}
