<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_beasiswa_daftar".
 *
 * @property int $kddaftar
 * @property int|null $kdbeasiswa
 * @property int|null $semester
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $tahun
 * @property string|null $ipk
 * @property string|null $npm
 * @property string|null $thn_masuk
 * @property string|null $bekerja_dimana
 * @property string|null $bekerja_sebagai
 * @property int|null $jml_saudara
 * @property string|null $status_mhs
 * @property string|null $beasiswa_jenis
 * @property string|null $beasiswa_semester
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string|null $jns_semester
 */
class PascaBeasiswaDaftar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_beasiswa_daftar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdbeasiswa', 'semester', 'jml_saudara'], 'integer'],
            [['tgl', 'wkt'], 'safe'],
            [['kdjur', 'kdprodi', 'ipk', 'thn_masuk'], 'string', 'max' => 30],
            [['tahun'], 'string', 'max' => 40],
            [['npm', 'beasiswa_semester'], 'string', 'max' => 20],
            [['bekerja_dimana', 'bekerja_sebagai', 'status_mhs'], 'string', 'max' => 100],
            [['beasiswa_jenis'], 'string', 'max' => 150],
            [['jns_semester'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kddaftar' => 'Kddaftar',
            'kdbeasiswa' => 'Kdbeasiswa',
            'semester' => 'Semester',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'ipk' => 'Ipk',
            'npm' => 'Npm',
            'thn_masuk' => 'Thn Masuk',
            'bekerja_dimana' => 'Bekerja Dimana',
            'bekerja_sebagai' => 'Bekerja Sebagai',
            'jml_saudara' => 'Jml Saudara',
            'status_mhs' => 'Status Mhs',
            'beasiswa_jenis' => 'Beasiswa Jenis',
            'beasiswa_semester' => 'Beasiswa Semester',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
