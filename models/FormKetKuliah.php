<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_ket_kuliah".
 *
 * @property int $kdketkuliah
 * @property string|null $jns_ket_kuliah
 * @property string|null $untuk_keperluan
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $smt
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $nama_ortu
 * @property string|null $nip_ortu
 * @property string|null $pangkat_gol_ortu
 * @property string|null $instansi_ortu
 * @property string|null $no_surat
 * @property string|null $tgl_surat
 * @property string|null $tgl_daftar
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $thn
 * @property string|null $nama_pejabat
 * @property string|null $nip_pejabat
 * @property string|null $pangkat_gol_pejabat
 */
class FormKetKuliah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_ket_kuliah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_surat', 'tgl_daftar', 'thn'], 'safe'],
            [['jns_ket_kuliah'], 'string', 'max' => 10],
            [['untuk_keperluan'], 'string', 'max' => 50],
            [['npm', 'kdjur', 'kdprodi', 'no_surat', 'nip_pejabat'], 'string', 'max' => 20],
            [['nama', 'smt', 'pangkat_gol_ortu'], 'string', 'max' => 35],
            [['nama_ortu'], 'string', 'max' => 100],
            [['nip_ortu'], 'string', 'max' => 25],
            [['instansi_ortu'], 'string', 'max' => 200],
            [['tahun'], 'string', 'max' => 9],
            [['jns_semester'], 'string', 'max' => 6],
            [['nama_pejabat'], 'string', 'max' => 30],
            [['pangkat_gol_pejabat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdketkuliah' => 'Kdketkuliah',
            'jns_ket_kuliah' => 'Jns Ket Kuliah',
            'untuk_keperluan' => 'Untuk Keperluan',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'smt' => 'Smt',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'nama_ortu' => 'Nama Ortu',
            'nip_ortu' => 'Nip Ortu',
            'pangkat_gol_ortu' => 'Pangkat Gol Ortu',
            'instansi_ortu' => 'Instansi Ortu',
            'no_surat' => 'No Surat',
            'tgl_surat' => 'Tgl Surat',
            'tgl_daftar' => 'Tgl Daftar',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'thn' => 'Thn',
            'nama_pejabat' => 'Nama Pejabat',
            'nip_pejabat' => 'Nip Pejabat',
            'pangkat_gol_pejabat' => 'Pangkat Gol Pejabat',
        ];
    }
}
