<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_beasiswa_semester".
 *
 * @property int $kdsemester
 * @property int|null $semester
 * @property int|null $status
 */
class PascaBeasiswaSemester extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_beasiswa_semester';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdsemester' => 'Kdsemester',
            'semester' => 'Semester',
            'status' => 'Status',
        ];
    }
}
