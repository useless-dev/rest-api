<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormSkpiLembagaId;

/**
 * FormSkpiLembagaIdSearch represents the model behind the search form of `app\models\FormSkpiLembagaId`.
 */
class FormSkpiLembagaIdSearch extends FormSkpiLembagaId
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdakses'], 'integer'],
            [['nama_pt', 'pendirian_pt', 'akreditasi_pt', 'akreditasi_pt_no', 'tahun_wisuda', 'periode_wisuda', 'tahun_akademik', 'jns_semester', 'kdfakultas', 'nama_fakultas', 'kdprodi', 'gelar', 'nama_prodi', 'akreditasi_prodi', 'akreditasi_prodi_no', 'jenis_pendidikan', 'prog_pendidikan', 'kkni_jenjang', 'syarat_penerimaan', 'bahasa_kuliah', 'sistem_penilaian', 'lama_studi', 'jenjang_pdd_lanjut', 'status_profesi', 'nama_jabatan_ttd', 'nama_pejabat_ttd', 'nip_pejabat_ttd', 'no_skpi', 'tgl_terbit'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormSkpiLembagaId::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdakses' => $this->kdakses,
        ]);

        $query->andFilterWhere(['like', 'nama_pt', $this->nama_pt])
            ->andFilterWhere(['like', 'pendirian_pt', $this->pendirian_pt])
            ->andFilterWhere(['like', 'akreditasi_pt', $this->akreditasi_pt])
            ->andFilterWhere(['like', 'akreditasi_pt_no', $this->akreditasi_pt_no])
            ->andFilterWhere(['like', 'tahun_wisuda', $this->tahun_wisuda])
            ->andFilterWhere(['like', 'periode_wisuda', $this->periode_wisuda])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas])
            ->andFilterWhere(['like', 'nama_fakultas', $this->nama_fakultas])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'gelar', $this->gelar])
            ->andFilterWhere(['like', 'nama_prodi', $this->nama_prodi])
            ->andFilterWhere(['like', 'akreditasi_prodi', $this->akreditasi_prodi])
            ->andFilterWhere(['like', 'akreditasi_prodi_no', $this->akreditasi_prodi_no])
            ->andFilterWhere(['like', 'jenis_pendidikan', $this->jenis_pendidikan])
            ->andFilterWhere(['like', 'prog_pendidikan', $this->prog_pendidikan])
            ->andFilterWhere(['like', 'kkni_jenjang', $this->kkni_jenjang])
            ->andFilterWhere(['like', 'syarat_penerimaan', $this->syarat_penerimaan])
            ->andFilterWhere(['like', 'bahasa_kuliah', $this->bahasa_kuliah])
            ->andFilterWhere(['like', 'sistem_penilaian', $this->sistem_penilaian])
            ->andFilterWhere(['like', 'lama_studi', $this->lama_studi])
            ->andFilterWhere(['like', 'jenjang_pdd_lanjut', $this->jenjang_pdd_lanjut])
            ->andFilterWhere(['like', 'status_profesi', $this->status_profesi])
            ->andFilterWhere(['like', 'nama_jabatan_ttd', $this->nama_jabatan_ttd])
            ->andFilterWhere(['like', 'nama_pejabat_ttd', $this->nama_pejabat_ttd])
            ->andFilterWhere(['like', 'nip_pejabat_ttd', $this->nip_pejabat_ttd])
            ->andFilterWhere(['like', 'no_skpi', $this->no_skpi])
            ->andFilterWhere(['like', 'tgl_terbit', $this->tgl_terbit]);

        return $dataProvider;
    }
}
