<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KdJalurPenerimaan;

/**
 * KdJalurPenerimaanSearch represents the model behind the search form of `app\models\KdJalurPenerimaan`.
 */
class KdJalurPenerimaanSearch extends KdJalurPenerimaan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdterima'], 'integer'],
            [['jalur_penerimaan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KdJalurPenerimaan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdterima' => $this->kdterima,
        ]);

        $query->andFilterWhere(['like', 'jalur_penerimaan', $this->jalur_penerimaan]);

        return $dataProvider;
    }
}
