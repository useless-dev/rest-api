<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ketua_jurusan".
 *
 * @property int $idjur
 * @property string|null $kdjur
 * @property string $sebutan
 * @property string|null $nama
 * @property string $nip
 * @property string|null $tahun
 * @property string $jns_semester
 */
class KetuaJurusan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ketua_jurusan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdjur', 'tahun'], 'string', 'max' => 20],
            [['sebutan'], 'string', 'max' => 100],
            [['nama'], 'string', 'max' => 50],
            [['nip'], 'string', 'max' => 30],
            [['jns_semester'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idjur' => 'Idjur',
            'kdjur' => 'Kdjur',
            'sebutan' => 'Sebutan',
            'nama' => 'Nama',
            'nip' => 'Nip',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
