<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmisMahasiswaRegulerAlumni;

/**
 * EmisMahasiswaRegulerAlumniSearch represents the model behind the search form of `app\models\EmisMahasiswaRegulerAlumni`.
 */
class EmisMahasiswaRegulerAlumniSearch extends EmisMahasiswaRegulerAlumni
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdmhs', 'semester'], 'integer'],
            [['tahun', 'jns_semester', 'kdjur', 'kdprodi', 'nama', 'npm', 'tmp_lahir', 'tgl_lahir', 'bln_lahir', 'thn_lahir', 'jenkel', 'asal_negara', 'asal_sekolah', 'jenjang_pendidikan', 'jml_sks', 'ijazah_no', 'ijazah_tgl', 'ijazah_bln', 'ijazah_thn', 'penghasilan_ortu', 'pekerjaan_ayah', 'pekerjaan_ibu', 'pendidikan_ayah', 'pendidikan_ibu', 'setelahlulus_status', 'setelahlulus_bekerja', 'setelahlulus_domisili'], 'safe'],
            [['ipk'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmisMahasiswaRegulerAlumni::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdmhs' => $this->kdmhs,
            'semester' => $this->semester,
            'ipk' => $this->ipk,
        ]);

        $query->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'tmp_lahir', $this->tmp_lahir])
            ->andFilterWhere(['like', 'tgl_lahir', $this->tgl_lahir])
            ->andFilterWhere(['like', 'bln_lahir', $this->bln_lahir])
            ->andFilterWhere(['like', 'thn_lahir', $this->thn_lahir])
            ->andFilterWhere(['like', 'jenkel', $this->jenkel])
            ->andFilterWhere(['like', 'asal_negara', $this->asal_negara])
            ->andFilterWhere(['like', 'asal_sekolah', $this->asal_sekolah])
            ->andFilterWhere(['like', 'jenjang_pendidikan', $this->jenjang_pendidikan])
            ->andFilterWhere(['like', 'jml_sks', $this->jml_sks])
            ->andFilterWhere(['like', 'ijazah_no', $this->ijazah_no])
            ->andFilterWhere(['like', 'ijazah_tgl', $this->ijazah_tgl])
            ->andFilterWhere(['like', 'ijazah_bln', $this->ijazah_bln])
            ->andFilterWhere(['like', 'ijazah_thn', $this->ijazah_thn])
            ->andFilterWhere(['like', 'penghasilan_ortu', $this->penghasilan_ortu])
            ->andFilterWhere(['like', 'pekerjaan_ayah', $this->pekerjaan_ayah])
            ->andFilterWhere(['like', 'pekerjaan_ibu', $this->pekerjaan_ibu])
            ->andFilterWhere(['like', 'pendidikan_ayah', $this->pendidikan_ayah])
            ->andFilterWhere(['like', 'pendidikan_ibu', $this->pendidikan_ibu])
            ->andFilterWhere(['like', 'setelahlulus_status', $this->setelahlulus_status])
            ->andFilterWhere(['like', 'setelahlulus_bekerja', $this->setelahlulus_bekerja])
            ->andFilterWhere(['like', 'setelahlulus_domisili', $this->setelahlulus_domisili]);

        return $dataProvider;
    }
}
