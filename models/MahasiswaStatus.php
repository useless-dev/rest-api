<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa_status".
 *
 * @property int $Id
 * @property string|null $npm
 * @property string|null $status_mhs
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $kdprodi
 * @property string|null $tgl_update
 * @property string|null $petugas
 */
class MahasiswaStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_update'], 'safe'],
            [['npm', 'tahun'], 'string', 'max' => 15],
            [['status_mhs'], 'string', 'max' => 1],
            [['jns_semester'], 'string', 'max' => 6],
            [['kdprodi'], 'string', 'max' => 20],
            [['petugas'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'npm' => 'Npm',
            'status_mhs' => 'Status Mhs',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'kdprodi' => 'Kdprodi',
            'tgl_update' => 'Tgl Update',
            'petugas' => 'Petugas',
        ];
    }
}
