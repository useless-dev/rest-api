<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DosenMengajarJadwal;

/**
 * DosenMengajarJadwalSearch represents the model behind the search form of `app\models\DosenMengajarJadwal`.
 */
class DosenMengajarJadwalSearch extends DosenMengajarJadwal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdjadwal', 'kdjam', 'kdfakultas'], 'integer'],
            [['kdmk', 'kurikulum', 'namamk', 'kddosen', 'namadosen', 'hari', 'jam_mulai', 'jam_selesai', 'tahun_akademik', 'jns_semester', 'kdprodi', 'kdkelas', 'kelas', 'koderuang', 'waktu', 'semester', 'sks'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DosenMengajarJadwal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdjadwal' => $this->kdjadwal,
            'jam_mulai' => $this->jam_mulai,
            'jam_selesai' => $this->jam_selesai,
            'kdjam' => $this->kdjam,
            'kdfakultas' => $this->kdfakultas,
        ]);

        $query->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'kurikulum', $this->kurikulum])
            ->andFilterWhere(['like', 'namamk', $this->namamk])
            ->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'namadosen', $this->namadosen])
            ->andFilterWhere(['like', 'hari', $this->hari])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'kdkelas', $this->kdkelas])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'koderuang', $this->koderuang])
            ->andFilterWhere(['like', 'waktu', $this->waktu])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'sks', $this->sks]);

        return $dataProvider;
    }
}
