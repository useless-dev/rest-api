<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kd_jalur_penerimaan".
 *
 * @property int $kdterima
 * @property string $jalur_penerimaan
 */
class KdJalurPenerimaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kd_jalur_penerimaan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jalur_penerimaan'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdterima' => 'Kdterima',
            'jalur_penerimaan' => 'Jalur Penerimaan',
        ];
    }
}
