<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs".
 *
 * @property int $kdkrs
 * @property string|null $npm
 * @property string|null $semester
 * @property string|null $kdmk
 * @property string|null $kdprodi
 * @property string|null $sks
 * @property string $keterangan
 * @property string|null $tahun
 * @property string $ipsebelumnya
 * @property string|null $ip
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string $kelas
 * @property string $jns_semester
 * @property string $kddosen
 * @property string|null $kurikulum
 * @property int|null $mk_urutan
 * @property int|null $kdfakultas
 */
class Krs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kelas', 'jns_semester', 'kddosen'], 'required'],
            [['mk_urutan', 'kdfakultas'], 'integer'],
            [['npm', 'ipsebelumnya', 'ip', 'kurikulum'], 'string', 'max' => 30],
            [['semester'], 'string', 'max' => 11],
            [['kdmk', 'keterangan'], 'string', 'max' => 35],
            [['kdprodi'], 'string', 'max' => 15],
            [['sks', 'tahun', 'jns_semester', 'kddosen'], 'string', 'max' => 20],
            [['tgl', 'wkt'], 'string', 'max' => 100],
            [['kelas'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkrs' => 'Kdkrs',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'kdmk' => 'Kdmk',
            'kdprodi' => 'Kdprodi',
            'sks' => 'Sks',
            'keterangan' => 'Keterangan',
            'tahun' => 'Tahun',
            'ipsebelumnya' => 'Ipsebelumnya',
            'ip' => 'Ip',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'kelas' => 'Kelas',
            'jns_semester' => 'Jns Semester',
            'kddosen' => 'Kddosen',
            'kurikulum' => 'Kurikulum',
            'mk_urutan' => 'Mk Urutan',
            'kdfakultas' => 'Kdfakultas',
        ];
    }

    public function getMatakuliah() {
        return $this->hasOne(Matakuliah::className(), ['kdmk' => 'kdmk','kdprodi'=>'kdprodi']);
    }

    public function getNilai() {
        return $this->hasOne(Nilai::className(), ['kdmk' => 'kdmk','npm'=>'npm','semester'=>'semester'])->orderBy(['nilai.jumlah'=>SORT_DESC]);

    }

    public function fields()
    {
        return [
            'kdkrs',
            'npm',
            'semester',
            'kdmk',
            'kdprodi',
            'sks',
            'keterangan',
            'tahun',
            'ipsebelumnya',
            'ip',
            'tgl',
            'wkt',
            'kelas',
            'jns_semester',
            'kddosen',
            'kurikulum',
            'mk_urutan',
            'kdfakultas',
            'matakuliah'=> function ($model)
            {
                if(!is_null($model->matakuliah))
                    return $model->matakuliah;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'nilai'=> function ($model)
            {
                if(!is_null($model->nilai))
                    return $model->nilai;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
        ];
    }
}
