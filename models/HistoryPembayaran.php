<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history_pembayaran".
 *
 * @property int $kdpembayaran
 * @property string|null $kdkeuangan
 * @property string|null $npm
 * @property string|null $kdtagihan
 * @property string|null $tgl_tagihan
 * @property string|null $tgl_bayar
 * @property string|null $jml_tagihan
 * @property string|null $denda
 * @property string|null $status
 * @property string|null $keterangan
 * @property string|null $file
 * @property string|null $status_hapus
 */
class HistoryPembayaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history_pembayaran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keterangan', 'status_hapus'], 'string'],
            [['kdkeuangan', 'npm', 'tgl_tagihan', 'tgl_bayar', 'jml_tagihan', 'denda'], 'string', 'max' => 30],
            [['kdtagihan'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 20],
            [['file'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdpembayaran' => 'Kdpembayaran',
            'kdkeuangan' => 'Kdkeuangan',
            'npm' => 'Npm',
            'kdtagihan' => 'Kdtagihan',
            'tgl_tagihan' => 'Tgl Tagihan',
            'tgl_bayar' => 'Tgl Bayar',
            'jml_tagihan' => 'Jml Tagihan',
            'denda' => 'Denda',
            'status' => 'Status',
            'keterangan' => 'Keterangan',
            'file' => 'File',
            'status_hapus' => 'Status Hapus',
        ];
    }

    public function getJenisTagihan()
    {
        return $this->hasOne(OptionPenghasilan::className(), ['kdtagihan' => 'kdtagihan']);
    }
    public function fields()
    {
        return [
            'kdpembayaran',
            'kdkeuangan',
            'npm',
            'kdtagihan',
            'tgl_tagihan',
            'tgl_bayar',
            'jml_tagihan',
            'denda',
            'status',
            'keterangan',
            'file',
            'status_hapus',
            'jenis_tagihan'=> function ($model)
            {
                if(isset($model->jenisTagihan))
                    return $model->jenisTagihan->jenis_tagihan;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
        ];
    }
}
