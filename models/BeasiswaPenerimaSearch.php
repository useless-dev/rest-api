<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BeasiswaPenerima;

/**
 * BeasiswaPenerimaSearch represents the model behind the search form of `app\models\BeasiswaPenerima`.
 */
class BeasiswaPenerimaSearch extends BeasiswaPenerima
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdpenerima'], 'integer'],
            [['npm', 'nama', 'kdbeasiswa', 'prodi', 'semester', 'tahun', 'no_sk', 'tgl_sk'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BeasiswaPenerima::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdpenerima' => $this->kdpenerima,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kdbeasiswa', $this->kdbeasiswa])
            ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'no_sk', $this->no_sk])
            ->andFilterWhere(['like', 'tgl_sk', $this->tgl_sk]);

        return $dataProvider;
    }
}
