<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kd_briva".
 *
 * @property int $kdbriva
 * @property string $kdprodi
 */
class KdBriva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kd_briva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdbriva'], 'required'],
            [['kdbriva'], 'integer'],
            [['kdprodi'], 'string', 'max' => 20],
            [['kdbriva'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdbriva' => 'Kdbriva',
            'kdprodi' => 'Kdprodi',
        ];
    }
}
