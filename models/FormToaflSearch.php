<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormToafl;

/**
 * FormToaflSearch represents the model behind the search form of `app\models\FormToafl`.
 */
class FormToaflSearch extends FormToafl
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['npm', 'semester', 'kdprodi', 'tahun', 'jns_semester', 'kelas', 'tgl_daftar', 'kddosen', 'nilai_1', 'nilai_2', 'nilai_3', 'nilai_akhir'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormToafl::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_daftar' => $this->tgl_daftar,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'nilai_1', $this->nilai_1])
            ->andFilterWhere(['like', 'nilai_2', $this->nilai_2])
            ->andFilterWhere(['like', 'nilai_3', $this->nilai_3])
            ->andFilterWhere(['like', 'nilai_akhir', $this->nilai_akhir]);

        return $dataProvider;
    }
}
