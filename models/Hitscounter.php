<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hitscounter".
 *
 * @property string $hits
 */
class Hitscounter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hitscounter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hits'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hits' => 'Hits',
        ];
    }
}
