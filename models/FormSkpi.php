<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_skpi".
 *
 * @property int $kdskpi
 * @property string $npm
 * @property string $kdjur
 * @property string $kdprodi
 * @property string $kategori
 * @property string $id_deskripsi
 * @property string $en_deskripsi
 * @property string $tingkat
 * @property string $tahun
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string $tahun_akademik
 * @property string $jns_semester
 * @property string $status
 * @property string $ket_status
 * @property string $file
 * @property string|null $tgl_update
 * @property string $user
 */
class FormSkpi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_skpi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_deskripsi', 'en_deskripsi'], 'required'],
            [['id_deskripsi', 'en_deskripsi'], 'string'],
            [['npm'], 'string', 'max' => 25],
            [['kdjur', 'kdprodi'], 'string', 'max' => 20],
            [['kategori', 'tahun_akademik', 'jns_semester', 'status', 'tgl_update'], 'string', 'max' => 50],
            [['tingkat'], 'string', 'max' => 100],
            [['tahun'], 'string', 'max' => 35],
            [['tgl', 'wkt'], 'string', 'max' => 15],
            [['ket_status', 'user'], 'string', 'max' => 255],
            [['file'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdskpi' => 'Kdskpi',
            'npm' => 'Npm',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'kategori' => 'Kategori',
            'id_deskripsi' => 'Id Deskripsi',
            'en_deskripsi' => 'En Deskripsi',
            'tingkat' => 'Tingkat',
            'tahun' => 'Tahun',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'status' => 'Status',
            'ket_status' => 'Ket Status',
            'file' => 'File',
            'tgl_update' => 'Tgl Update',
            'user' => 'User',
        ];
    }
}
