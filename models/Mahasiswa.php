<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property string $npm
 * @property string|null $thn_masuk
 * @property string|null $status_mhs
 * @property string|null $status_mhs2
 * @property string|null $tgl_status2
 * @property string|null $thn_lulus
 * @property string|null $nosurat
 * @property string|null $notrn
 * @property string|null $username
 * @property string|null $password
 * @property string|null $sessions
 * @property string|null $nama
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $tmp_lahir
 * @property string|null $tgl_lahir
 * @property string|null $jenkel
 * @property string|null $agama
 * @property string|null $status_kerja
 * @property string|null $status_perkawinan
 * @property string|null $warga_negara
 * @property string|null $alamat_asal
 * @property string $jalan
 * @property string $rt
 * @property string $rw
 * @property string $propinsi
 * @property string $kabupaten
 * @property string $kecamatan
 * @property string $kelurahan
 * @property string|null $notelp
 * @property string|null $notelpinternet
 * @property string|null $kode_pos
 * @property string|null $npsn
 * @property string|null $asal_sekolah
 * @property string|null $nama_sekolah
 * @property string|null $alamat_sekolah
 * @property string|null $ijazah_tahun
 * @property string|null $ijazah_no
 * @property string|null $foto
 * @property string|null $nama_ayah
 * @property string|null $tmp_lahir_ayah
 * @property string|null $tgl_lahir_ayah
 * @property string|null $pendidikan_ayah
 * @property string|null $pekerjaan_ayah
 * @property string|null $penghasilan_ayah
 * @property string|null $nama_ibu
 * @property string|null $pendidikan_ibu
 * @property string|null $pekerjaan_ibu
 * @property string|null $penghasilan_ibu
 * @property string|null $alamat_ortu
 * @property string|null $notelp_ortu
 * @property string $jalur_penerimaan
 * @property int|null $kdpa
 * @property string $kelas
 * @property string|null $nik
 * @property string|null $nisn
 * @property int|null $kdfakultas
 * @property string|null $tgl_masuk
 * @property string|null $pin
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm', 'kelas'], 'required'],
            [['kdpa', 'kdfakultas'], 'integer'],
            [['tgl_masuk'], 'safe'],
            [['npm'], 'string', 'max' => 11],
            [['thn_masuk', 'thn_lulus', 'nosurat', 'notrn', 'alamat_asal', 'alamat_sekolah', 'foto', 'alamat_ortu', 'pin'], 'string', 'max' => 255],
            [['status_mhs', 'status_mhs2', 'agama', 'status_kerja', 'status_perkawinan', 'kode_pos', 'asal_sekolah', 'tgl_lahir_ayah', 'penghasilan_ibu', 'notelp_ortu', 'kelas'], 'string', 'max' => 20],
            [['tgl_status2', 'tgl_lahir', 'jenkel', 'warga_negara', 'pendidikan_ayah', 'pekerjaan_ayah', 'penghasilan_ayah', 'jalur_penerimaan'], 'string', 'max' => 30],
            [['username', 'password', 'sessions', 'kdprodi', 'propinsi', 'kabupaten', 'kecamatan', 'kelurahan', 'nama_ayah', 'nama_ibu', 'pendidikan_ibu', 'pekerjaan_ibu'], 'string', 'max' => 50],
            [['nama', 'tmp_lahir', 'jalan', 'tmp_lahir_ayah'], 'string', 'max' => 100],
            [['kdjur', 'notelp', 'notelpinternet', 'nik', 'nisn'], 'string', 'max' => 35],
            [['rt', 'rw'], 'string', 'max' => 10],
            [['npsn'], 'string', 'max' => 15],
            [['nama_sekolah', 'ijazah_no'], 'string', 'max' => 40],
            [['ijazah_tahun'], 'string', 'max' => 4],
            [['npm'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'npm' => 'Npm',
            'thn_masuk' => 'Thn Masuk',
            'status_mhs' => 'Status Mhs',
            'status_mhs2' => 'Status Mhs 2',
            'tgl_status2' => 'Tgl Status 2',
            'thn_lulus' => 'Thn Lulus',
            'nosurat' => 'Nosurat',
            'notrn' => 'Notrn',
            'username' => 'Username',
            'password' => 'Password',
            'sessions' => 'Sessions',
            'nama' => 'Nama',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'tmp_lahir' => 'Tmp Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'jenkel' => 'Jenkel',
            'agama' => 'Agama',
            'status_kerja' => 'Status Kerja',
            'status_perkawinan' => 'Status Perkawinan',
            'warga_negara' => 'Warga Negara',
            'alamat_asal' => 'Alamat Asal',
            'jalan' => 'Jalan',
            'rt' => 'Rt',
            'rw' => 'Rw',
            'propinsi' => 'Propinsi',
            'kabupaten' => 'Kabupaten',
            'kecamatan' => 'Kecamatan',
            'kelurahan' => 'Kelurahan',
            'notelp' => 'Notelp',
            'notelpinternet' => 'Notelpinternet',
            'kode_pos' => 'Kode Pos',
            'npsn' => 'Npsn',
            'asal_sekolah' => 'Asal Sekolah',
            'nama_sekolah' => 'Nama Sekolah',
            'alamat_sekolah' => 'Alamat Sekolah',
            'ijazah_tahun' => 'Ijazah Tahun',
            'ijazah_no' => 'Ijazah No',
            'foto' => 'Foto',
            'nama_ayah' => 'Nama Ayah',
            'tmp_lahir_ayah' => 'Tmp Lahir Ayah',
            'tgl_lahir_ayah' => 'Tgl Lahir Ayah',
            'pendidikan_ayah' => 'Pendidikan Ayah',
            'pekerjaan_ayah' => 'Pekerjaan Ayah',
            'penghasilan_ayah' => 'Penghasilan Ayah',
            'nama_ibu' => 'Nama Ibu',
            'pendidikan_ibu' => 'Pendidikan Ibu',
            'pekerjaan_ibu' => 'Pekerjaan Ibu',
            'penghasilan_ibu' => 'Penghasilan Ibu',
            'alamat_ortu' => 'Alamat Ortu',
            'notelp_ortu' => 'Notelp Ortu',
            'jalur_penerimaan' => 'Jalur Penerimaan',
            'kdpa' => 'Kdpa',
            'kelas' => 'Kelas',
            'nik' => 'Nik',
            'nisn' => 'Nisn',
            'kdfakultas' => 'Kdfakultas',
            'tgl_masuk' => 'Tgl Masuk',
            'pin' => 'Pin',
        ];
    }

    public function getJurusan()
    {
        return $this->hasOne(Jurusan::className(), ['kdjur' => 'kdjur']);
    }

    public function getProdi()
    {
        return $this->hasOne(Prodi::className(), ['kdprodi' => 'kdprodi']);
    }

    public function getKrsValidasi()
    {
        return $this->hasMany(KrsValidasi::className(), ['npm' => 'npm']);
    }
    public function getJalurMasuk()
    {
        return $this->hasOne(KdJalurPenerimaan::className(), ['kdterima' => 'jalur_penerimaan']);
    }
    public function getPendidikanIbu()
    {
        return $this->hasOne(OptionPendidikanortu::className(), ['kdpenortu' => 'pendidikan_ibu']);
    }
    public function getPendidikanAyah()
    {
        return $this->hasOne(OptionPendidikanortu::className(), ['kdpenortu' => 'pendidikan_ayah']);
    }
    public function getPekerjaanIbu()
    {
        return $this->hasOne(OptionPekerjaan::className(), ['kdpekerjaan' => 'pekerjaan_ibu']);
    }
    public function getPekerjaanAyah()
    {
        return $this->hasOne(OptionPekerjaan::className(), ['kdpekerjaan' => 'pekerjaan_ayah']);
    }
    public function getPenghasilanIbu()
    {
        return $this->hasOne(OptionPenghasilan::className(), ['kdpenghasilan' => 'penghasilan_ibu']);
    }
    public function getPenghasilanAyah()
    {
        return $this->hasOne(OptionPenghasilan::className(), ['kdpenghasilan' => 'penghasilan_ayah']);
    }
    public function getDosenPa()
    {
        return $this->hasOne(Dosen::className(), ['kddosen' => 'kdpa']);
    }
    public function getKasubagAkaFak()
    {
        return $this->hasOne(FakultasKasubagAkademik::className(), ['kdfakultas' => 'kdfakultas']);
    }
    public function fields()
    {
        return [
            'npm' ,
            'thn_masuk' ,
            'status_mhs' ,
            'status_mhs2' ,
            'tgl_status2' ,
            'thn_lulus' ,
            'nosurat' ,
            'notrn' ,
            'username' ,
            'password' ,
            'sessions' ,
            'nama' ,
            'kdjur' ,
            'kdprodi' ,
            'tmp_lahir' ,
            'tgl_lahir' ,
            'jenkel' ,
            'agama' ,
            'status_kerja' ,
            'status_perkawinan' ,
            'warga_negara' ,
            'alamat_asal' ,
            'jalan' ,
            'rt' ,
            'rw' ,
            'propinsi' ,
            'kabupaten' ,
            'kecamatan' ,
            'kelurahan' ,
            'notelp' ,
            'notelpinternet' ,
            'kode_pos' ,
            'npsn' ,
            'asal_sekolah' ,
            'nama_sekolah' ,
            'alamat_sekolah' ,
            'ijazah_tahun' ,
            'ijazah_no' ,
            'foto' ,
            'nama_ayah' ,
            'tmp_lahir_ayah' ,
            'tgl_lahir_ayah' ,
            'pendidikan_ayah' ,
            'pekerjaan_ayah' ,
            'penghasilan_ayah' ,
            'nama_ibu' ,
            'pendidikan_ibu' ,
            'pekerjaan_ibu' ,
            'penghasilan_ibu' ,
            'alamat_ortu' ,
            'notelp_ortu' ,
            'jalur_penerimaan' ,
            'kdpa' ,
            'kelas' ,
            'nik' ,
            'nisn' ,
            'kdfakultas' ,
            'tgl_masuk' ,
            'pin' ,
            'nama_jurusan'=> function ($model)
            {
                if(isset($model->jurusan))
                    return $model->jurusan->namajur;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'nama_prodi'=> function ($model)
            {
                if(isset($model->prodi))
                    return $model->prodi->namaprodi;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'nama_fakultas'=> function ($model)
            {
                if(isset($model->jurusan->fakultas))
                    return $model->jurusan->fakultas;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'nama_jalur'=> function ($model)
            {
                if(isset($model->jalurMasuk))
                    return $model->jalurMasuk->jalur_penerimaan;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'pend_ibu'=> function ($model)
            {
                if(isset($model->pendidikanIbu))
                    return $model->pendidikanIbu->pendidikan;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'pend_ayah'=> function ($model)
            {
                if(isset($model->pendidikanAyah))
                    return $model->pendidikanAyah->pendidikan;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'pek_ibu'=> function ($model)
            {
                if(isset($model->pekerjaanIbu))
                    return $model->pekerjaanIbu->pekerjaan;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'pek_ayah'=> function ($model)
            {
                if(isset($model->pekerjaanAyah))
                    return $model->pekerjaanAyah->pekerjaan;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'peng_ibu'=> function ($model)
            {
                if(isset($model->penghasilanIbu))
                    return $model->penghasilanIbu->penghasilan;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'peng_ayah'=> function ($model)
            {
                if(isset($model->penghasilanAyah))
                    return $model->penghasilanAyah->penghasilan;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'dosen_pa'=> function ($model)
            {
                if(isset($model->dosenPa))
                    return $model->dosenPa->nama;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
            'kasubagAkaFak'=> function ($model)
            {
                if(isset($model->kasuba))
                    return $model->kasubagAkaFak;
                else
                    return 'Data Relasi Tidak Ditemukan';
            },
        ];
    }
}
