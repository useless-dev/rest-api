<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "komentar".
 *
 * @property int $no
 * @property string|null $tipe
 * @property string|null $kd_tipe
 * @property string|null $nama
 * @property string|null $email
 * @property string|null $website
 * @property string|null $komentar
 * @property string|null $tglwkt
 * @property string|null $status
 */
class Komentar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'komentar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipe'], 'string', 'max' => 20],
            [['kd_tipe'], 'string', 'max' => 255],
            [['nama'], 'string', 'max' => 50],
            [['email', 'website'], 'string', 'max' => 100],
            [['komentar'], 'string', 'max' => 350],
            [['tglwkt'], 'string', 'max' => 30],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'tipe' => 'Tipe',
            'kd_tipe' => 'Kd Tipe',
            'nama' => 'Nama',
            'email' => 'Email',
            'website' => 'Website',
            'komentar' => 'Komentar',
            'tglwkt' => 'Tglwkt',
            'status' => 'Status',
        ];
    }
}
