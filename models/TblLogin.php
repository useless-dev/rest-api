<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_login".
 *
 * @property int $id
 * @property string $kodebriva
 * @property string $first_name
 * @property string $last_name
 * @property string $nik
 * @property string $gender
 * @property string $email
 * @property string $password
 * @property string $address
 * @property string $mobile_no
 * @property string $raport
 * @property string $kk
 * @property string $session
 */
class TblLogin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'gender', 'email', 'password', 'address', 'mobile_no', 'raport', 'kk', 'session'], 'required'],
            [['address'], 'string'],
            [['kodebriva'], 'string', 'max' => 13],
            [['first_name', 'last_name'], 'string', 'max' => 100],
            [['nik'], 'string', 'max' => 16],
            [['gender'], 'string', 'max' => 30],
            [['email', 'password', 'raport', 'kk'], 'string', 'max' => 200],
            [['mobile_no'], 'string', 'max' => 15],
            [['session'], 'string', 'max' => 40],
            [['nik'], 'unique'],
            [['session'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kodebriva' => 'Kodebriva',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'nik' => 'Nik',
            'gender' => 'Gender',
            'email' => 'Email',
            'password' => 'Password',
            'address' => 'Address',
            'mobile_no' => 'Mobile No',
            'raport' => 'Raport',
            'kk' => 'Kk',
            'session' => 'Session',
        ];
    }
}
