<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MatakuliahAntara;

/**
 * MatakuliahAntaraSearch represents the model behind the search form of `app\models\MatakuliahAntara`.
 */
class MatakuliahAntaraSearch extends MatakuliahAntara
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idmk', 'krs_aktif'], 'integer'],
            [['kdmk', 'semester', 'kdprodi', 'namamk', 'sks', 'bersarat', 'keterangan', 'kurikulum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatakuliahAntara::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idmk' => $this->idmk,
            'krs_aktif' => $this->krs_aktif,
        ]);

        $query->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'namamk', $this->namamk])
            ->andFilterWhere(['like', 'sks', $this->sks])
            ->andFilterWhere(['like', 'bersarat', $this->bersarat])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'kurikulum', $this->kurikulum]);

        return $dataProvider;
    }
}
