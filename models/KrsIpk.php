<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs_ipk".
 *
 * @property int $idipk
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $kdprodi
 * @property string|null $tahun_akademik
 * @property string|null $jns_semester
 * @property int|null $semester_ini
 * @property int|null $jml_sks
 * @property int|null $jml_nilai
 * @property float|null $ipk
 */
class KrsIpk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs_ipk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester_ini', 'jml_sks', 'jml_nilai'], 'integer'],
            [['ipk'], 'number'],
            [['npm', 'kdprodi'], 'string', 'max' => 30],
            [['nama'], 'string', 'max' => 35],
            [['tahun_akademik'], 'string', 'max' => 25],
            [['jns_semester'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idipk' => 'Idipk',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'kdprodi' => 'Kdprodi',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'semester_ini' => 'Semester Ini',
            'jml_sks' => 'Jml Sks',
            'jml_nilai' => 'Jml Nilai',
            'ipk' => 'Ipk',
        ];
    }
}
