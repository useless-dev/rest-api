<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormSkpiCapaianEn;

/**
 * FormSkpiCapaianEnSearch represents the model behind the search form of `app\models\FormSkpiCapaianEn`.
 */
class FormSkpiCapaianEnSearch extends FormSkpiCapaianEn
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdakses'], 'integer'],
            [['tahun_wisuda', 'periode_wisuda', 'tahun_akademik', 'jns_semester', 'kdfakultas', 'kdprodi', 'bidang_kerja', 'pengetahuan', 'sikap_khusus'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormSkpiCapaianEn::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdakses' => $this->kdakses,
        ]);

        $query->andFilterWhere(['like', 'tahun_wisuda', $this->tahun_wisuda])
            ->andFilterWhere(['like', 'periode_wisuda', $this->periode_wisuda])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'bidang_kerja', $this->bidang_kerja])
            ->andFilterWhere(['like', 'pengetahuan', $this->pengetahuan])
            ->andFilterWhere(['like', 'sikap_khusus', $this->sikap_khusus]);

        return $dataProvider;
    }
}
