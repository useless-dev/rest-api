<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_alumni_jadwal".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string|null $tahun
 * @property string|null $periode
 * @property int|null $kuota
 */
class FormAlumniJadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_alumni_jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai'], 'safe'],
            [['kuota'], 'integer'],
            [['tahun'], 'string', 'max' => 30],
            [['periode'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'kuota' => 'Kuota',
        ];
    }
}
