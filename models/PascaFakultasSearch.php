<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PascaFakultas;

/**
 * PascaFakultasSearch represents the model behind the search form of `app\models\PascaFakultas`.
 */
class PascaFakultasSearch extends PascaFakultas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjur'], 'integer'],
            [['kdjur', 'namajur', 'kdfakultas', 'singkatan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PascaFakultas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idjur' => $this->idjur,
        ]);

        $query->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'namajur', $this->namajur])
            ->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas])
            ->andFilterWhere(['like', 'singkatan', $this->singkatan]);

        return $dataProvider;
    }
}
