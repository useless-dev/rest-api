<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "herregistrasi".
 *
 * @property int $id
 * @property string|null $npm
 * @property string|null $semester
 * @property string|null $ipk
 * @property string|null $tahun
 * @property string|null $jns_semester
 */
class Herregistrasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'herregistrasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm', 'semester', 'ipk', 'tahun', 'jns_semester'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'ipk' => 'Ipk',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
