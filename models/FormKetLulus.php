<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_ket_lulus".
 *
 * @property int $kdlulus
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $smt
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $ipk
 * @property string|null $predikat
 * @property string|null $untuk_keperluan
 * @property string|null $tgl_lulus
 * @property string|null $no_surat
 * @property string|null $tgl_surat
 * @property string|null $tgl_daftar
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $thn
 * @property string|null $nama_waka1
 * @property string|null $nip_waka1
 */
class FormKetLulus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_ket_lulus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_lulus', 'tgl_surat', 'tgl_daftar', 'thn'], 'safe'],
            [['npm', 'kdjur', 'kdprodi', 'ipk', 'no_surat', 'nip_waka1'], 'string', 'max' => 20],
            [['nama', 'smt', 'predikat'], 'string', 'max' => 35],
            [['untuk_keperluan'], 'string', 'max' => 50],
            [['tahun'], 'string', 'max' => 9],
            [['jns_semester'], 'string', 'max' => 6],
            [['nama_waka1'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdlulus' => 'Kdlulus',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'smt' => 'Smt',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'ipk' => 'Ipk',
            'predikat' => 'Predikat',
            'untuk_keperluan' => 'Untuk Keperluan',
            'tgl_lulus' => 'Tgl Lulus',
            'no_surat' => 'No Surat',
            'tgl_surat' => 'Tgl Surat',
            'tgl_daftar' => 'Tgl Daftar',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'thn' => 'Thn',
            'nama_waka1' => 'Nama Waka 1',
            'nip_waka1' => 'Nip Waka 1',
        ];
    }
}
