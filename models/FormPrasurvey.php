<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_prasurvey".
 *
 * @property int $id
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $smt
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $tot_sks
 * @property string|null $ipk
 * @property string|null $predikat
 * @property string $linksyarat
 * @property string|null $judul_ta
 * @property string|null $tempat_prasurvey
 * @property string|null $sebutan_pimpinan
 * @property string|null $no_tugas
 * @property string|null $no_prasurvey
 * @property string|null $tgl_surat
 * @property string|null $tgl_daftar
 * @property string|null $wkt_daftar
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $thn
 * @property string|null $nama_pejabat
 * @property string|null $nip_pejabat
 */
class FormPrasurvey extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_prasurvey';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_ta'], 'string'],
            [['tgl_surat', 'tgl_daftar', 'wkt_daftar', 'thn'], 'safe'],
            [['npm', 'kdjur', 'kdprodi', 'tot_sks', 'ipk', 'no_tugas', 'no_prasurvey', 'nip_pejabat'], 'string', 'max' => 20],
            [['nama', 'smt', 'predikat'], 'string', 'max' => 35],
            [['linksyarat'], 'string', 'max' => 500],
            [['tempat_prasurvey'], 'string', 'max' => 50],
            [['sebutan_pimpinan', 'nama_pejabat'], 'string', 'max' => 30],
            [['tahun'], 'string', 'max' => 9],
            [['jns_semester'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'smt' => 'Smt',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'tot_sks' => 'Tot Sks',
            'ipk' => 'Ipk',
            'predikat' => 'Predikat',
            'linksyarat' => 'Linksyarat',
            'judul_ta' => 'Judul Ta',
            'tempat_prasurvey' => 'Tempat Prasurvey',
            'sebutan_pimpinan' => 'Sebutan Pimpinan',
            'no_tugas' => 'No Tugas',
            'no_prasurvey' => 'No Prasurvey',
            'tgl_surat' => 'Tgl Surat',
            'tgl_daftar' => 'Tgl Daftar',
            'wkt_daftar' => 'Wkt Daftar',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'thn' => 'Thn',
            'nama_pejabat' => 'Nama Pejabat',
            'nip_pejabat' => 'Nip Pejabat',
        ];
    }
}
