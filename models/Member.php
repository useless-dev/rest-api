<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property int $kdmember
 * @property string|null $username
 * @property string|null $password
 * @property string|null $sessions
 * @property string|null $level
 * @property string|null $jenis_login
 * @property string|null $default_pass
 * @property string|null $nama
 * @property string|null $nipnis
 * @property string|null $foto
 * @property string|null $email
 * @property string|null $nohp
 * @property string|null $kdfakultas
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'sessions', 'default_pass', 'nama', 'nipnis', 'foto', 'email', 'nohp'], 'string', 'max' => 100],
            [['level', 'jenis_login', 'kdfakultas'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdmember' => 'Kdmember',
            'username' => 'Username',
            'password' => 'Password',
            'sessions' => 'Sessions',
            'level' => 'Level',
            'jenis_login' => 'Jenis Login',
            'default_pass' => 'Default Pass',
            'nama' => 'Nama',
            'nipnis' => 'Nipnis',
            'foto' => 'Foto',
            'email' => 'Email',
            'nohp' => 'Nohp',
            'kdfakultas' => 'Kdfakultas',
        ];
    }
}
