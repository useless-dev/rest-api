<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmisJmlMhsJurusanusia;

/**
 * EmisJmlMhsJurusanusiaSearch represents the model behind the search form of `app\models\EmisJmlMhsJurusanusia`.
 */
class EmisJmlMhsJurusanusiaSearch extends EmisJmlMhsJurusanusia
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdejmju', 'kdjur', 'u17', 'u18', 'u19', 'u20', 'u21', 'u22', 'u23', 'u24', 'u25', 'u26'], 'integer'],
            [['kdprodi', 'tahun_akademik'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmisJmlMhsJurusanusia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdejmju' => $this->kdejmju,
            'kdjur' => $this->kdjur,
            'u17' => $this->u17,
            'u18' => $this->u18,
            'u19' => $this->u19,
            'u20' => $this->u20,
            'u21' => $this->u21,
            'u22' => $this->u22,
            'u23' => $this->u23,
            'u24' => $this->u24,
            'u25' => $this->u25,
            'u26' => $this->u26,
        ]);

        $query->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik]);

        return $dataProvider;
    }
}
