<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dosen".
 *
 * @property int $kddosen
 * @property string|null $username
 * @property string|null $password
 * @property string|null $sessions
 * @property string|null $gelar_depan
 * @property string|null $gelar_belakang
 * @property string|null $nama
 * @property string|null $nip
 * @property string|null $tmp_lahir
 * @property string|null $tgl_lahir
 * @property string|null $jenkel
 * @property string|null $agama
 * @property string|null $status_kerja
 * @property string|null $status_perkawinan
 * @property string|null $warga_negara
 * @property string|null $alamat_asal
 * @property string|null $notelp
 * @property string|null $kode_pos
 * @property string|null $foto
 * @property string|null $nidn
 * @property string|null $jnsdosen
 * @property string|null $homebase
 */
class Dosen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dosen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'sessions'], 'string', 'max' => 50],
            [['gelar_depan', 'gelar_belakang', 'nip', 'notelp'], 'string', 'max' => 35],
            [['nama', 'tmp_lahir'], 'string', 'max' => 100],
            [['tgl_lahir', 'jenkel', 'warga_negara'], 'string', 'max' => 30],
            [['agama', 'status_kerja', 'status_perkawinan', 'kode_pos'], 'string', 'max' => 20],
            [['alamat_asal', 'foto', 'nidn', 'jnsdosen', 'homebase'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kddosen' => 'Kddosen',
            'username' => 'Username',
            'password' => 'Password',
            'sessions' => 'Sessions',
            'gelar_depan' => 'Gelar Depan',
            'gelar_belakang' => 'Gelar Belakang',
            'nama' => 'Nama',
            'nip' => 'Nip',
            'tmp_lahir' => 'Tmp Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'jenkel' => 'Jenkel',
            'agama' => 'Agama',
            'status_kerja' => 'Status Kerja',
            'status_perkawinan' => 'Status Perkawinan',
            'warga_negara' => 'Warga Negara',
            'alamat_asal' => 'Alamat Asal',
            'notelp' => 'Notelp',
            'kode_pos' => 'Kode Pos',
            'foto' => 'Foto',
            'nidn' => 'Nidn',
            'jnsdosen' => 'Jnsdosen',
            'homebase' => 'Homebase',
        ];
    }
}
