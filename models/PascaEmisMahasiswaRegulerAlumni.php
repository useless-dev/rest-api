<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_emis_mahasiswa_reguler_alumni".
 *
 * @property int $kdmhs
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $nama
 * @property string|null $npm
 * @property string|null $tmp_lahir
 * @property string|null $tgl_lahir
 * @property string|null $bln_lahir
 * @property string|null $thn_lahir
 * @property string|null $jenkel
 * @property string|null $asal_negara
 * @property string|null $asal_sekolah
 * @property string|null $jenjang_pendidikan
 * @property int|null $semester
 * @property string|null $jml_sks
 * @property float|null $ipk
 * @property string|null $ijazah_no
 * @property string|null $ijazah_tgl
 * @property string|null $ijazah_bln
 * @property string|null $ijazah_thn
 * @property string|null $penghasilan_ortu
 * @property string|null $pekerjaan_ayah
 * @property string|null $pekerjaan_ibu
 * @property string|null $pendidikan_ayah
 * @property string|null $pendidikan_ibu
 * @property string|null $setelahlulus_status
 * @property string|null $setelahlulus_bekerja
 * @property string|null $setelahlulus_domisili
 */
class PascaEmisMahasiswaRegulerAlumni extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_emis_mahasiswa_reguler_alumni';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester'], 'integer'],
            [['ipk'], 'number'],
            [['tahun', 'jns_semester', 'kdjur', 'npm', 'tgl_lahir', 'bln_lahir', 'thn_lahir', 'jenkel', 'jml_sks', 'ijazah_bln', 'penghasilan_ortu', 'setelahlulus_domisili'], 'string', 'max' => 35],
            [['kdprodi', 'asal_negara'], 'string', 'max' => 50],
            [['nama', 'ijazah_no', 'setelahlulus_bekerja'], 'string', 'max' => 100],
            [['tmp_lahir', 'asal_sekolah', 'pekerjaan_ayah', 'pekerjaan_ibu', 'pendidikan_ayah', 'pendidikan_ibu', 'setelahlulus_status'], 'string', 'max' => 255],
            [['jenjang_pendidikan', 'ijazah_tgl'], 'string', 'max' => 20],
            [['ijazah_thn'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdmhs' => 'Kdmhs',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'nama' => 'Nama',
            'npm' => 'Npm',
            'tmp_lahir' => 'Tmp Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'bln_lahir' => 'Bln Lahir',
            'thn_lahir' => 'Thn Lahir',
            'jenkel' => 'Jenkel',
            'asal_negara' => 'Asal Negara',
            'asal_sekolah' => 'Asal Sekolah',
            'jenjang_pendidikan' => 'Jenjang Pendidikan',
            'semester' => 'Semester',
            'jml_sks' => 'Jml Sks',
            'ipk' => 'Ipk',
            'ijazah_no' => 'Ijazah No',
            'ijazah_tgl' => 'Ijazah Tgl',
            'ijazah_bln' => 'Ijazah Bln',
            'ijazah_thn' => 'Ijazah Thn',
            'penghasilan_ortu' => 'Penghasilan Ortu',
            'pekerjaan_ayah' => 'Pekerjaan Ayah',
            'pekerjaan_ibu' => 'Pekerjaan Ibu',
            'pendidikan_ayah' => 'Pendidikan Ayah',
            'pendidikan_ibu' => 'Pendidikan Ibu',
            'setelahlulus_status' => 'Setelahlulus Status',
            'setelahlulus_bekerja' => 'Setelahlulus Bekerja',
            'setelahlulus_domisili' => 'Setelahlulus Domisili',
        ];
    }
}
