<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akademik".
 *
 * @property int $kdakademik
 * @property string|null $hak_akses
 * @property string|null $kdjur
 * @property string|null $username
 * @property string|null $password
 * @property string|null $sessions
 * @property string|null $nama
 * @property string|null $nip
 * @property string|null $tmp_lahir
 * @property string|null $tgl_lahir
 * @property string|null $jenkel
 * @property string|null $agama
 * @property string|null $status_kerja
 * @property string|null $status_perkawinan
 * @property string|null $warga_negara
 * @property string|null $alamat_asal
 * @property string|null $notelp
 * @property string|null $kode_pos
 * @property string|null $foto
 */
class Akademik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'akademik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hak_akses'], 'string', 'max' => 1],
            [['kdjur'], 'string', 'max' => 10],
            [['username', 'password', 'sessions'], 'string', 'max' => 50],
            [['nama', 'tmp_lahir'], 'string', 'max' => 100],
            [['nip', 'notelp'], 'string', 'max' => 35],
            [['tgl_lahir', 'jenkel', 'warga_negara'], 'string', 'max' => 30],
            [['agama', 'status_kerja', 'status_perkawinan', 'kode_pos'], 'string', 'max' => 20],
            [['alamat_asal', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakademik' => 'Kdakademik',
            'hak_akses' => 'Hak Akses',
            'kdjur' => 'Kdjur',
            'username' => 'Username',
            'password' => 'Password',
            'sessions' => 'Sessions',
            'nama' => 'Nama',
            'nip' => 'Nip',
            'tmp_lahir' => 'Tmp Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'jenkel' => 'Jenkel',
            'agama' => 'Agama',
            'status_kerja' => 'Status Kerja',
            'status_perkawinan' => 'Status Perkawinan',
            'warga_negara' => 'Warga Negara',
            'alamat_asal' => 'Alamat Asal',
            'notelp' => 'Notelp',
            'kode_pos' => 'Kode Pos',
            'foto' => 'Foto',
        ];
    }
}
