<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_ujikomp_akses".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string $tahun
 * @property string $periode
 * @property string $tahun_akademik
 * @property string $jns_semester
 * @property string $syarat_smt
 * @property string $kuota
 */
class FormUjikompAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_ujikomp_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai'], 'safe'],
            [['tahun'], 'string', 'max' => 30],
            [['periode'], 'string', 'max' => 5],
            [['tahun_akademik', 'jns_semester'], 'string', 'max' => 15],
            [['syarat_smt'], 'string', 'max' => 3],
            [['kuota'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'syarat_smt' => 'Syarat Smt',
            'kuota' => 'Kuota',
        ];
    }
}
