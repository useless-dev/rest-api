<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormUjikomp;

/**
 * FormUjikompSearch represents the model behind the search form of `app\models\FormUjikomp`.
 */
class FormUjikompSearch extends FormUjikomp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['npm', 'semester', 'kdprodi', 'tahun', 'periode', 'kelas', 'tgl_daftar', 'alasan', 'n_word', 'n_excel', 'n_ppt', 'n_akhir', 'no_sertifikat', 'no_seri', 'tgl_sertifikat', 'qr_seri'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormUjikomp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_daftar' => $this->tgl_daftar,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'periode', $this->periode])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'alasan', $this->alasan])
            ->andFilterWhere(['like', 'n_word', $this->n_word])
            ->andFilterWhere(['like', 'n_excel', $this->n_excel])
            ->andFilterWhere(['like', 'n_ppt', $this->n_ppt])
            ->andFilterWhere(['like', 'n_akhir', $this->n_akhir])
            ->andFilterWhere(['like', 'no_sertifikat', $this->no_sertifikat])
            ->andFilterWhere(['like', 'no_seri', $this->no_seri])
            ->andFilterWhere(['like', 'tgl_sertifikat', $this->tgl_sertifikat])
            ->andFilterWhere(['like', 'qr_seri', $this->qr_seri]);

        return $dataProvider;
    }
}
