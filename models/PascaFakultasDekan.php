<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_fakultas_dekan".
 *
 * @property int $idjur
 * @property string|null $kdfakultas
 * @property string|null $namajur
 * @property string|null $tahun_akademik
 * @property string|null $jns_semester
 * @property string|null $dekan
 * @property string|null $wadek1
 * @property string|null $wadek2
 * @property string|null $wadek3
 */
class PascaFakultasDekan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_fakultas_dekan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdfakultas'], 'string', 'max' => 35],
            [['namajur'], 'string', 'max' => 255],
            [['tahun_akademik'], 'string', 'max' => 15],
            [['jns_semester', 'dekan', 'wadek1', 'wadek2', 'wadek3'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idjur' => 'Idjur',
            'kdfakultas' => 'Kdfakultas',
            'namajur' => 'Namajur',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'dekan' => 'Dekan',
            'wadek1' => 'Wadek 1',
            'wadek2' => 'Wadek 2',
            'wadek3' => 'Wadek 3',
        ];
    }
}
