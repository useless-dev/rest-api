<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kd_kelas".
 *
 * @property int $kdkelas
 * @property string $kelas
 */
class KdKelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kd_kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kelas'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkelas' => 'Kdkelas',
            'kelas' => 'Kelas',
        ];
    }
}
