<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jadwal_input_nilai".
 *
 * @property int $kdjadwal
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string|null $tahun
 */
class JadwalInputNilai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jadwal_input_nilai';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai', 'tahun'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdjadwal' => 'Kdjadwal',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
        ];
    }
}
