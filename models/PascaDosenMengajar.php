<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_dosen_mengajar".
 *
 * @property int $kddosenkls
 * @property string $kddosen
 * @property string $kdmk
 * @property string $kdprodi
 * @property string $kurikulum
 * @property string $kdkelas
 * @property string $kelas
 * @property string $tahun
 * @property string $jns_semester
 */
class PascaDosenMengajar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_dosen_mengajar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kddosen'], 'string', 'max' => 10],
            [['kdmk', 'kurikulum'], 'string', 'max' => 35],
            [['kdprodi', 'tahun', 'jns_semester'], 'string', 'max' => 15],
            [['kdkelas', 'kelas'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kddosenkls' => 'Kddosenkls',
            'kddosen' => 'Kddosen',
            'kdmk' => 'Kdmk',
            'kdprodi' => 'Kdprodi',
            'kurikulum' => 'Kurikulum',
            'kdkelas' => 'Kdkelas',
            'kelas' => 'Kelas',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
