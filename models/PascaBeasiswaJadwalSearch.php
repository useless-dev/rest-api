<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PascaBeasiswaJadwal;

/**
 * PascaBeasiswaJadwalSearch represents the model behind the search form of `app\models\PascaBeasiswaJadwal`.
 */
class PascaBeasiswaJadwalSearch extends PascaBeasiswaJadwal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdakses'], 'integer'],
            [['mulai', 'selesai', 'tahun', 'kdbeasiswa', 'ipk_minimal'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PascaBeasiswaJadwal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdakses' => $this->kdakses,
        ]);

        $query->andFilterWhere(['like', 'mulai', $this->mulai])
            ->andFilterWhere(['like', 'selesai', $this->selesai])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'kdbeasiswa', $this->kdbeasiswa])
            ->andFilterWhere(['like', 'ipk_minimal', $this->ipk_minimal]);

        return $dataProvider;
    }
}
