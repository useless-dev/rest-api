<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email".
 *
 * @property int $kdemail
 * @property string|null $kduser
 * @property string|null $kirimke
 * @property string|null $judul
 * @property string|null $isi
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string|null $kduser_read
 * @property string|null $kirimke_read
 */
class Email extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isi'], 'string'],
            [['kduser', 'tgl', 'wkt', 'kduser_read', 'kirimke_read'], 'string', 'max' => 30],
            [['kirimke', 'judul'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdemail' => 'Kdemail',
            'kduser' => 'Kduser',
            'kirimke' => 'Kirimke',
            'judul' => 'Judul',
            'isi' => 'Isi',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'kduser_read' => 'Kduser Read',
            'kirimke_read' => 'Kirimke Read',
        ];
    }
}
