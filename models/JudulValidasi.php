<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "judul_validasi".
 *
 * @property int $kdakses
 * @property string|null $npm
 * @property string|null $kdjur
 * @property int|null $validasi
 * @property string $tahun
 * @property string $jns_semester
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string|null $petugas
 */
class JudulValidasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'judul_validasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['validasi'], 'integer'],
            [['tahun', 'jns_semester'], 'required'],
            [['tgl', 'wkt'], 'safe'],
            [['npm'], 'string', 'max' => 30],
            [['kdjur'], 'string', 'max' => 10],
            [['tahun', 'jns_semester'], 'string', 'max' => 20],
            [['petugas'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'npm' => 'Npm',
            'kdjur' => 'Kdjur',
            'validasi' => 'Validasi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'petugas' => 'Petugas',
        ];
    }
}
