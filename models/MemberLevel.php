<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_level".
 *
 * @property int $kdjenislogin
 * @property string|null $level
 * @property string|null $nama_level
 * @property string|null $level_group
 */
class MemberLevel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_level';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level', 'level_group'], 'string', 'max' => 35],
            [['nama_level'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdjenislogin' => 'Kdjenislogin',
            'level' => 'Level',
            'nama_level' => 'Nama Level',
            'level_group' => 'Level Group',
        ];
    }
}
