<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mahasiswa;

/**
 * MahasiswaSearch represents the model behind the search form of `app\models\Mahasiswa`.
 */
class MahasiswaSearch extends Mahasiswa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm', 'thn_masuk', 'status_mhs', 'status_mhs2', 'tgl_status2', 'thn_lulus', 'nosurat', 'notrn', 'username', 'password', 'sessions', 'nama', 'kdjur', 'kdprodi', 'tmp_lahir', 'tgl_lahir', 'jenkel', 'agama', 'status_kerja', 'status_perkawinan', 'warga_negara', 'alamat_asal', 'jalan', 'rt', 'rw', 'propinsi', 'kabupaten', 'kecamatan', 'kelurahan', 'notelp', 'notelpinternet', 'kode_pos', 'npsn', 'asal_sekolah', 'nama_sekolah', 'alamat_sekolah', 'ijazah_tahun', 'ijazah_no', 'foto', 'nama_ayah', 'tmp_lahir_ayah', 'tgl_lahir_ayah', 'pendidikan_ayah', 'pekerjaan_ayah', 'penghasilan_ayah', 'nama_ibu', 'pendidikan_ibu', 'pekerjaan_ibu', 'penghasilan_ibu', 'alamat_ortu', 'notelp_ortu', 'jalur_penerimaan', 'kelas', 'nik', 'nisn', 'tgl_masuk', 'pin'], 'safe'],
            [['kdpa', 'kdfakultas'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mahasiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdpa' => $this->kdpa,
            'kdfakultas' => $this->kdfakultas,
            'tgl_masuk' => $this->tgl_masuk,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'thn_masuk', $this->thn_masuk])
            ->andFilterWhere(['like', 'status_mhs', $this->status_mhs])
            ->andFilterWhere(['like', 'status_mhs2', $this->status_mhs2])
            ->andFilterWhere(['like', 'tgl_status2', $this->tgl_status2])
            ->andFilterWhere(['like', 'thn_lulus', $this->thn_lulus])
            ->andFilterWhere(['like', 'nosurat', $this->nosurat])
            ->andFilterWhere(['like', 'notrn', $this->notrn])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'sessions', $this->sessions])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tmp_lahir', $this->tmp_lahir])
            ->andFilterWhere(['like', 'tgl_lahir', $this->tgl_lahir])
            ->andFilterWhere(['like', 'jenkel', $this->jenkel])
            ->andFilterWhere(['like', 'agama', $this->agama])
            ->andFilterWhere(['like', 'status_kerja', $this->status_kerja])
            ->andFilterWhere(['like', 'status_perkawinan', $this->status_perkawinan])
            ->andFilterWhere(['like', 'warga_negara', $this->warga_negara])
            ->andFilterWhere(['like', 'alamat_asal', $this->alamat_asal])
            ->andFilterWhere(['like', 'jalan', $this->jalan])
            ->andFilterWhere(['like', 'rt', $this->rt])
            ->andFilterWhere(['like', 'rw', $this->rw])
            ->andFilterWhere(['like', 'propinsi', $this->propinsi])
            ->andFilterWhere(['like', 'kabupaten', $this->kabupaten])
            ->andFilterWhere(['like', 'kecamatan', $this->kecamatan])
            ->andFilterWhere(['like', 'kelurahan', $this->kelurahan])
            ->andFilterWhere(['like', 'notelp', $this->notelp])
            ->andFilterWhere(['like', 'notelpinternet', $this->notelpinternet])
            ->andFilterWhere(['like', 'kode_pos', $this->kode_pos])
            ->andFilterWhere(['like', 'npsn', $this->npsn])
            ->andFilterWhere(['like', 'asal_sekolah', $this->asal_sekolah])
            ->andFilterWhere(['like', 'nama_sekolah', $this->nama_sekolah])
            ->andFilterWhere(['like', 'alamat_sekolah', $this->alamat_sekolah])
            ->andFilterWhere(['like', 'ijazah_tahun', $this->ijazah_tahun])
            ->andFilterWhere(['like', 'ijazah_no', $this->ijazah_no])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'nama_ayah', $this->nama_ayah])
            ->andFilterWhere(['like', 'tmp_lahir_ayah', $this->tmp_lahir_ayah])
            ->andFilterWhere(['like', 'tgl_lahir_ayah', $this->tgl_lahir_ayah])
            ->andFilterWhere(['like', 'pendidikan_ayah', $this->pendidikan_ayah])
            ->andFilterWhere(['like', 'pekerjaan_ayah', $this->pekerjaan_ayah])
            ->andFilterWhere(['like', 'penghasilan_ayah', $this->penghasilan_ayah])
            ->andFilterWhere(['like', 'nama_ibu', $this->nama_ibu])
            ->andFilterWhere(['like', 'pendidikan_ibu', $this->pendidikan_ibu])
            ->andFilterWhere(['like', 'pekerjaan_ibu', $this->pekerjaan_ibu])
            ->andFilterWhere(['like', 'penghasilan_ibu', $this->penghasilan_ibu])
            ->andFilterWhere(['like', 'alamat_ortu', $this->alamat_ortu])
            ->andFilterWhere(['like', 'notelp_ortu', $this->notelp_ortu])
            ->andFilterWhere(['like', 'jalur_penerimaan', $this->jalur_penerimaan])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'nisn', $this->nisn])
            ->andFilterWhere(['like', 'pin', $this->pin]);

        return $dataProvider;
    }
}
