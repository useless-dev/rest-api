<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_emis_jml_mhs_jurusanusia".
 *
 * @property int $kdejmju
 * @property int|null $kdjur
 * @property string|null $kdprodi
 * @property int|null $u17
 * @property int|null $u18
 * @property int|null $u19
 * @property int|null $u20
 * @property int|null $u21
 * @property int|null $u22
 * @property int|null $u23
 * @property int|null $u24
 * @property int|null $u25
 * @property int|null $u26
 * @property string|null $tahun_akademik
 */
class PascaEmisJmlMhsJurusanusia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_emis_jml_mhs_jurusanusia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdjur', 'u17', 'u18', 'u19', 'u20', 'u21', 'u22', 'u23', 'u24', 'u25', 'u26'], 'integer'],
            [['kdprodi', 'tahun_akademik'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdejmju' => 'Kdejmju',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'u17' => 'U 17',
            'u18' => 'U 18',
            'u19' => 'U 19',
            'u20' => 'U 20',
            'u21' => 'U 21',
            'u22' => 'U 22',
            'u23' => 'U 23',
            'u24' => 'U 24',
            'u25' => 'U 25',
            'u26' => 'U 26',
            'tahun_akademik' => 'Tahun Akademik',
        ];
    }
}
