<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormKetMasastudi;

/**
 * FormKetMasastudiSearch represents the model behind the search form of `app\models\FormKetMasastudi`.
 */
class FormKetMasastudiSearch extends FormKetMasastudi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdketmasastudi'], 'integer'],
            [['jns_ket_masastudi', 'untuk_keperluan', 'npm', 'nama', 'smt', 'kdjur', 'kdprodi', 'no_surat', 'tgl_surat', 'tgl_daftar', 'tahun', 'jns_semester', 'thn', 'nama_pejabat', 'nip_pejabat', 'pangkat_gol_pejabat', 'smt_1', 'tgl_mulai', 'tgl_berakhir'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKetMasastudi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdketmasastudi' => $this->kdketmasastudi,
            'tgl_surat' => $this->tgl_surat,
            'tgl_daftar' => $this->tgl_daftar,
            'thn' => $this->thn,
            'tgl_mulai' => $this->tgl_mulai,
            'tgl_berakhir' => $this->tgl_berakhir,
        ]);

        $query->andFilterWhere(['like', 'jns_ket_masastudi', $this->jns_ket_masastudi])
            ->andFilterWhere(['like', 'untuk_keperluan', $this->untuk_keperluan])
            ->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'smt', $this->smt])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'no_surat', $this->no_surat])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'nama_pejabat', $this->nama_pejabat])
            ->andFilterWhere(['like', 'nip_pejabat', $this->nip_pejabat])
            ->andFilterWhere(['like', 'pangkat_gol_pejabat', $this->pangkat_gol_pejabat])
            ->andFilterWhere(['like', 'smt_1', $this->smt_1]);

        return $dataProvider;
    }
}
