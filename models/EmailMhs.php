<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_mhs".
 *
 * @property int $Id
 * @property string|null $npm
 * @property string|null $email
 * @property string|null $pass
 */
class EmailMhs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_mhs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm', 'email', 'pass'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'npm' => 'Npm',
            'email' => 'Email',
            'pass' => 'Pass',
        ];
    }
}
