<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "konter".
 *
 * @property string $ip
 * @property string $tanggal
 * @property string $waktu
 */
class Konter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'konter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip'], 'string', 'max' => 50],
            [['tanggal', 'waktu'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ip' => 'Ip',
            'tanggal' => 'Tanggal',
            'waktu' => 'Waktu',
        ];
    }
}
