<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DosenMengajarSementara;

/**
 * DosenMengajarSementaraSearch represents the model behind the search form of `app\models\DosenMengajarSementara`.
 */
class DosenMengajarSementaraSearch extends DosenMengajarSementara
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kddosenkls', 'kdfakultas'], 'integer'],
            [['kddosen', 'kdmk', 'kdprodi', 'kurikulum', 'kdkelas', 'kelas', 'tahun', 'jns_semester'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DosenMengajarSementara::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kddosenkls' => $this->kddosenkls,
            'kdfakultas' => $this->kdfakultas,
        ]);

        $query->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'kurikulum', $this->kurikulum])
            ->andFilterWhere(['like', 'kdkelas', $this->kdkelas])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
