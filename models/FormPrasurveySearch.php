<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormPrasurvey;

/**
 * FormPrasurveySearch represents the model behind the search form of `app\models\FormPrasurvey`.
 */
class FormPrasurveySearch extends FormPrasurvey
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['npm', 'nama', 'smt', 'kdjur', 'kdprodi', 'tot_sks', 'ipk', 'predikat', 'linksyarat', 'judul_ta', 'tempat_prasurvey', 'sebutan_pimpinan', 'no_tugas', 'no_prasurvey', 'tgl_surat', 'tgl_daftar', 'wkt_daftar', 'tahun', 'jns_semester', 'thn', 'nama_pejabat', 'nip_pejabat'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormPrasurvey::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_surat' => $this->tgl_surat,
            'tgl_daftar' => $this->tgl_daftar,
            'wkt_daftar' => $this->wkt_daftar,
            'thn' => $this->thn,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'smt', $this->smt])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tot_sks', $this->tot_sks])
            ->andFilterWhere(['like', 'ipk', $this->ipk])
            ->andFilterWhere(['like', 'predikat', $this->predikat])
            ->andFilterWhere(['like', 'linksyarat', $this->linksyarat])
            ->andFilterWhere(['like', 'judul_ta', $this->judul_ta])
            ->andFilterWhere(['like', 'tempat_prasurvey', $this->tempat_prasurvey])
            ->andFilterWhere(['like', 'sebutan_pimpinan', $this->sebutan_pimpinan])
            ->andFilterWhere(['like', 'no_tugas', $this->no_tugas])
            ->andFilterWhere(['like', 'no_prasurvey', $this->no_prasurvey])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'nama_pejabat', $this->nama_pejabat])
            ->andFilterWhere(['like', 'nip_pejabat', $this->nip_pejabat]);

        return $dataProvider;
    }
}
