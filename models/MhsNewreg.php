<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mhs_newreg".
 *
 * @property int $kdnewreg
 * @property string|null $npm
 * @property string|null $jns
 * @property int|null $status
 */
class MhsNewreg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mhs_newreg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['npm', 'jns'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdnewreg' => 'Kdnewreg',
            'npm' => 'Npm',
            'jns' => 'Jns',
            'status' => 'Status',
        ];
    }
}
