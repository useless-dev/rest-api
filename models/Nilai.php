<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nilai".
 *
 * @property int $kdnilai
 * @property string $kddosen
 * @property string|null $npm
 * @property string|null $kdmk
 * @property string|null $kdprodi
 * @property int|null $semester
 * @property string|null $sks
 * @property string|null $huruf
 * @property float|null $angka
 * @property float|null $jumlah
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string $keterangan
 * @property string $tgl_input
 * @property string $waktu_input
 * @property string $petugas
 * @property string $tgl_khs
 * @property int|null $mk_urutan
 * @property int|null $kdfakultas
 */
class Nilai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nilai';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester', 'mk_urutan', 'kdfakultas'], 'integer'],
            [['angka', 'jumlah'], 'number'],
            [['kddosen', 'npm', 'kdmk', 'kdprodi', 'sks'], 'string', 'max' => 20],
            [['huruf', 'keterangan'], 'string', 'max' => 30],
            [['tahun'], 'string', 'max' => 15],
            [['jns_semester'], 'string', 'max' => 6],
            [['tgl_input', 'waktu_input', 'tgl_khs'], 'string', 'max' => 22],
            [['petugas'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdnilai' => 'Kdnilai',
            'kddosen' => 'Kddosen',
            'npm' => 'Npm',
            'kdmk' => 'Kdmk',
            'kdprodi' => 'Kdprodi',
            'semester' => 'Semester',
            'sks' => 'Sks',
            'huruf' => 'Huruf',
            'angka' => 'Angka',
            'jumlah' => 'Jumlah',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'keterangan' => 'Keterangan',
            'tgl_input' => 'Tgl Input',
            'waktu_input' => 'Waktu Input',
            'petugas' => 'Petugas',
            'tgl_khs' => 'Tgl Khs',
            'mk_urutan' => 'Mk Urutan',
            'kdfakultas' => 'Kdfakultas',
        ];
    }
}
