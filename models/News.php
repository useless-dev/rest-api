<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $no
 * @property string|null $judul
 * @property string|null $pengirim
 * @property string|null $isi
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string|null $baca
 * @property int $bulan
 * @property string|null $gambar
 * @property string|null $file
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isi'], 'string'],
            [['bulan'], 'integer'],
            [['judul', 'gambar'], 'string', 'max' => 255],
            [['pengirim'], 'string', 'max' => 50],
            [['tgl', 'wkt', 'baca'], 'string', 'max' => 35],
            [['file'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'judul' => 'Judul',
            'pengirim' => 'Pengirim',
            'isi' => 'Isi',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'baca' => 'Baca',
            'bulan' => 'Bulan',
            'gambar' => 'Gambar',
            'file' => 'File',
        ];
    }
}
