<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "egroup".
 *
 * @property int $id_group
 * @property string|null $group
 * @property string|null $p1
 * @property string|null $p2
 * @property string|null $p3
 * @property string|null $p4
 * @property string|null $p5
 * @property string|null $CreatedDate
 */
class Egroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'egroup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group', 'p1', 'p2', 'p3', 'p4', 'p5'], 'string', 'max' => 255],
            [['CreatedDate'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_group' => 'Id Group',
            'group' => 'Group',
            'p1' => 'P 1',
            'p2' => 'P 2',
            'p3' => 'P 3',
            'p4' => 'P 4',
            'p5' => 'P 5',
            'CreatedDate' => 'Created Date',
        ];
    }
}
