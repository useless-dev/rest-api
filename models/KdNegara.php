<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kd_negara".
 *
 * @property int $Id
 * @property string|null $kdnegara
 * @property string|null $negara
 */
class KdNegara extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kd_negara';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdnegara'], 'string', 'max' => 4],
            [['negara'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'kdnegara' => 'Kdnegara',
            'negara' => 'Negara',
        ];
    }
}
