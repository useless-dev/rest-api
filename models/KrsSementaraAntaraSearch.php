<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KrsSementaraAntara;

/**
 * KrsSementaraAntaraSearch represents the model behind the search form of `app\models\KrsSementaraAntara`.
 */
class KrsSementaraAntaraSearch extends KrsSementaraAntara
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdkrs'], 'integer'],
            [['kdmk', 'sks', 'ip', 'tgl', 'wkt', 'kelas', 'kddosen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KrsSementaraAntara::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdkrs' => $this->kdkrs,
        ]);

        $query->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'sks', $this->sks])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'tgl', $this->tgl])
            ->andFilterWhere(['like', 'wkt', $this->wkt])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'kddosen', $this->kddosen]);

        return $dataProvider;
    }
}
