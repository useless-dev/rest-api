<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "okpp_jns_skpns".
 *
 * @property int $Id
 * @property string|null $namaskpns
 */
class OkppJnsSkpns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'okpp_jns_skpns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namaskpns'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'namaskpns' => 'Namaskpns',
        ];
    }
}
