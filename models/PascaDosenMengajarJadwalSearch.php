<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PascaDosenMengajarJadwal;

/**
 * PascaDosenMengajarJadwalSearch represents the model behind the search form of `app\models\PascaDosenMengajarJadwal`.
 */
class PascaDosenMengajarJadwalSearch extends PascaDosenMengajarJadwal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdjadwal'], 'integer'],
            [['kdmk', 'kurikulum', 'namamk', 'kddosen', 'namadosen', 'hari', 'jam_mulai', 'jam_selesai', 'tahun_akademik', 'jns_semester', 'kdprodi', 'kdkelas', 'kelas'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PascaDosenMengajarJadwal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdjadwal' => $this->kdjadwal,
            'jam_mulai' => $this->jam_mulai,
            'jam_selesai' => $this->jam_selesai,
        ]);

        $query->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'kurikulum', $this->kurikulum])
            ->andFilterWhere(['like', 'namamk', $this->namamk])
            ->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'namadosen', $this->namadosen])
            ->andFilterWhere(['like', 'hari', $this->hari])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'kdkelas', $this->kdkelas])
            ->andFilterWhere(['like', 'kelas', $this->kelas]);

        return $dataProvider;
    }
}
