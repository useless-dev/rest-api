<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs_akses".
 *
 * @property int $kdkrsakses
 * @property string|null $npm
 * @property string|null $status
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string|null $tahun
 * @property string $jns_semester
 * @property int|null $kdakademik
 */
class KrsAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
            [['tgl', 'wkt'], 'safe'],
            [['jns_semester'], 'required'],
            [['kdakademik'], 'integer'],
            [['npm', 'tahun', 'jns_semester'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkrsakses' => 'Kdkrsakses',
            'npm' => 'Npm',
            'status' => 'Status',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'kdakademik' => 'Kdakademik',
        ];
    }
}
