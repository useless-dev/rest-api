<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_opak".
 *
 * @property int $kdopak
 * @property string $npm
 * @property string $kdprodi
 * @property string $tahun
 * @property string $jns_semester
 * @property string $ukuran_jaket
 * @property string $tgl_daftar
 * @property float|null $nilai_akhir
 * @property string $status_lulus
 */
class FormOpak extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_opak';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar'], 'safe'],
            [['nilai_akhir'], 'number'],
            [['npm'], 'string', 'max' => 15],
            [['kdprodi', 'tahun'], 'string', 'max' => 20],
            [['jns_semester', 'ukuran_jaket'], 'string', 'max' => 10],
            [['status_lulus'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdopak' => 'Kdopak',
            'npm' => 'Npm',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'ukuran_jaket' => 'Ukuran Jaket',
            'tgl_daftar' => 'Tgl Daftar',
            'nilai_akhir' => 'Nilai Akhir',
            'status_lulus' => 'Status Lulus',
        ];
    }
}
