<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_wifi_akses".
 *
 * @property int $kdakses
 * @property string $mulai
 * @property string $selesai
 * @property string $tahun
 * @property string $periode
 * @property string $syarat_smt
 * @property string $kuota
 */
class FormWifiAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_wifi_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai', 'tahun'], 'string', 'max' => 30],
            [['periode'], 'string', 'max' => 1],
            [['syarat_smt'], 'string', 'max' => 3],
            [['kuota'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'syarat_smt' => 'Syarat Smt',
            'kuota' => 'Kuota',
        ];
    }
}
