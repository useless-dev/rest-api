<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormResearch;

/**
 * FormResearchSearch represents the model behind the search form of `app\models\FormResearch`.
 */
class FormResearchSearch extends FormResearch
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdresearch'], 'integer'],
            [['npm', 'nama', 'smt', 'kdjur', 'kdprodi', 'ipk', 'predikat', 'judul_ta', 'tempat_research', 'sebutan_pimpinan', 'no_tugas', 'no_research', 'tgl_surat', 'tgl_daftar', 'tahun', 'jns_semester', 'thn', 'nama_waka1', 'nip_waka1'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormResearch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdresearch' => $this->kdresearch,
            'tgl_surat' => $this->tgl_surat,
            'tgl_daftar' => $this->tgl_daftar,
            'thn' => $this->thn,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'smt', $this->smt])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'ipk', $this->ipk])
            ->andFilterWhere(['like', 'predikat', $this->predikat])
            ->andFilterWhere(['like', 'judul_ta', $this->judul_ta])
            ->andFilterWhere(['like', 'tempat_research', $this->tempat_research])
            ->andFilterWhere(['like', 'sebutan_pimpinan', $this->sebutan_pimpinan])
            ->andFilterWhere(['like', 'no_tugas', $this->no_tugas])
            ->andFilterWhere(['like', 'no_research', $this->no_research])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'nama_waka1', $this->nama_waka1])
            ->andFilterWhere(['like', 'nip_waka1', $this->nip_waka1]);

        return $dataProvider;
    }
}
