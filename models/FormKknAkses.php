<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_kkn_akses".
 *
 * @property int $kdakses
 * @property string $mulai
 * @property string $selesai
 * @property string $tahun
 * @property string $periode
 * @property string $tahun_akademik
 * @property string $jns_semester
 * @property string $syarat_sks
 * @property string $kuota
 */
class FormKknAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_kkn_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai'], 'safe'],
            [['tahun'], 'string', 'max' => 30],
            [['periode'], 'string', 'max' => 1],
            [['tahun_akademik'], 'string', 'max' => 15],
            [['jns_semester', 'kuota'], 'string', 'max' => 10],
            [['syarat_sks'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'syarat_sks' => 'Syarat Sks',
            'kuota' => 'Kuota',
        ];
    }
}
