<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormSkpi;

/**
 * FormSkpiSearch represents the model behind the search form of `app\models\FormSkpi`.
 */
class FormSkpiSearch extends FormSkpi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdskpi'], 'integer'],
            [['npm', 'kdjur', 'kdprodi', 'kategori', 'id_deskripsi', 'en_deskripsi', 'tingkat', 'tahun', 'tgl', 'wkt', 'tahun_akademik', 'jns_semester', 'status', 'ket_status', 'file', 'tgl_update', 'user'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormSkpi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdskpi' => $this->kdskpi,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'id_deskripsi', $this->id_deskripsi])
            ->andFilterWhere(['like', 'en_deskripsi', $this->en_deskripsi])
            ->andFilterWhere(['like', 'tingkat', $this->tingkat])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'tgl', $this->tgl])
            ->andFilterWhere(['like', 'wkt', $this->wkt])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'ket_status', $this->ket_status])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'tgl_update', $this->tgl_update])
            ->andFilterWhere(['like', 'user', $this->user]);

        return $dataProvider;
    }
}
