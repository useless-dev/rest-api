<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_beasiswa_informasi".
 *
 * @property int $kdinformasi
 * @property int|null $kdbeasiswa
 * @property string|null $judul
 * @property int|null $kdakademik
 * @property string|null $isi
 * @property string|null $tgl
 * @property string|null $wkt
 */
class PascaBeasiswaInformasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_beasiswa_informasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdbeasiswa', 'kdakademik'], 'integer'],
            [['isi'], 'string'],
            [['tgl', 'wkt'], 'safe'],
            [['judul'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdinformasi' => 'Kdinformasi',
            'kdbeasiswa' => 'Kdbeasiswa',
            'judul' => 'Judul',
            'kdakademik' => 'Kdakademik',
            'isi' => 'Isi',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
        ];
    }
}
