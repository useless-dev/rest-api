<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PascaFakultasDekan;

/**
 * PascaFakultasDekanSearch represents the model behind the search form of `app\models\PascaFakultasDekan`.
 */
class PascaFakultasDekanSearch extends PascaFakultasDekan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjur'], 'integer'],
            [['kdfakultas', 'namajur', 'tahun_akademik', 'jns_semester', 'dekan', 'wadek1', 'wadek2', 'wadek3'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PascaFakultasDekan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idjur' => $this->idjur,
        ]);

        $query->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas])
            ->andFilterWhere(['like', 'namajur', $this->namajur])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'dekan', $this->dekan])
            ->andFilterWhere(['like', 'wadek1', $this->wadek1])
            ->andFilterWhere(['like', 'wadek2', $this->wadek2])
            ->andFilterWhere(['like', 'wadek3', $this->wadek3]);

        return $dataProvider;
    }
}
