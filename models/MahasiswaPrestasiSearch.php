<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MahasiswaPrestasi;

/**
 * MahasiswaPrestasiSearch represents the model behind the search form of `app\models\MahasiswaPrestasi`.
 */
class MahasiswaPrestasiSearch extends MahasiswaPrestasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdbeasiswa', 'validasi', 'semester'], 'integer'],
            [['npm', 'jns_beasiswa', 'tahun', 'jns_semester', 'tgl', 'wkt'], 'safe'],
            [['jumlah'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MahasiswaPrestasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdbeasiswa' => $this->kdbeasiswa,
            'validasi' => $this->validasi,
            'semester' => $this->semester,
            'jumlah' => $this->jumlah,
            'tgl' => $this->tgl,
            'wkt' => $this->wkt,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'jns_beasiswa', $this->jns_beasiswa])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
