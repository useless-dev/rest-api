<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matakuliah_antara".
 *
 * @property int $idmk
 * @property string $kdmk
 * @property string $semester
 * @property string|null $kdprodi
 * @property string|null $namamk
 * @property string|null $sks
 * @property string $bersarat
 * @property string $keterangan
 * @property int|null $krs_aktif
 * @property string|null $kurikulum
 */
class MatakuliahAntara extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matakuliah_antara';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester'], 'required'],
            [['krs_aktif'], 'integer'],
            [['kdmk', 'bersarat', 'kurikulum'], 'string', 'max' => 30],
            [['semester', 'sks'], 'string', 'max' => 10],
            [['kdprodi'], 'string', 'max' => 20],
            [['namamk', 'keterangan'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmk' => 'Idmk',
            'kdmk' => 'Kdmk',
            'semester' => 'Semester',
            'kdprodi' => 'Kdprodi',
            'namamk' => 'Namamk',
            'sks' => 'Sks',
            'bersarat' => 'Bersarat',
            'keterangan' => 'Keterangan',
            'krs_aktif' => 'Krs Aktif',
            'kurikulum' => 'Kurikulum',
        ];
    }
}
