<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edom_awal".
 *
 * @property int $Id
 * @property string|null $npm
 * @property string|null $tahun
 * @property string|null $semester
 * @property string|null $p1
 * @property string|null $kdmk
 * @property string|null $kddosen
 * @property string|null $kurikulum
 * @property string|null $kdprodi
 * @property string|null $p2
 * @property string|null $p3
 * @property string|null $p4
 * @property string|null $p5
 * @property string|null $p6
 * @property string|null $p7
 * @property string|null $p8
 * @property string|null $p9
 * @property string|null $p10
 * @property string|null $p11
 * @property string|null $p12
 * @property string|null $p13
 * @property string|null $p14
 * @property string|null $p15
 * @property string|null $p16
 * @property string|null $p17
 * @property string|null $p18
 * @property string|null $p19
 * @property string|null $p20
 * @property string|null $p21
 * @property string|null $p22
 * @property string|null $p23
 * @property string|null $p24
 * @property string|null $p25
 * @property string|null $p26
 * @property string|null $p27
 * @property string|null $p28
 * @property string|null $kelas
 * @property string|null $jns_semester
 */
class EdomAwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'edom_awal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm', 'semester', 'kddosen', 'kelas', 'jns_semester'], 'string', 'max' => 20],
            [['tahun'], 'string', 'max' => 10],
            [['p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10', 'p11', 'p12', 'p13', 'p14', 'p15', 'p16', 'p17', 'p18', 'p19', 'p20', 'p21', 'p22', 'p23', 'p24', 'p25', 'p26', 'p27', 'p28'], 'string', 'max' => 100],
            [['kdmk'], 'string', 'max' => 35],
            [['kurikulum', 'kdprodi'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'npm' => 'Npm',
            'tahun' => 'Tahun',
            'semester' => 'Semester',
            'p1' => 'P 1',
            'kdmk' => 'Kdmk',
            'kddosen' => 'Kddosen',
            'kurikulum' => 'Kurikulum',
            'kdprodi' => 'Kdprodi',
            'p2' => 'P 2',
            'p3' => 'P 3',
            'p4' => 'P 4',
            'p5' => 'P 5',
            'p6' => 'P 6',
            'p7' => 'P 7',
            'p8' => 'P 8',
            'p9' => 'P 9',
            'p10' => 'P 10',
            'p11' => 'P 11',
            'p12' => 'P 12',
            'p13' => 'P 13',
            'p14' => 'P 14',
            'p15' => 'P 15',
            'p16' => 'P 16',
            'p17' => 'P 17',
            'p18' => 'P 18',
            'p19' => 'P 19',
            'p20' => 'P 20',
            'p21' => 'P 21',
            'p22' => 'P 22',
            'p23' => 'P 23',
            'p24' => 'P 24',
            'p25' => 'P 25',
            'p26' => 'P 26',
            'p27' => 'P 27',
            'p28' => 'P 28',
            'kelas' => 'Kelas',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
