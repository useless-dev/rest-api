<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JadwalDaftarPpl;

/**
 * JadwalDaftarPplSearch represents the model behind the search form of `app\models\JadwalDaftarPpl`.
 */
class JadwalDaftarPplSearch extends JadwalDaftarPpl
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdakses'], 'integer'],
            [['kdprodi', 'mulai', 'selesai', 'tahun', 'jns_semester', 'periode'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JadwalDaftarPpl::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdakses' => $this->kdakses,
        ]);

        $query->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'mulai', $this->mulai])
            ->andFilterWhere(['like', 'selesai', $this->selesai])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'periode', $this->periode]);

        return $dataProvider;
    }
}
