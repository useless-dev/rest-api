<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jurusan_iain_ketua".
 *
 * @property int $idprodi
 * @property string|null $kdprodi_pdpt
 * @property string $kdprodi
 * @property int|null $kdfakultas
 * @property string|null $kajur
 * @property string|null $tahun_akademik
 * @property string|null $jns_semester
 */
class JurusanIainKetua extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jurusan_iain_ketua';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdfakultas'], 'integer'],
            [['kdprodi_pdpt', 'tahun_akademik', 'jns_semester'], 'string', 'max' => 15],
            [['kdprodi'], 'string', 'max' => 35],
            [['kajur'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idprodi' => 'Idprodi',
            'kdprodi_pdpt' => 'Kdprodi Pdpt',
            'kdprodi' => 'Kdprodi',
            'kdfakultas' => 'Kdfakultas',
            'kajur' => 'Kajur',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
