<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_komputer_akses".
 *
 * @property int $kdakses
 * @property string $kdprodi
 * @property string $status
 */
class FormKomputerAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_komputer_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdprodi'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'kdprodi' => 'Kdprodi',
            'status' => 'Status',
        ];
    }
}
