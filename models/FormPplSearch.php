<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormPpl;

/**
 * FormPplSearch represents the model behind the search form of `app\models\FormPpl`.
 */
class FormPplSearch extends FormPpl
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nilai_1', 'nilai_2', 'nilai_3', 'nilai_akhir'], 'integer'],
            [['npm', 'semester', 'kdjur', 'kdprodi', 'tahun', 'jns_semester', 'total_sks', 'ipk', 'predikat', 'tgl_daftar', 'wkt_daftar', 'kddosen', 'no_sertifikat'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormPpl::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_daftar' => $this->tgl_daftar,
            'wkt_daftar' => $this->wkt_daftar,
            'nilai_1' => $this->nilai_1,
            'nilai_2' => $this->nilai_2,
            'nilai_3' => $this->nilai_3,
            'nilai_akhir' => $this->nilai_akhir,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'total_sks', $this->total_sks])
            ->andFilterWhere(['like', 'ipk', $this->ipk])
            ->andFilterWhere(['like', 'predikat', $this->predikat])
            ->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'no_sertifikat', $this->no_sertifikat]);

        return $dataProvider;
    }
}
