<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Krs;

/**
 * KrsSearch represents the model behind the search form of `app\models\Krs`.
 */
class KrsSearch extends Krs
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdkrs', 'mk_urutan', 'kdfakultas'], 'integer'],
            [['npm', 'semester', 'kdmk', 'kdprodi', 'sks', 'keterangan', 'tahun', 'ipsebelumnya', 'ip', 'tgl', 'wkt', 'kelas', 'jns_semester', 'kddosen', 'kurikulum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Krs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdkrs' => $this->kdkrs,
            'mk_urutan' => $this->mk_urutan,
            'kdfakultas' => $this->kdfakultas,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'sks', $this->sks])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'ipsebelumnya', $this->ipsebelumnya])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'tgl', $this->tgl])
            ->andFilterWhere(['like', 'wkt', $this->wkt])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'kurikulum', $this->kurikulum]);

        return $dataProvider;
    }
}
