<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MhsNewreg;

/**
 * MhsNewregSearch represents the model behind the search form of `app\models\MhsNewreg`.
 */
class MhsNewregSearch extends MhsNewreg
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdnewreg', 'status'], 'integer'],
            [['npm', 'jns'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhsNewreg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdnewreg' => $this->kdnewreg,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'jns', $this->jns]);

        return $dataProvider;
    }
}
