<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa_aktif_persemester".
 *
 * @property int $kdkrsvalidasi
 * @property string|null $npm
 * @property int|null $validasi
 * @property int|null $semester
 * @property string $jns_validasi
 * @property string $tahun
 * @property string $jns_semester
 * @property float|null $jumlah
 * @property string|null $tgl
 * @property string|null $wkt
 */
class MahasiswaAktifPersemester extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa_aktif_persemester';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['validasi', 'semester'], 'integer'],
            [['jns_validasi', 'tahun', 'jns_semester'], 'required'],
            [['jumlah'], 'number'],
            [['tgl', 'wkt'], 'safe'],
            [['npm'], 'string', 'max' => 30],
            [['jns_validasi', 'tahun', 'jns_semester'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkrsvalidasi' => 'Kdkrsvalidasi',
            'npm' => 'Npm',
            'validasi' => 'Validasi',
            'semester' => 'Semester',
            'jns_validasi' => 'Jns Validasi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'jumlah' => 'Jumlah',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
        ];
    }
}
