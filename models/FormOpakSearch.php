<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormOpak;

/**
 * FormOpakSearch represents the model behind the search form of `app\models\FormOpak`.
 */
class FormOpakSearch extends FormOpak
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdopak'], 'integer'],
            [['npm', 'kdprodi', 'tahun', 'jns_semester', 'ukuran_jaket', 'tgl_daftar', 'status_lulus'], 'safe'],
            [['nilai_akhir'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormOpak::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdopak' => $this->kdopak,
            'tgl_daftar' => $this->tgl_daftar,
            'nilai_akhir' => $this->nilai_akhir,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'ukuran_jaket', $this->ukuran_jaket])
            ->andFilterWhere(['like', 'status_lulus', $this->status_lulus]);

        return $dataProvider;
    }
}
