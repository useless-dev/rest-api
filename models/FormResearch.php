<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_research".
 *
 * @property int $kdresearch
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $smt
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $ipk
 * @property string|null $predikat
 * @property string|null $judul_ta
 * @property string|null $tempat_research
 * @property string|null $sebutan_pimpinan
 * @property string|null $no_tugas
 * @property string|null $no_research
 * @property string|null $tgl_surat
 * @property string|null $tgl_daftar
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $thn
 * @property string|null $nama_waka1
 * @property string|null $nip_waka1
 */
class FormResearch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_research';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_ta'], 'string'],
            [['tgl_surat', 'tgl_daftar', 'thn'], 'safe'],
            [['npm', 'kdjur', 'kdprodi', 'ipk', 'no_tugas', 'no_research', 'nip_waka1'], 'string', 'max' => 20],
            [['nama', 'smt', 'predikat'], 'string', 'max' => 35],
            [['tempat_research'], 'string', 'max' => 50],
            [['sebutan_pimpinan', 'nama_waka1'], 'string', 'max' => 30],
            [['tahun'], 'string', 'max' => 9],
            [['jns_semester'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdresearch' => 'Kdresearch',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'smt' => 'Smt',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'ipk' => 'Ipk',
            'predikat' => 'Predikat',
            'judul_ta' => 'Judul Ta',
            'tempat_research' => 'Tempat Research',
            'sebutan_pimpinan' => 'Sebutan Pimpinan',
            'no_tugas' => 'No Tugas',
            'no_research' => 'No Research',
            'tgl_surat' => 'Tgl Surat',
            'tgl_daftar' => 'Tgl Daftar',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'thn' => 'Thn',
            'nama_waka1' => 'Nama Waka 1',
            'nip_waka1' => 'Nip Waka 1',
        ];
    }
}
