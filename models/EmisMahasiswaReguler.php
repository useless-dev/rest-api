<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emis_mahasiswa_reguler".
 *
 * @property int $kdmhs
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $nama
 * @property string|null $npm
 * @property string|null $tmp_lahir
 * @property string|null $tgl_lahir
 * @property string|null $bln_lahir
 * @property string|null $thn_lahir
 * @property string|null $jenkel
 * @property string|null $asal_negara
 * @property string|null $tgl_masuk
 * @property string|null $bln_masuk
 * @property string|null $thn_masuk
 * @property string|null $status_mhs
 * @property string|null $status_mhs2
 * @property string|null $tgl_status
 * @property string|null $bln_status
 * @property string|null $thn_status
 * @property string|null $jenjang_pendidikan
 * @property int|null $semester
 * @property string|null $asal_sekolah
 * @property float|null $ipk
 * @property string|null $penghasilan_ortu
 * @property string|null $pekerjaan_ayah
 * @property string|null $pekerjaan_ibu
 * @property string|null $pendidikan_ayah
 * @property string|null $pendidikan_ibu
 * @property string|null $beasiswa_miskin
 * @property string|null $beasiswa_bidikmisi
 * @property string|null $beasiswa_lain
 * @property string|null $alamat
 * @property string|null $kabupaten
 * @property string|null $propinsi_kode
 * @property string|null $propinsi_nama
 */
class EmisMahasiswaReguler extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emis_mahasiswa_reguler';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester'], 'integer'],
            [['ipk'], 'number'],
            [['tahun', 'jns_semester', 'kdjur', 'npm', 'tgl_lahir', 'bln_lahir', 'thn_lahir', 'jenkel', 'tgl_masuk', 'bln_masuk', 'thn_masuk', 'status_mhs', 'status_mhs2', 'tgl_status', 'bln_status', 'thn_status', 'penghasilan_ortu', 'propinsi_kode'], 'string', 'max' => 35],
            [['kdprodi', 'asal_negara'], 'string', 'max' => 50],
            [['nama', 'kabupaten', 'propinsi_nama'], 'string', 'max' => 100],
            [['tmp_lahir', 'asal_sekolah', 'pekerjaan_ayah', 'pekerjaan_ibu', 'pendidikan_ayah', 'pendidikan_ibu', 'beasiswa_miskin', 'beasiswa_bidikmisi', 'beasiswa_lain', 'alamat'], 'string', 'max' => 255],
            [['jenjang_pendidikan'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdmhs' => 'Kdmhs',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'nama' => 'Nama',
            'npm' => 'Npm',
            'tmp_lahir' => 'Tmp Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'bln_lahir' => 'Bln Lahir',
            'thn_lahir' => 'Thn Lahir',
            'jenkel' => 'Jenkel',
            'asal_negara' => 'Asal Negara',
            'tgl_masuk' => 'Tgl Masuk',
            'bln_masuk' => 'Bln Masuk',
            'thn_masuk' => 'Thn Masuk',
            'status_mhs' => 'Status Mhs',
            'status_mhs2' => 'Status Mhs 2',
            'tgl_status' => 'Tgl Status',
            'bln_status' => 'Bln Status',
            'thn_status' => 'Thn Status',
            'jenjang_pendidikan' => 'Jenjang Pendidikan',
            'semester' => 'Semester',
            'asal_sekolah' => 'Asal Sekolah',
            'ipk' => 'Ipk',
            'penghasilan_ortu' => 'Penghasilan Ortu',
            'pekerjaan_ayah' => 'Pekerjaan Ayah',
            'pekerjaan_ibu' => 'Pekerjaan Ibu',
            'pendidikan_ayah' => 'Pendidikan Ayah',
            'pendidikan_ibu' => 'Pendidikan Ibu',
            'beasiswa_miskin' => 'Beasiswa Miskin',
            'beasiswa_bidikmisi' => 'Beasiswa Bidikmisi',
            'beasiswa_lain' => 'Beasiswa Lain',
            'alamat' => 'Alamat',
            'kabupaten' => 'Kabupaten',
            'propinsi_kode' => 'Propinsi Kode',
            'propinsi_nama' => 'Propinsi Nama',
        ];
    }
}
