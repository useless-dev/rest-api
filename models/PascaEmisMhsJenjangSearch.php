<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PascaEmisMhsJenjang;

/**
 * PascaEmisMhsJenjangSearch represents the model behind the search form of `app\models\PascaEmisMhsJenjang`.
 */
class PascaEmisMhsJenjangSearch extends PascaEmisMhsJenjang
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdmhsjenjang', 'jml_laki', 'jml_perempuan', 'total'], 'integer'],
            [['tahun_akademik', 'jenjang'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PascaEmisMhsJenjang::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdmhsjenjang' => $this->kdmhsjenjang,
            'jml_laki' => $this->jml_laki,
            'jml_perempuan' => $this->jml_perempuan,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jenjang', $this->jenjang]);

        return $dataProvider;
    }
}
