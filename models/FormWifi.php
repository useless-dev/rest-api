<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_wifi".
 *
 * @property int $id
 * @property string $npm
 * @property string $semester
 * @property string $kdprodi
 * @property string $tahun
 * @property string $periode
 * @property string $tgl_daftar
 * @property string $username
 * @property string $password
 * @property string $aktif
 */
class FormWifi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_wifi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar'], 'safe'],
            [['aktif'], 'string'],
            [['npm', 'username'], 'string', 'max' => 15],
            [['semester'], 'string', 'max' => 5],
            [['kdprodi', 'tahun'], 'string', 'max' => 20],
            [['periode'], 'string', 'max' => 10],
            [['password'], 'string', 'max' => 8],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'tgl_daftar' => 'Tgl Daftar',
            'username' => 'Username',
            'password' => 'Password',
            'aktif' => 'Aktif',
        ];
    }
}
