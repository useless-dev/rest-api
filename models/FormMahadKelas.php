<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_mahad_kelas".
 *
 * @property int $idkelas
 * @property string $kelas
 * @property string $jumlah_max
 * @property string $jumlah_mhs
 */
class FormMahadKelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_mahad_kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kelas'], 'string', 'max' => 15],
            [['jumlah_max'], 'string', 'max' => 2],
            [['jumlah_mhs'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idkelas' => 'Idkelas',
            'kelas' => 'Kelas',
            'jumlah_max' => 'Jumlah Max',
            'jumlah_mhs' => 'Jumlah Mhs',
        ];
    }
}
