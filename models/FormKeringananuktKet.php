<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_keringananukt_ket".
 *
 * @property int $kdbeasiswa
 * @property string|null $beasiswa
 * @property string|null $keterangan
 */
class FormKeringananuktKet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_keringananukt_ket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['beasiswa'], 'string', 'max' => 100],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdbeasiswa' => 'Kdbeasiswa',
            'beasiswa' => 'Beasiswa',
            'keterangan' => 'Keterangan',
        ];
    }
}
