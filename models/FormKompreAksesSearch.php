<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormKompreAkses;

/**
 * FormKompreAksesSearch represents the model behind the search form of `app\models\FormKompreAkses`.
 */
class FormKompreAksesSearch extends FormKompreAkses
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdakses'], 'integer'],
            [['mulai', 'selesai', 'tahun', 'jns_semester', 'kdjur', 'total_sks', 'ipk'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKompreAkses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdakses' => $this->kdakses,
            'mulai' => $this->mulai,
            'selesai' => $this->selesai,
        ]);

        $query->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'total_sks', $this->total_sks])
            ->andFilterWhere(['like', 'ipk', $this->ipk]);

        return $dataProvider;
    }
}
