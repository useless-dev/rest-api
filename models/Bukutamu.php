<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bukutamu".
 *
 * @property int $no
 * @property string|null $pengirim
 * @property string|null $email
 * @property string|null $website
 * @property string|null $komentar
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string|null $balas
 * @property string|null $status
 */
class Bukutamu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bukutamu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pengirim'], 'string', 'max' => 35],
            [['email', 'website'], 'string', 'max' => 40],
            [['komentar', 'balas'], 'string', 'max' => 255],
            [['tgl', 'wkt'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'pengirim' => 'Pengirim',
            'email' => 'Email',
            'website' => 'Website',
            'komentar' => 'Komentar',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'balas' => 'Balas',
            'status' => 'Status',
        ];
    }
}
