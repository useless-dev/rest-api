<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmisMasterJenjang;

/**
 * EmisMasterJenjangSearch represents the model behind the search form of `app\models\EmisMasterJenjang`.
 */
class EmisMasterJenjangSearch extends EmisMasterJenjang
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdjenjang'], 'integer'],
            [['jenjang'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmisMasterJenjang::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdjenjang' => $this->kdjenjang,
        ]);

        $query->andFilterWhere(['like', 'jenjang', $this->jenjang]);

        return $dataProvider;
    }
}
