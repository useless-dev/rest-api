<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EdomJadwal;

/**
 * EdomJadwalSearch represents the model behind the search form of `app\models\EdomJadwal`.
 */
class EdomJadwalSearch extends EdomJadwal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdakses'], 'integer'],
            [['mulai', 'selesai', 'tahun', 'periode', 'tahun_akademik', 'jns_semester', 'syarat_smt', 'kuota', 'kdfakultas', 'warek1', 'nip_warek1', 'ka_tipd', 'nip_tipd', 'ttd_warek1', 'ttd_katipd'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EdomJadwal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdakses' => $this->kdakses,
            'mulai' => $this->mulai,
            'selesai' => $this->selesai,
        ]);

        $query->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'periode', $this->periode])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'syarat_smt', $this->syarat_smt])
            ->andFilterWhere(['like', 'kuota', $this->kuota])
            ->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas])
            ->andFilterWhere(['like', 'warek1', $this->warek1])
            ->andFilterWhere(['like', 'nip_warek1', $this->nip_warek1])
            ->andFilterWhere(['like', 'ka_tipd', $this->ka_tipd])
            ->andFilterWhere(['like', 'nip_tipd', $this->nip_tipd])
            ->andFilterWhere(['like', 'ttd_warek1', $this->ttd_warek1])
            ->andFilterWhere(['like', 'ttd_katipd', $this->ttd_katipd]);

        return $dataProvider;
    }
}
