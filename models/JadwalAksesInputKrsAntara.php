<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jadwal_akses_input_krs_antara".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string|null $tahun
 */
class JadwalAksesInputKrsAntara extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jadwal_akses_input_krs_antara';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai', 'tahun'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
        ];
    }
}
