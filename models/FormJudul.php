<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_judul".
 *
 * @property int $kdjudul
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $smt
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $total_sks
 * @property string|null $ipk
 * @property string|null $predikat
 * @property string|null $kdpa
 * @property string|null $judul_ta1
 * @property string|null $judul_ta2
 * @property string|null $tgl_daftar
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $thn
 */
class FormJudul extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_judul';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_ta1', 'judul_ta2'], 'string'],
            [['tgl_daftar', 'thn'], 'safe'],
            [['npm', 'kdjur', 'kdprodi', 'ipk'], 'string', 'max' => 20],
            [['nama', 'smt', 'predikat'], 'string', 'max' => 35],
            [['total_sks'], 'string', 'max' => 3],
            [['kdpa'], 'string', 'max' => 255],
            [['tahun'], 'string', 'max' => 9],
            [['jns_semester'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdjudul' => 'Kdjudul',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'smt' => 'Smt',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'total_sks' => 'Total Sks',
            'ipk' => 'Ipk',
            'predikat' => 'Predikat',
            'kdpa' => 'Kdpa',
            'judul_ta1' => 'Judul Ta 1',
            'judul_ta2' => 'Judul Ta 2',
            'tgl_daftar' => 'Tgl Daftar',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'thn' => 'Thn',
        ];
    }
}
