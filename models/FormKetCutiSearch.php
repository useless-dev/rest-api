<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormKetCuti;

/**
 * FormKetCutiSearch represents the model behind the search form of `app\models\FormKetCuti`.
 */
class FormKetCutiSearch extends FormKetCuti
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdketcuti'], 'integer'],
            [['jns_ket_cuti', 'untuk_keperluan', 'npm', 'nama', 'smt', 'kdjur', 'kdprodi', 'nama_ortu', 'nip_ortu', 'pangkat_gol_ortu', 'instansi_ortu', 'no_surat', 'tgl_surat', 'tgl_daftar', 'tahun', 'jns_semester', 'thn', 'nama_pejabat', 'nip_pejabat', 'pangkat_gol_pejabat', 'smt_1', 'tgl_mulai', 'tgl_berakhir'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKetCuti::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdketcuti' => $this->kdketcuti,
            'tgl_surat' => $this->tgl_surat,
            'tgl_daftar' => $this->tgl_daftar,
            'thn' => $this->thn,
            'tgl_mulai' => $this->tgl_mulai,
            'tgl_berakhir' => $this->tgl_berakhir,
        ]);

        $query->andFilterWhere(['like', 'jns_ket_cuti', $this->jns_ket_cuti])
            ->andFilterWhere(['like', 'untuk_keperluan', $this->untuk_keperluan])
            ->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'smt', $this->smt])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'nama_ortu', $this->nama_ortu])
            ->andFilterWhere(['like', 'nip_ortu', $this->nip_ortu])
            ->andFilterWhere(['like', 'pangkat_gol_ortu', $this->pangkat_gol_ortu])
            ->andFilterWhere(['like', 'instansi_ortu', $this->instansi_ortu])
            ->andFilterWhere(['like', 'no_surat', $this->no_surat])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'nama_pejabat', $this->nama_pejabat])
            ->andFilterWhere(['like', 'nip_pejabat', $this->nip_pejabat])
            ->andFilterWhere(['like', 'pangkat_gol_pejabat', $this->pangkat_gol_pejabat])
            ->andFilterWhere(['like', 'smt_1', $this->smt_1]);

        return $dataProvider;
    }
}
