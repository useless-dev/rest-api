<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emis_master_jns_semester".
 *
 * @property int $kdjnssemester
 * @property string|null $jns_semester
 */
class EmisMasterJnsSemester extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emis_master_jns_semester';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jns_semester'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdjnssemester' => 'Kdjnssemester',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
