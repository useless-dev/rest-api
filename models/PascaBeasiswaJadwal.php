<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_beasiswa_jadwal".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string|null $tahun
 * @property string|null $kdbeasiswa
 * @property string|null $ipk_minimal
 */
class PascaBeasiswaJadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_beasiswa_jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai', 'tahun'], 'string', 'max' => 30],
            [['kdbeasiswa', 'ipk_minimal'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'kdbeasiswa' => 'Kdbeasiswa',
            'ipk_minimal' => 'Ipk Minimal',
        ];
    }
}
