<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_batasan_perkelas".
 *
 * @property int $kdbatasan
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property int|null $jml
 * @property string|null $jns_semester
 * @property string|null $tahun
 */
class PascaBatasanPerkelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_batasan_perkelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jml'], 'integer'],
            [['kdjur', 'jns_semester'], 'string', 'max' => 20],
            [['kdprodi', 'tahun'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdbatasan' => 'Kdbatasan',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'jml' => 'Jml',
            'jns_semester' => 'Jns Semester',
            'tahun' => 'Tahun',
        ];
    }
}
