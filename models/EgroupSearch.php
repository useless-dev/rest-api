<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Egroup;

/**
 * EgroupSearch represents the model behind the search form of `app\models\Egroup`.
 */
class EgroupSearch extends Egroup
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_group'], 'integer'],
            [['group', 'p1', 'p2', 'p3', 'p4', 'p5', 'CreatedDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Egroup::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_group' => $this->id_group,
        ]);

        $query->andFilterWhere(['like', 'group', $this->group])
            ->andFilterWhere(['like', 'p1', $this->p1])
            ->andFilterWhere(['like', 'p2', $this->p2])
            ->andFilterWhere(['like', 'p3', $this->p3])
            ->andFilterWhere(['like', 'p4', $this->p4])
            ->andFilterWhere(['like', 'p5', $this->p5])
            ->andFilterWhere(['like', 'CreatedDate', $this->CreatedDate]);

        return $dataProvider;
    }
}
