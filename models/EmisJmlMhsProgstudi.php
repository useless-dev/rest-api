<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emis_jml_mhs_progstudi".
 *
 * @property int $kdejmp
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $jenjang
 * @property int|null $jml_laki
 * @property int|null $jml_perempuan
 * @property int|null $jml_total
 * @property string|null $tahun_akademik
 */
class EmisJmlMhsProgstudi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emis_jml_mhs_progstudi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jml_laki', 'jml_perempuan', 'jml_total'], 'integer'],
            [['kdjur', 'kdprodi', 'jenjang', 'tahun_akademik'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdejmp' => 'Kdejmp',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'jenjang' => 'Jenjang',
            'jml_laki' => 'Jml Laki',
            'jml_perempuan' => 'Jml Perempuan',
            'jml_total' => 'Jml Total',
            'tahun_akademik' => 'Tahun Akademik',
        ];
    }
}
