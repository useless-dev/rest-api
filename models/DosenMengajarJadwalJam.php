<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dosen_mengajar_jadwal_jam".
 *
 * @property int $idjam
 * @property int|null $kdjam
 * @property string|null $durasi
 * @property string|null $jam_tampil
 */
class DosenMengajarJadwalJam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dosen_mengajar_jadwal_jam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdjam'], 'integer'],
            [['durasi', 'jam_tampil'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idjam' => 'Idjam',
            'kdjam' => 'Kdjam',
            'durasi' => 'Durasi',
            'jam_tampil' => 'Jam Tampil',
        ];
    }
}
