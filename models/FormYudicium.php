<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_yudicium".
 *
 * @property int $kdydsm
 * @property string $npm
 * @property string $kdprodi
 * @property string $tahun
 * @property string $jns_semester
 * @property string $tgl_daftar
 * @property string $jam_daftar
 * @property string $tgl_lulus
 * @property string $judul_ta
 * @property string $ipk
 * @property string $predikat
 */
class FormYudicium extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_yudicium';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_ta'], 'required'],
            [['judul_ta'], 'string'],
            [['npm'], 'string', 'max' => 15],
            [['kdprodi', 'tahun'], 'string', 'max' => 20],
            [['jns_semester'], 'string', 'max' => 10],
            [['tgl_daftar', 'jam_daftar', 'tgl_lulus'], 'string', 'max' => 30],
            [['ipk'], 'string', 'max' => 5],
            [['predikat'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdydsm' => 'Kdydsm',
            'npm' => 'Npm',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'tgl_daftar' => 'Tgl Daftar',
            'jam_daftar' => 'Jam Daftar',
            'tgl_lulus' => 'Tgl Lulus',
            'judul_ta' => 'Judul Ta',
            'ipk' => 'Ipk',
            'predikat' => 'Predikat',
        ];
    }
}
