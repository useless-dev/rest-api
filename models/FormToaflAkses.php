<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_toafl_akses".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $periode
 */
class FormToaflAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_toafl_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai', 'tahun'], 'string', 'max' => 30],
            [['jns_semester'], 'string', 'max' => 15],
            [['periode'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'periode' => 'Periode',
        ];
    }
}
