<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs_kelas".
 *
 * @property int $krskelas
 * @property string $kdmk
 * @property string $kdprodi
 * @property string $tahun
 * @property string $jns_semester
 * @property string $kelas
 * @property int|null $jumlah
 * @property int|null $kdfakultas
 */
class KrsKelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs_kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jumlah', 'kdfakultas'], 'integer'],
            [['kdmk', 'kdprodi', 'tahun', 'jns_semester'], 'string', 'max' => 30],
            [['kelas'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'krskelas' => 'Krskelas',
            'kdmk' => 'Kdmk',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'kelas' => 'Kelas',
            'jumlah' => 'Jumlah',
            'kdfakultas' => 'Kdfakultas',
        ];
    }
}
