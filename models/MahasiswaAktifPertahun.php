<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa_aktif_pertahun".
 *
 * @property int $kdmhsstatuspertahun
 * @property string $npm
 * @property string|null $nama
 * @property string|null $jenkel
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property int|null $semester
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property int|null $total_sks
 * @property int|null $total_nilai
 * @property float|null $ips
 * @property int|null $kdfakultas
 */
class MahasiswaAktifPertahun extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa_aktif_pertahun';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester', 'total_sks', 'total_nilai', 'kdfakultas'], 'integer'],
            [['ips'], 'number'],
            [['npm'], 'string', 'max' => 11],
            [['nama'], 'string', 'max' => 100],
            [['jenkel', 'kdjur', 'kdprodi', 'tahun', 'jns_semester'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdmhsstatuspertahun' => 'Kdmhsstatuspertahun',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'jenkel' => 'Jenkel',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'semester' => 'Semester',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'total_sks' => 'Total Sks',
            'total_nilai' => 'Total Nilai',
            'ips' => 'Ips',
            'kdfakultas' => 'Kdfakultas',
        ];
    }
}
