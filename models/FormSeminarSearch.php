<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormSeminar;

/**
 * FormSeminarSearch represents the model behind the search form of `app\models\FormSeminar`.
 */
class FormSeminarSearch extends FormSeminar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['npm', 'semester', 'kdjur', 'kdprodi', 'tahun', 'jns_semester', 'tgl_daftar', 'wkt_daftar', 'bulan_daftar', 'pembimbing_1', 'pembimbing_2', 'judul_ta', 'linksyarat', 'linkskripsi'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormSeminar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_daftar' => $this->tgl_daftar,
            'wkt_daftar' => $this->wkt_daftar,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'bulan_daftar', $this->bulan_daftar])
            ->andFilterWhere(['like', 'pembimbing_1', $this->pembimbing_1])
            ->andFilterWhere(['like', 'pembimbing_2', $this->pembimbing_2])
            ->andFilterWhere(['like', 'judul_ta', $this->judul_ta])
            ->andFilterWhere(['like', 'linksyarat', $this->linksyarat])
            ->andFilterWhere(['like', 'linkskripsi', $this->linkskripsi]);

        return $dataProvider;
    }
}
