<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmisJmlMhsProgstudi;

/**
 * EmisJmlMhsProgstudiSearch represents the model behind the search form of `app\models\EmisJmlMhsProgstudi`.
 */
class EmisJmlMhsProgstudiSearch extends EmisJmlMhsProgstudi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdejmp', 'jml_laki', 'jml_perempuan', 'jml_total'], 'integer'],
            [['kdjur', 'kdprodi', 'jenjang', 'tahun_akademik'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmisJmlMhsProgstudi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdejmp' => $this->kdejmp,
            'jml_laki' => $this->jml_laki,
            'jml_perempuan' => $this->jml_perempuan,
            'jml_total' => $this->jml_total,
        ]);

        $query->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'jenjang', $this->jenjang])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik]);

        return $dataProvider;
    }
}
