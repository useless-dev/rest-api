<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmisMahasiswaReguler;

/**
 * EmisMahasiswaRegulerSearch represents the model behind the search form of `app\models\EmisMahasiswaReguler`.
 */
class EmisMahasiswaRegulerSearch extends EmisMahasiswaReguler
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdmhs', 'semester'], 'integer'],
            [['tahun', 'jns_semester', 'kdjur', 'kdprodi', 'nama', 'npm', 'tmp_lahir', 'tgl_lahir', 'bln_lahir', 'thn_lahir', 'jenkel', 'asal_negara', 'tgl_masuk', 'bln_masuk', 'thn_masuk', 'status_mhs', 'status_mhs2', 'tgl_status', 'bln_status', 'thn_status', 'jenjang_pendidikan', 'asal_sekolah', 'penghasilan_ortu', 'pekerjaan_ayah', 'pekerjaan_ibu', 'pendidikan_ayah', 'pendidikan_ibu', 'beasiswa_miskin', 'beasiswa_bidikmisi', 'beasiswa_lain', 'alamat', 'kabupaten', 'propinsi_kode', 'propinsi_nama'], 'safe'],
            [['ipk'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmisMahasiswaReguler::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdmhs' => $this->kdmhs,
            'semester' => $this->semester,
            'ipk' => $this->ipk,
        ]);

        $query->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'tmp_lahir', $this->tmp_lahir])
            ->andFilterWhere(['like', 'tgl_lahir', $this->tgl_lahir])
            ->andFilterWhere(['like', 'bln_lahir', $this->bln_lahir])
            ->andFilterWhere(['like', 'thn_lahir', $this->thn_lahir])
            ->andFilterWhere(['like', 'jenkel', $this->jenkel])
            ->andFilterWhere(['like', 'asal_negara', $this->asal_negara])
            ->andFilterWhere(['like', 'tgl_masuk', $this->tgl_masuk])
            ->andFilterWhere(['like', 'bln_masuk', $this->bln_masuk])
            ->andFilterWhere(['like', 'thn_masuk', $this->thn_masuk])
            ->andFilterWhere(['like', 'status_mhs', $this->status_mhs])
            ->andFilterWhere(['like', 'status_mhs2', $this->status_mhs2])
            ->andFilterWhere(['like', 'tgl_status', $this->tgl_status])
            ->andFilterWhere(['like', 'bln_status', $this->bln_status])
            ->andFilterWhere(['like', 'thn_status', $this->thn_status])
            ->andFilterWhere(['like', 'jenjang_pendidikan', $this->jenjang_pendidikan])
            ->andFilterWhere(['like', 'asal_sekolah', $this->asal_sekolah])
            ->andFilterWhere(['like', 'penghasilan_ortu', $this->penghasilan_ortu])
            ->andFilterWhere(['like', 'pekerjaan_ayah', $this->pekerjaan_ayah])
            ->andFilterWhere(['like', 'pekerjaan_ibu', $this->pekerjaan_ibu])
            ->andFilterWhere(['like', 'pendidikan_ayah', $this->pendidikan_ayah])
            ->andFilterWhere(['like', 'pendidikan_ibu', $this->pendidikan_ibu])
            ->andFilterWhere(['like', 'beasiswa_miskin', $this->beasiswa_miskin])
            ->andFilterWhere(['like', 'beasiswa_bidikmisi', $this->beasiswa_bidikmisi])
            ->andFilterWhere(['like', 'beasiswa_lain', $this->beasiswa_lain])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'kabupaten', $this->kabupaten])
            ->andFilterWhere(['like', 'propinsi_kode', $this->propinsi_kode])
            ->andFilterWhere(['like', 'propinsi_nama', $this->propinsi_nama]);

        return $dataProvider;
    }
}
