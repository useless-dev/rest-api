<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orangtua_login".
 *
 * @property int $kdortu
 * @property string|null $npm
 * @property string|null $username
 * @property string|null $password
 * @property string|null $sessions
 * @property string|null $nama
 */
class OrangtuaLogin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orangtua_login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm', 'username', 'password', 'sessions', 'nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdortu' => 'Kdortu',
            'npm' => 'Npm',
            'username' => 'Username',
            'password' => 'Password',
            'sessions' => 'Sessions',
            'nama' => 'Nama',
        ];
    }
}
