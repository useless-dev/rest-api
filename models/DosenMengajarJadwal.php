<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dosen_mengajar_jadwal".
 *
 * @property int $kdjadwal
 * @property string|null $kdmk
 * @property string|null $kurikulum
 * @property string|null $namamk
 * @property string|null $kddosen
 * @property string|null $namadosen
 * @property string|null $hari
 * @property string|null $jam_mulai
 * @property string|null $jam_selesai
 * @property string|null $tahun_akademik
 * @property string|null $jns_semester
 * @property string $kdprodi
 * @property string $kdkelas
 * @property string $kelas
 * @property string|null $koderuang
 * @property int|null $kdjam
 * @property string|null $waktu
 * @property int|null $kdfakultas
 * @property string|null $semester
 * @property string|null $sks
 */
class DosenMengajarJadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dosen_mengajar_jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jam_mulai', 'jam_selesai'], 'safe'],
            [['kdjam', 'kdfakultas'], 'integer'],
            [['kdmk', 'kurikulum', 'kddosen', 'hari', 'tahun_akademik', 'jns_semester', 'koderuang', 'semester', 'sks'], 'string', 'max' => 35],
            [['namamk', 'namadosen', 'waktu'], 'string', 'max' => 100],
            [['kdprodi'], 'string', 'max' => 15],
            [['kdkelas', 'kelas'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdjadwal' => 'Kdjadwal',
            'kdmk' => 'Kdmk',
            'kurikulum' => 'Kurikulum',
            'namamk' => 'Namamk',
            'kddosen' => 'Kddosen',
            'namadosen' => 'Namadosen',
            'hari' => 'Hari',
            'jam_mulai' => 'Jam Mulai',
            'jam_selesai' => 'Jam Selesai',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'kdprodi' => 'Kdprodi',
            'kdkelas' => 'Kdkelas',
            'kelas' => 'Kelas',
            'koderuang' => 'Koderuang',
            'kdjam' => 'Kdjam',
            'waktu' => 'Waktu',
            'kdfakultas' => 'Kdfakultas',
            'semester' => 'Semester',
            'sks' => 'Sks',
        ];
    }
}
