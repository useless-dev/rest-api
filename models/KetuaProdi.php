<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ketua_prodi".
 *
 * @property int $idprodi
 * @property string|null $kdprodi
 * @property string $sebutan
 * @property string|null $nama
 * @property string $nip
 * @property string|null $tahun
 * @property string $jns_semester
 */
class KetuaProdi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ketua_prodi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdprodi', 'tahun'], 'string', 'max' => 20],
            [['sebutan'], 'string', 'max' => 100],
            [['nama'], 'string', 'max' => 50],
            [['nip'], 'string', 'max' => 30],
            [['jns_semester'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idprodi' => 'Idprodi',
            'kdprodi' => 'Kdprodi',
            'sebutan' => 'Sebutan',
            'nama' => 'Nama',
            'nip' => 'Nip',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
        ];
    }
}
