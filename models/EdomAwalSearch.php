<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EdomAwal;

/**
 * EdomAwalSearch represents the model behind the search form of `app\models\EdomAwal`.
 */
class EdomAwalSearch extends EdomAwal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id'], 'integer'],
            [['npm', 'tahun', 'semester', 'p1', 'kdmk', 'kddosen', 'kurikulum', 'kdprodi', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10', 'p11', 'p12', 'p13', 'p14', 'p15', 'p16', 'p17', 'p18', 'p19', 'p20', 'p21', 'p22', 'p23', 'p24', 'p25', 'p26', 'p27', 'p28', 'kelas', 'jns_semester'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EdomAwal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Id' => $this->Id,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'p1', $this->p1])
            ->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'kddosen', $this->kddosen])
            ->andFilterWhere(['like', 'kurikulum', $this->kurikulum])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'p2', $this->p2])
            ->andFilterWhere(['like', 'p3', $this->p3])
            ->andFilterWhere(['like', 'p4', $this->p4])
            ->andFilterWhere(['like', 'p5', $this->p5])
            ->andFilterWhere(['like', 'p6', $this->p6])
            ->andFilterWhere(['like', 'p7', $this->p7])
            ->andFilterWhere(['like', 'p8', $this->p8])
            ->andFilterWhere(['like', 'p9', $this->p9])
            ->andFilterWhere(['like', 'p10', $this->p10])
            ->andFilterWhere(['like', 'p11', $this->p11])
            ->andFilterWhere(['like', 'p12', $this->p12])
            ->andFilterWhere(['like', 'p13', $this->p13])
            ->andFilterWhere(['like', 'p14', $this->p14])
            ->andFilterWhere(['like', 'p15', $this->p15])
            ->andFilterWhere(['like', 'p16', $this->p16])
            ->andFilterWhere(['like', 'p17', $this->p17])
            ->andFilterWhere(['like', 'p18', $this->p18])
            ->andFilterWhere(['like', 'p19', $this->p19])
            ->andFilterWhere(['like', 'p20', $this->p20])
            ->andFilterWhere(['like', 'p21', $this->p21])
            ->andFilterWhere(['like', 'p22', $this->p22])
            ->andFilterWhere(['like', 'p23', $this->p23])
            ->andFilterWhere(['like', 'p24', $this->p24])
            ->andFilterWhere(['like', 'p25', $this->p25])
            ->andFilterWhere(['like', 'p26', $this->p26])
            ->andFilterWhere(['like', 'p27', $this->p27])
            ->andFilterWhere(['like', 'p28', $this->p28])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
