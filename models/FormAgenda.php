<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_agenda".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string $tahun
 * @property string $periode
 * @property string $agenda
 * @property string $kdjur
 */
class FormAgenda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_agenda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai'], 'safe'],
            [['tahun'], 'string', 'max' => 30],
            [['periode'], 'string', 'max' => 6],
            [['agenda'], 'string', 'max' => 20],
            [['kdjur'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'agenda' => 'Agenda',
            'kdjur' => 'Kdjur',
        ];
    }
}
