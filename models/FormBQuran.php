<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_b_quran".
 *
 * @property int $id
 * @property string $npm
 * @property string $semester
 * @property string $kdprodi
 * @property string $tahun
 * @property string $jns_semester
 * @property string $level
 * @property string $tgl_daftar
 * @property string $wkt_daftar
 * @property string $kddosen
 * @property string $nilai_1
 * @property string $nilai_2
 * @property string $nilai_3
 * @property string $nilai_akhir
 * @property string $no_sertifikat
 */
class FormBQuran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_b_quran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar', 'wkt_daftar'], 'safe'],
            [['npm'], 'string', 'max' => 15],
            [['semester', 'nilai_akhir'], 'string', 'max' => 5],
            [['kdprodi', 'tahun'], 'string', 'max' => 20],
            [['jns_semester', 'level'], 'string', 'max' => 10],
            [['kddosen', 'nilai_1', 'nilai_2', 'nilai_3'], 'string', 'max' => 4],
            [['no_sertifikat'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'level' => 'Level',
            'tgl_daftar' => 'Tgl Daftar',
            'wkt_daftar' => 'Wkt Daftar',
            'kddosen' => 'Kddosen',
            'nilai_1' => 'Nilai  1',
            'nilai_2' => 'Nilai  2',
            'nilai_3' => 'Nilai  3',
            'nilai_akhir' => 'Nilai Akhir',
            'no_sertifikat' => 'No Sertifikat',
        ];
    }
}
