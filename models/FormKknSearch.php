<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormKkn;

/**
 * FormKknSearch represents the model behind the search form of `app\models\FormKkn`.
 */
class FormKknSearch extends FormKkn
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['npm', 'semester', 'kdprodi', 'tahun', 'periode', 'ukuran_jaket', 'tgl_daftar', 'status_nikah', 'nama_suami', 'kerja_suami', 'ket_sakit', 'alamat_kost', 'total_sks', 'ipk_sementara', 'nilai_akhir'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKkn::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_daftar' => $this->tgl_daftar,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'periode', $this->periode])
            ->andFilterWhere(['like', 'ukuran_jaket', $this->ukuran_jaket])
            ->andFilterWhere(['like', 'status_nikah', $this->status_nikah])
            ->andFilterWhere(['like', 'nama_suami', $this->nama_suami])
            ->andFilterWhere(['like', 'kerja_suami', $this->kerja_suami])
            ->andFilterWhere(['like', 'ket_sakit', $this->ket_sakit])
            ->andFilterWhere(['like', 'alamat_kost', $this->alamat_kost])
            ->andFilterWhere(['like', 'total_sks', $this->total_sks])
            ->andFilterWhere(['like', 'ipk_sementara', $this->ipk_sementara])
            ->andFilterWhere(['like', 'nilai_akhir', $this->nilai_akhir]);

        return $dataProvider;
    }
}
