<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_yudicium_akses".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string $tahun
 * @property string $periode
 * @property string $kuota
 * @property string $kdjur
 */
class FormYudiciumAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_yudicium_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai'], 'safe'],
            [['tahun'], 'string', 'max' => 30],
            [['periode'], 'string', 'max' => 6],
            [['kuota'], 'string', 'max' => 10],
            [['kdjur'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'kuota' => 'Kuota',
            'kdjur' => 'Kdjur',
        ];
    }
}
