<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormKeringananuktKet;

/**
 * FormKeringananuktKetSearch represents the model behind the search form of `app\models\FormKeringananuktKet`.
 */
class FormKeringananuktKetSearch extends FormKeringananuktKet
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdbeasiswa'], 'integer'],
            [['beasiswa', 'keterangan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKeringananuktKet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdbeasiswa' => $this->kdbeasiswa,
        ]);

        $query->andFilterWhere(['like', 'beasiswa', $this->beasiswa])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
