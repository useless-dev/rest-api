<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MatakuliahUrutan;

/**
 * MatakuliahUrutanSearch represents the model behind the search form of `app\models\MatakuliahUrutan`.
 */
class MatakuliahUrutanSearch extends MatakuliahUrutan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no', 'urutan'], 'integer'],
            [['kdmk', 'semester', 'kdprodi', 'namamk', 'sks', 'kdfakultas'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatakuliahUrutan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'no' => $this->no,
            'urutan' => $this->urutan,
        ]);

        $query->andFilterWhere(['like', 'kdmk', $this->kdmk])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'namamk', $this->namamk])
            ->andFilterWhere(['like', 'sks', $this->sks])
            ->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas]);

        return $dataProvider;
    }
}
