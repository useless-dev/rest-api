<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormJudul;

/**
 * FormJudulSearch represents the model behind the search form of `app\models\FormJudul`.
 */
class FormJudulSearch extends FormJudul
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdjudul'], 'integer'],
            [['npm', 'nama', 'smt', 'kdjur', 'kdprodi', 'total_sks', 'ipk', 'predikat', 'kdpa', 'judul_ta1', 'judul_ta2', 'tgl_daftar', 'tahun', 'jns_semester', 'thn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormJudul::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdjudul' => $this->kdjudul,
            'tgl_daftar' => $this->tgl_daftar,
            'thn' => $this->thn,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'smt', $this->smt])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'total_sks', $this->total_sks])
            ->andFilterWhere(['like', 'ipk', $this->ipk])
            ->andFilterWhere(['like', 'predikat', $this->predikat])
            ->andFilterWhere(['like', 'kdpa', $this->kdpa])
            ->andFilterWhere(['like', 'judul_ta1', $this->judul_ta1])
            ->andFilterWhere(['like', 'judul_ta2', $this->judul_ta2])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
