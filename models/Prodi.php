<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prodi".
 *
 * @property int $idprodi
 * @property string $kdprodi
 * @property string|null $kdjur
 * @property string|null $namaprodi
 * @property string|null $jenjang
 * @property string|null $kode_jenjang
 * @property string|null $kdprodi_pdpt
 * @property int|null $kdfakultas
 */
class Prodi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prodi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdfakultas'], 'integer'],
            [['kdprodi'], 'string', 'max' => 35],
            [['kdjur'], 'string', 'max' => 11],
            [['namaprodi'], 'string', 'max' => 100],
            [['jenjang', 'kode_jenjang'], 'string', 'max' => 20],
            [['kdprodi_pdpt'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idprodi' => 'Idprodi',
            'kdprodi' => 'Kdprodi',
            'kdjur' => 'Kdjur',
            'namaprodi' => 'Namaprodi',
            'jenjang' => 'Jenjang',
            'kode_jenjang' => 'Kode Jenjang',
            'kdprodi_pdpt' => 'Kdprodi Pdpt',
            'kdfakultas' => 'Kdfakultas',
        ];
    }
}
