<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jadwal_daftar_ppl".
 *
 * @property int $kdakses
 * @property string|null $kdprodi
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $periode
 */
class JadwalDaftarPpl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jadwal_daftar_ppl';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdprodi', 'jns_semester'], 'string', 'max' => 15],
            [['mulai', 'selesai', 'tahun'], 'string', 'max' => 30],
            [['periode'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'kdprodi' => 'Kdprodi',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'periode' => 'Periode',
        ];
    }
}
