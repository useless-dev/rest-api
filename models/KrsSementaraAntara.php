<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs_sementara_antara".
 *
 * @property int $kdkrs
 * @property string $kdmk
 * @property string $sks
 * @property string $ip
 * @property string $tgl
 * @property string $wkt
 * @property string $kelas
 * @property string $kddosen
 */
class KrsSementaraAntara extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs_sementara_antara';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kddosen'], 'required'],
            [['kdmk'], 'string', 'max' => 35],
            [['sks'], 'string', 'max' => 10],
            [['ip'], 'string', 'max' => 30],
            [['tgl', 'wkt'], 'string', 'max' => 100],
            [['kelas'], 'string', 'max' => 5],
            [['kddosen'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkrs' => 'Kdkrs',
            'kdmk' => 'Kdmk',
            'sks' => 'Sks',
            'ip' => 'Ip',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'kelas' => 'Kelas',
            'kddosen' => 'Kddosen',
        ];
    }
}
