<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HistoryPembayaran;

/**
 * HistoryPembayaranSearch represents the model behind the search form of `app\models\HistoryPembayaran`.
 */
class HistoryPembayaranSearch extends HistoryPembayaran
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdpembayaran'], 'integer'],
            [['kdkeuangan', 'npm', 'kdtagihan', 'tgl_tagihan', 'tgl_bayar', 'jml_tagihan', 'denda', 'status', 'keterangan', 'file', 'status_hapus'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HistoryPembayaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdpembayaran' => $this->kdpembayaran,
        ]);

        $query->andFilterWhere(['like', 'kdkeuangan', $this->kdkeuangan])
            ->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'kdtagihan', $this->kdtagihan])
            ->andFilterWhere(['like', 'tgl_tagihan', $this->tgl_tagihan])
            ->andFilterWhere(['like', 'tgl_bayar', $this->tgl_bayar])
            ->andFilterWhere(['like', 'jml_tagihan', $this->jml_tagihan])
            ->andFilterWhere(['like', 'denda', $this->denda])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'status_hapus', $this->status_hapus]);

        return $dataProvider;
    }
}
