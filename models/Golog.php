<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "golog".
 *
 * @property int $kdgolog
 * @property string|null $tipe
 * @property string|null $subtipe
 * @property string|null $kduser
 * @property string|null $ip
 * @property string|null $tgl
 * @property string|null $wkt
 * @property string|null $aktivitas
 * @property string|null $lokasi
 */
class Golog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'golog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aktivitas', 'lokasi'], 'string'],
            [['tipe', 'kduser', 'ip', 'wkt'], 'string', 'max' => 30],
            [['subtipe', 'tgl'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdgolog' => 'Kdgolog',
            'tipe' => 'Tipe',
            'subtipe' => 'Subtipe',
            'kduser' => 'Kduser',
            'ip' => 'Ip',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
            'aktivitas' => 'Aktivitas',
            'lokasi' => 'Lokasi',
        ];
    }
}
