<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "okpp_jns_pangkat".
 *
 * @property int $Id
 * @property string|null $namapangkat
 */
class OkppJnsPangkat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'okpp_jns_pangkat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namapangkat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'namapangkat' => 'Namapangkat',
        ];
    }
}
