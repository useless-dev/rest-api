<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gelar".
 *
 * @property string|null $kdprodi
 * @property string|null $kode_prodi
 * @property string|null $prodi
 * @property string|null $status
 * @property string|null $jenjang
 * @property string|null $gelar
 */
class Gelar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gelar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdprodi', 'kode_prodi', 'prodi', 'status', 'jenjang', 'gelar'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdprodi' => 'Kdprodi',
            'kode_prodi' => 'Kode Prodi',
            'prodi' => 'Prodi',
            'status' => 'Status',
            'jenjang' => 'Jenjang',
            'gelar' => 'Gelar',
        ];
    }
}
