<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_komputer_dosen".
 *
 * @property int $no
 * @property string $kddosen
 * @property string $nip
 * @property string $nama
 */
class FormKomputerDosen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_komputer_dosen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kddosen'], 'string', 'max' => 4],
            [['nip', 'nama'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'kddosen' => 'Kddosen',
            'nip' => 'Nip',
            'nama' => 'Nama',
        ];
    }
}
