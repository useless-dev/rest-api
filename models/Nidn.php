<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nidn".
 *
 * @property int $Id
 * @property string|null $kddosen
 * @property string|null $nidn
 * @property string|null $status
 * @property string|null $homebase
 */
class Nidn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nidn';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kddosen', 'nidn', 'status', 'homebase'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'kddosen' => 'Kddosen',
            'nidn' => 'Nidn',
            'status' => 'Status',
            'homebase' => 'Homebase',
        ];
    }
}
