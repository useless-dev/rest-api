<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edom_jadwal".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string $tahun
 * @property string $periode
 * @property string $tahun_akademik
 * @property string $jns_semester
 * @property string $syarat_smt
 * @property string $kuota
 * @property string $kdfakultas
 * @property string $warek1
 * @property string $nip_warek1
 * @property string $ka_tipd
 * @property string $nip_tipd
 * @property string|null $ttd_warek1
 * @property string|null $ttd_katipd
 */
class EdomJadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'edom_jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai'], 'safe'],
            [['tahun', 'periode', 'kdfakultas'], 'string', 'max' => 15],
            [['tahun_akademik', 'nip_warek1', 'nip_tipd'], 'string', 'max' => 30],
            [['jns_semester'], 'string', 'max' => 6],
            [['syarat_smt'], 'string', 'max' => 3],
            [['kuota'], 'string', 'max' => 10],
            [['warek1', 'ka_tipd', 'ttd_warek1', 'ttd_katipd'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'syarat_smt' => 'Syarat Smt',
            'kuota' => 'Kuota',
            'kdfakultas' => 'Kdfakultas',
            'warek1' => 'Warek 1',
            'nip_warek1' => 'Nip Warek 1',
            'ka_tipd' => 'Ka Tipd',
            'nip_tipd' => 'Nip Tipd',
            'ttd_warek1' => 'Ttd Warek 1',
            'ttd_katipd' => 'Ttd Katipd',
        ];
    }
}
