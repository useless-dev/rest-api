<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JurusanIain;

/**
 * JurusanIainSearch represents the model behind the search form of `app\models\JurusanIain`.
 */
class JurusanIainSearch extends JurusanIain
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idprodi', 'kdfakultas'], 'integer'],
            [['kdprodi_pdpt', 'kdprodi', 'kdjur', 'namaprodi', 'jenjang', 'namajenjang', 'kode_jenjang', 'banpt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JurusanIain::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idprodi' => $this->idprodi,
            'kdfakultas' => $this->kdfakultas,
        ]);

        $query->andFilterWhere(['like', 'kdprodi_pdpt', $this->kdprodi_pdpt])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'namaprodi', $this->namaprodi])
            ->andFilterWhere(['like', 'jenjang', $this->jenjang])
            ->andFilterWhere(['like', 'namajenjang', $this->namajenjang])
            ->andFilterWhere(['like', 'kode_jenjang', $this->kode_jenjang])
            ->andFilterWhere(['like', 'banpt', $this->banpt]);

        return $dataProvider;
    }
}
