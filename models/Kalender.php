<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kalender".
 *
 * @property int $Id
 * @property string|null $ta
 * @property string|null $sm
 * @property string|null $ukt
 * @property string|null $krs
 * @property string|null $nilai
 * @property string|null $smtr
 */
class Kalender extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kalender';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ta', 'sm', 'ukt', 'krs', 'nilai', 'smtr'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'ta' => 'Ta',
            'sm' => 'Sm',
            'ukt' => 'Ukt',
            'krs' => 'Krs',
            'nilai' => 'Nilai',
            'smtr' => 'Smtr',
        ];
    }
}
