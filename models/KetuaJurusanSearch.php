<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KetuaJurusan;

/**
 * KetuaJurusanSearch represents the model behind the search form of `app\models\KetuaJurusan`.
 */
class KetuaJurusanSearch extends KetuaJurusan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjur'], 'integer'],
            [['kdjur', 'sebutan', 'nama', 'nip', 'tahun', 'jns_semester'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KetuaJurusan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idjur' => $this->idjur,
        ]);

        $query->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'sebutan', $this->sebutan])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'nip', $this->nip])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
