<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_kompre_akses".
 *
 * @property int $kdakses
 * @property string|null $mulai
 * @property string|null $selesai
 * @property string $tahun
 * @property string $jns_semester
 * @property string $kdjur
 * @property string $total_sks
 * @property string $ipk
 */
class FormKompreAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_kompre_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai'], 'safe'],
            [['tahun'], 'string', 'max' => 30],
            [['jns_semester'], 'string', 'max' => 15],
            [['kdjur'], 'string', 'max' => 5],
            [['total_sks', 'ipk'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'kdjur' => 'Kdjur',
            'total_sks' => 'Total Sks',
            'ipk' => 'Ipk',
        ];
    }
}
