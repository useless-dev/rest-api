<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_munaqosyah".
 *
 * @property int $id
 * @property string $npm
 * @property string $semester
 * @property string $kdjur
 * @property string $kdprodi
 * @property string $tahun
 * @property string $jns_semester
 * @property string $tgl_daftar
 * @property string $wkt_daftar
 * @property string $bulan_daftar
 * @property string $pembimbing_1
 * @property string $pembimbing_2
 * @property string $judul_ta
 * @property string $linksyarat
 * @property string $linkskripsi
 */
class FormMunaqosyah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_munaqosyah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar', 'wkt_daftar'], 'safe'],
            [['npm'], 'string', 'max' => 15],
            [['semester'], 'string', 'max' => 5],
            [['kdjur', 'jns_semester'], 'string', 'max' => 10],
            [['kdprodi', 'tahun'], 'string', 'max' => 20],
            [['bulan_daftar'], 'string', 'max' => 25],
            [['pembimbing_1', 'pembimbing_2'], 'string', 'max' => 100],
            [['judul_ta', 'linksyarat', 'linkskripsi'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'tgl_daftar' => 'Tgl Daftar',
            'wkt_daftar' => 'Wkt Daftar',
            'bulan_daftar' => 'Bulan Daftar',
            'pembimbing_1' => 'Pembimbing  1',
            'pembimbing_2' => 'Pembimbing  2',
            'judul_ta' => 'Judul Ta',
            'linksyarat' => 'Linksyarat',
            'linkskripsi' => 'Linkskripsi',
        ];
    }
}
