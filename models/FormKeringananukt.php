<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_keringananukt".
 *
 * @property int $id
 * @property string $npm
 * @property string $semester
 * @property string $jml_ukt
 * @property string $kdjur
 * @property string $kdprodi
 * @property string $tahun
 * @property string $jns_semester
 * @property string $tgl_daftar
 * @property string $wkt_daftar
 * @property string $bulan_daftar
 * @property string $alasan_1
 * @property string $ayah_1
 * @property string $ibu_2
 * @property string $beasiswa_1
 * @property string $judul_ta
 * @property string $fotosm1
 * @property string $linkskripsi
 * @property string|null $ukt
 */
class FormKeringananukt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_keringananukt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar', 'wkt_daftar'], 'safe'],
            [['npm'], 'string', 'max' => 15],
            [['semester'], 'string', 'max' => 5],
            [['jml_ukt', 'kdjur', 'jns_semester'], 'string', 'max' => 10],
            [['kdprodi', 'tahun'], 'string', 'max' => 20],
            [['bulan_daftar'], 'string', 'max' => 25],
            [['alasan_1', 'ayah_1', 'ibu_2', 'beasiswa_1'], 'string', 'max' => 100],
            [['judul_ta', 'fotosm1', 'linkskripsi', 'ukt'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'jml_ukt' => 'Jml Ukt',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'tgl_daftar' => 'Tgl Daftar',
            'wkt_daftar' => 'Wkt Daftar',
            'bulan_daftar' => 'Bulan Daftar',
            'alasan_1' => 'Alasan  1',
            'ayah_1' => 'Ayah  1',
            'ibu_2' => 'Ibu  2',
            'beasiswa_1' => 'Beasiswa  1',
            'judul_ta' => 'Judul Ta',
            'fotosm1' => 'Fotosm 1',
            'linkskripsi' => 'Linkskripsi',
            'ukt' => 'Ukt',
        ];
    }
}
