<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_bimbingan".
 *
 * @property int $id
 * @property string $npm
 * @property string|null $nama
 * @property string $semester
 * @property string $kdjur
 * @property string $kdprodi
 * @property string $tahun
 * @property string $jns_semester
 * @property string $tgl_daftar
 * @property string $wkt_daftar
 * @property string $bulan_daftar
 * @property string $pembimbing_1
 * @property string $pembimbing_2
 * @property string $judul_ta
 * @property string $linksyarat
 * @property string $total_sks
 * @property string $ipk
 * @property string|null $no_surat
 * @property string|null $tgl_surat
 * @property string|null $thn
 */
class FormBimbingan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_bimbingan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar', 'wkt_daftar', 'tgl_surat', 'thn'], 'safe'],
            [['npm'], 'string', 'max' => 15],
            [['nama'], 'string', 'max' => 35],
            [['semester'], 'string', 'max' => 5],
            [['kdjur', 'jns_semester', 'total_sks', 'ipk'], 'string', 'max' => 10],
            [['kdprodi', 'tahun', 'no_surat'], 'string', 'max' => 20],
            [['bulan_daftar'], 'string', 'max' => 25],
            [['pembimbing_1', 'pembimbing_2'], 'string', 'max' => 100],
            [['judul_ta', 'linksyarat'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'semester' => 'Semester',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'tgl_daftar' => 'Tgl Daftar',
            'wkt_daftar' => 'Wkt Daftar',
            'bulan_daftar' => 'Bulan Daftar',
            'pembimbing_1' => 'Pembimbing  1',
            'pembimbing_2' => 'Pembimbing  2',
            'judul_ta' => 'Judul Ta',
            'linksyarat' => 'Linksyarat',
            'total_sks' => 'Total Sks',
            'ipk' => 'Ipk',
            'no_surat' => 'No Surat',
            'tgl_surat' => 'Tgl Surat',
            'thn' => 'Thn',
        ];
    }
}
