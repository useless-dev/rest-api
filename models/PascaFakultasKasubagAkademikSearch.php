<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PascaFakultasKasubagAkademik;

/**
 * PascaFakultasKasubagAkademikSearch represents the model behind the search form of `app\models\PascaFakultasKasubagAkademik`.
 */
class PascaFakultasKasubagAkademikSearch extends PascaFakultasKasubagAkademik
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['kdfakultas', 'tahun_akademik', 'jns_semester', 'kasubag', 'nip'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PascaFakultasKasubagAkademik::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'kdfakultas', $this->kdfakultas])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'kasubag', $this->kasubag])
            ->andFilterWhere(['like', 'nip', $this->nip]);

        return $dataProvider;
    }
}
