<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasca_emis_mhs_jenjang".
 *
 * @property int $kdmhsjenjang
 * @property string|null $tahun_akademik
 * @property string|null $jenjang
 * @property int|null $jml_laki
 * @property int|null $jml_perempuan
 * @property int|null $total
 */
class PascaEmisMhsJenjang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasca_emis_mhs_jenjang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jml_laki', 'jml_perempuan', 'total'], 'integer'],
            [['tahun_akademik', 'jenjang'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdmhsjenjang' => 'Kdmhsjenjang',
            'tahun_akademik' => 'Tahun Akademik',
            'jenjang' => 'Jenjang',
            'jml_laki' => 'Jml Laki',
            'jml_perempuan' => 'Jml Perempuan',
            'total' => 'Total',
        ];
    }
}
