<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa_beasiswa".
 *
 * @property int $kdprestasi
 * @property string|null $npm
 * @property int|null $validasi
 * @property int|null $semester
 * @property string $jns_prestasi
 * @property string $tahun
 * @property string $jns_semester
 * @property float|null $juara
 * @property string|null $foto
 * @property string|null $tgl
 * @property string|null $wkt
 */
class MahasiswaBeasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa_beasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['validasi', 'semester'], 'integer'],
            [['jns_prestasi', 'tahun', 'jns_semester'], 'required'],
            [['juara'], 'number'],
            [['tgl', 'wkt'], 'safe'],
            [['npm'], 'string', 'max' => 30],
            [['jns_prestasi', 'tahun', 'jns_semester'], 'string', 'max' => 20],
            [['foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdprestasi' => 'Kdprestasi',
            'npm' => 'Npm',
            'validasi' => 'Validasi',
            'semester' => 'Semester',
            'jns_prestasi' => 'Jns Prestasi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'juara' => 'Juara',
            'foto' => 'Foto',
            'tgl' => 'Tgl',
            'wkt' => 'Wkt',
        ];
    }
}
