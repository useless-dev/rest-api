<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_opak_akses".
 *
 * @property int $kdakses
 * @property string $mulai
 * @property string $selesai
 * @property string $tahun
 * @property string $periode
 * @property string $kuota
 */
class FormOpakAkses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_opak_akses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mulai', 'selesai', 'tahun', 'periode', 'kuota'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdakses' => 'Kdakses',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'kuota' => 'Kuota',
        ];
    }
}
