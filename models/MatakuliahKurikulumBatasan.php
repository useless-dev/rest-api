<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matakuliah_kurikulum_batasan".
 *
 * @property int $kdbatasan
 * @property string|null $kdprodi
 * @property string|null $tahun_aktif
 * @property string|null $kurikulum
 */
class MatakuliahKurikulumBatasan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matakuliah_kurikulum_batasan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tahun_aktif'], 'safe'],
            [['kdprodi', 'kurikulum'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdbatasan' => 'Kdbatasan',
            'kdprodi' => 'Kdprodi',
            'tahun_aktif' => 'Tahun Aktif',
            'kurikulum' => 'Kurikulum',
        ];
    }
}
