<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "option_pekerjaan".
 *
 * @property string $kdpekerjaan
 * @property string|null $no_urut
 * @property string $pekerjaan
 */
class OptionPekerjaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'option_pekerjaan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdpekerjaan'], 'required'],
            [['kdpekerjaan'], 'string', 'max' => 20],
            [['no_urut'], 'string', 'max' => 2],
            [['pekerjaan'], 'string', 'max' => 50],
            [['kdpekerjaan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdpekerjaan' => 'Kdpekerjaan',
            'no_urut' => 'No Urut',
            'pekerjaan' => 'Pekerjaan',
        ];
    }
}
