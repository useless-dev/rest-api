<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_kkn".
 *
 * @property int $id
 * @property string $npm
 * @property string $semester
 * @property string $kdprodi
 * @property string $tahun
 * @property string $periode
 * @property string $ukuran_jaket
 * @property string $tgl_daftar
 * @property string $status_nikah
 * @property string $nama_suami
 * @property string $kerja_suami
 * @property string $ket_sakit
 * @property string $alamat_kost
 * @property string $total_sks
 * @property string $ipk_sementara
 * @property string $nilai_akhir
 */
class FormKkn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_kkn';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar'], 'safe'],
            [['npm'], 'string', 'max' => 15],
            [['semester', 'nilai_akhir'], 'string', 'max' => 5],
            [['kdprodi'], 'string', 'max' => 20],
            [['tahun', 'ipk_sementara'], 'string', 'max' => 4],
            [['periode'], 'string', 'max' => 2],
            [['ukuran_jaket'], 'string', 'max' => 10],
            [['status_nikah'], 'string', 'max' => 25],
            [['nama_suami', 'kerja_suami', 'ket_sakit'], 'string', 'max' => 50],
            [['alamat_kost'], 'string', 'max' => 200],
            [['total_sks'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'ukuran_jaket' => 'Ukuran Jaket',
            'tgl_daftar' => 'Tgl Daftar',
            'status_nikah' => 'Status Nikah',
            'nama_suami' => 'Nama Suami',
            'kerja_suami' => 'Kerja Suami',
            'ket_sakit' => 'Ket Sakit',
            'alamat_kost' => 'Alamat Kost',
            'total_sks' => 'Total Sks',
            'ipk_sementara' => 'Ipk Sementara',
            'nilai_akhir' => 'Nilai Akhir',
        ];
    }
}
