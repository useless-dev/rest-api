<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_komputer".
 *
 * @property int $kdkomp
 * @property string $npm
 * @property string $semester
 * @property string $kdfakultas
 * @property string $kdprodi
 * @property string $tahun_akademik
 * @property string $jns_semester
 * @property string $tahun
 * @property string $periode
 * @property string $kelas
 * @property string $tgl_daftar
 * @property string $kddosen
 * @property string $n_word
 * @property string $n_excel
 * @property string $n_ppt
 * @property string $n_internet
 * @property string $n_akhir
 * @property string $predikat
 * @property string $no_sertifikat
 * @property string $no_seri
 * @property string $tgl_sertifikat
 * @property string $qr_seri
 * @property int|null $status_cetak
 * @property int|null $status_ambil
 */
class FormKomputer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_komputer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar'], 'safe'],
            [['status_cetak', 'status_ambil'], 'integer'],
            [['npm', 'kdfakultas', 'tahun', 'no_seri'], 'string', 'max' => 15],
            [['semester', 'periode', 'n_word', 'n_excel', 'n_ppt', 'n_internet', 'n_akhir'], 'string', 'max' => 5],
            [['kdprodi', 'tahun_akademik'], 'string', 'max' => 20],
            [['jns_semester', 'kelas', 'kddosen'], 'string', 'max' => 10],
            [['predikat'], 'string', 'max' => 30],
            [['no_sertifikat'], 'string', 'max' => 50],
            [['tgl_sertifikat'], 'string', 'max' => 40],
            [['qr_seri'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdkomp' => 'Kdkomp',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'kdfakultas' => 'Kdfakultas',
            'kdprodi' => 'Kdprodi',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'kelas' => 'Kelas',
            'tgl_daftar' => 'Tgl Daftar',
            'kddosen' => 'Kddosen',
            'n_word' => 'N Word',
            'n_excel' => 'N Excel',
            'n_ppt' => 'N Ppt',
            'n_internet' => 'N Internet',
            'n_akhir' => 'N Akhir',
            'predikat' => 'Predikat',
            'no_sertifikat' => 'No Sertifikat',
            'no_seri' => 'No Seri',
            'tgl_sertifikat' => 'Tgl Sertifikat',
            'qr_seri' => 'Qr Seri',
            'status_cetak' => 'Status Cetak',
            'status_ambil' => 'Status Ambil',
        ];
    }
}
