<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matakuliah_urutan".
 *
 * @property int $no
 * @property int $urutan
 * @property string $kdmk
 * @property string $semester
 * @property string|null $kdprodi
 * @property string|null $namamk
 * @property string|null $sks
 * @property string|null $kdfakultas
 */
class MatakuliahUrutan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matakuliah_urutan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urutan', 'semester'], 'required'],
            [['urutan'], 'integer'],
            [['kdmk'], 'string', 'max' => 30],
            [['semester', 'sks'], 'string', 'max' => 10],
            [['kdprodi'], 'string', 'max' => 20],
            [['namamk'], 'string', 'max' => 50],
            [['kdfakultas'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no' => 'No',
            'urutan' => 'Urutan',
            'kdmk' => 'Kdmk',
            'semester' => 'Semester',
            'kdprodi' => 'Kdprodi',
            'namamk' => 'Namamk',
            'sks' => 'Sks',
            'kdfakultas' => 'Kdfakultas',
        ];
    }
}
