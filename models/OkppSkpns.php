<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "okpp_skpns".
 *
 * @property int $id
 * @property string $kddosen
 * @property string $jns_skpns
 * @property string $fotosm1
 * @property string $linkdrive
 * @property string|null $petugas
 * @property string|null $tgl
 * @property string $jam
 * @property string|null $ip
 * @property string|null $deteksi
 */
class OkppSkpns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'okpp_skpns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jam'], 'safe'],
            [['kddosen'], 'string', 'max' => 5],
            [['jns_skpns'], 'string', 'max' => 50],
            [['fotosm1', 'linkdrive'], 'string', 'max' => 500],
            [['petugas', 'tgl', 'deteksi'], 'string', 'max' => 100],
            [['ip'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kddosen' => 'Kddosen',
            'jns_skpns' => 'Jns Skpns',
            'fotosm1' => 'Fotosm 1',
            'linkdrive' => 'Linkdrive',
            'petugas' => 'Petugas',
            'tgl' => 'Tgl',
            'jam' => 'Jam',
            'ip' => 'Ip',
            'deteksi' => 'Deteksi',
        ];
    }
}
