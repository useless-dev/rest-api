<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fakultas_kabag".
 *
 * @property int $idjur
 * @property string|null $kdfakultas
 * @property string|null $namajur
 * @property string|null $tahun_akademik
 * @property string|null $jns_semester
 * @property string|null $kabag
 * @property string|null $kasubag1
 * @property string|null $kasubag2
 * @property string|null $kasubag3
 */
class FakultasKabag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fakultas_kabag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdfakultas'], 'string', 'max' => 35],
            [['namajur'], 'string', 'max' => 255],
            [['tahun_akademik'], 'string', 'max' => 15],
            [['jns_semester', 'kabag', 'kasubag1', 'kasubag2', 'kasubag3'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idjur' => 'Idjur',
            'kdfakultas' => 'Kdfakultas',
            'namajur' => 'Namajur',
            'tahun_akademik' => 'Tahun Akademik',
            'jns_semester' => 'Jns Semester',
            'kabag' => 'Kabag',
            'kasubag1' => 'Kasubag 1',
            'kasubag2' => 'Kasubag 2',
            'kasubag3' => 'Kasubag 3',
        ];
    }
}
