<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormAlumni;

/**
 * FormAlumniSearch represents the model behind the search form of `app\models\FormAlumni`.
 */
class FormAlumniSearch extends FormAlumni
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdalumni'], 'integer'],
            [['npm', 'nama', 'kdjur', 'kdprodi', 'tmp_lahir', 'tgl_lahir', 'jenkel', 'status_perkawinan', 'alamat', 'notelp', 'tgl_lulus', 'ipk', 'predikat', 'judul_ta', 'krj_jenis', 'krj_nama', 'krj_jabatan', 'krj_lama', 'krj_alamat', 'krj_notelp', 'tgl_input', 'thn', 'periode'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormAlumni::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdalumni' => $this->kdalumni,
            'tgl_input' => $this->tgl_input,
            'thn' => $this->thn,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tmp_lahir', $this->tmp_lahir])
            ->andFilterWhere(['like', 'tgl_lahir', $this->tgl_lahir])
            ->andFilterWhere(['like', 'jenkel', $this->jenkel])
            ->andFilterWhere(['like', 'status_perkawinan', $this->status_perkawinan])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'notelp', $this->notelp])
            ->andFilterWhere(['like', 'tgl_lulus', $this->tgl_lulus])
            ->andFilterWhere(['like', 'ipk', $this->ipk])
            ->andFilterWhere(['like', 'predikat', $this->predikat])
            ->andFilterWhere(['like', 'judul_ta', $this->judul_ta])
            ->andFilterWhere(['like', 'krj_jenis', $this->krj_jenis])
            ->andFilterWhere(['like', 'krj_nama', $this->krj_nama])
            ->andFilterWhere(['like', 'krj_jabatan', $this->krj_jabatan])
            ->andFilterWhere(['like', 'krj_lama', $this->krj_lama])
            ->andFilterWhere(['like', 'krj_alamat', $this->krj_alamat])
            ->andFilterWhere(['like', 'krj_notelp', $this->krj_notelp])
            ->andFilterWhere(['like', 'periode', $this->periode]);

        return $dataProvider;
    }
}
