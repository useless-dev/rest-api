<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_pa".
 *
 * @property int $Idpa
 * @property string|null $npm
 * @property string|null $validasi
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $kddosen
 */
class FormPa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_pa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm'], 'string', 'max' => 11],
            [['validasi'], 'string', 'max' => 1],
            [['tahun'], 'string', 'max' => 9],
            [['jns_semester'], 'string', 'max' => 7],
            [['kddosen'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Idpa' => 'Idpa',
            'npm' => 'Npm',
            'validasi' => 'Validasi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'kddosen' => 'Kddosen',
        ];
    }
}
