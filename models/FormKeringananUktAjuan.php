<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_keringanan_ukt_ajuan".
 *
 * @property int $Id
 * @property string|null $alasan_ajuan_ukt
 */
class FormKeringananUktAjuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_keringanan_ukt_ajuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alasan_ajuan_ukt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'alasan_ajuan_ukt' => 'Alasan Ajuan Ukt',
        ];
    }
}
