<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormWorkshopAkses;

/**
 * FormWorkshopAksesSearch represents the model behind the search form of `app\models\FormWorkshopAkses`.
 */
class FormWorkshopAksesSearch extends FormWorkshopAkses
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdakses'], 'integer'],
            [['mulai', 'selesai', 'tahun', 'tahun_akademik', 'jns_semester', 'syarat_smt', 'kuota', 'nm_workshop'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormWorkshopAkses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdakses' => $this->kdakses,
            'mulai' => $this->mulai,
            'selesai' => $this->selesai,
        ]);

        $query->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'syarat_smt', $this->syarat_smt])
            ->andFilterWhere(['like', 'kuota', $this->kuota])
            ->andFilterWhere(['like', 'nm_workshop', $this->nm_workshop]);

        return $dataProvider;
    }
}
