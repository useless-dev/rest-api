<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MahasiswaAktifPertahun;

/**
 * MahasiswaAktifPertahunSearch represents the model behind the search form of `app\models\MahasiswaAktifPertahun`.
 */
class MahasiswaAktifPertahunSearch extends MahasiswaAktifPertahun
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdmhsstatuspertahun', 'semester', 'total_sks', 'total_nilai', 'kdfakultas'], 'integer'],
            [['npm', 'nama', 'jenkel', 'kdjur', 'kdprodi', 'tahun', 'jns_semester'], 'safe'],
            [['ips'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MahasiswaAktifPertahun::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdmhsstatuspertahun' => $this->kdmhsstatuspertahun,
            'semester' => $this->semester,
            'total_sks' => $this->total_sks,
            'total_nilai' => $this->total_nilai,
            'ips' => $this->ips,
            'kdfakultas' => $this->kdfakultas,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jenkel', $this->jenkel])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
