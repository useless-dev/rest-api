<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_alumni".
 *
 * @property int $kdalumni
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $tmp_lahir
 * @property string|null $tgl_lahir
 * @property string|null $jenkel
 * @property string|null $status_perkawinan
 * @property string|null $alamat
 * @property string|null $notelp
 * @property string|null $tgl_lulus
 * @property string|null $ipk
 * @property string|null $predikat
 * @property string|null $judul_ta
 * @property string|null $krj_jenis
 * @property string|null $krj_nama
 * @property string|null $krj_jabatan
 * @property string|null $krj_lama
 * @property string|null $krj_alamat
 * @property string|null $krj_notelp
 * @property string|null $tgl_input
 * @property string|null $thn
 * @property string|null $periode
 */
class FormAlumni extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_alumni';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_ta'], 'string'],
            [['tgl_input', 'thn'], 'safe'],
            [['npm', 'kdjur', 'kdprodi', 'jenkel', 'ipk'], 'string', 'max' => 20],
            [['nama', 'notelp', 'tgl_lulus', 'predikat'], 'string', 'max' => 35],
            [['tmp_lahir', 'alamat', 'krj_nama', 'krj_alamat', 'krj_notelp'], 'string', 'max' => 255],
            [['tgl_lahir', 'krj_jenis'], 'string', 'max' => 50],
            [['status_perkawinan'], 'string', 'max' => 30],
            [['krj_jabatan'], 'string', 'max' => 100],
            [['krj_lama'], 'string', 'max' => 10],
            [['periode'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdalumni' => 'Kdalumni',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'tmp_lahir' => 'Tmp Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'jenkel' => 'Jenkel',
            'status_perkawinan' => 'Status Perkawinan',
            'alamat' => 'Alamat',
            'notelp' => 'Notelp',
            'tgl_lulus' => 'Tgl Lulus',
            'ipk' => 'Ipk',
            'predikat' => 'Predikat',
            'judul_ta' => 'Judul Ta',
            'krj_jenis' => 'Krj Jenis',
            'krj_nama' => 'Krj Nama',
            'krj_jabatan' => 'Krj Jabatan',
            'krj_lama' => 'Krj Lama',
            'krj_alamat' => 'Krj Alamat',
            'krj_notelp' => 'Krj Notelp',
            'tgl_input' => 'Tgl Input',
            'thn' => 'Thn',
            'periode' => 'Periode',
        ];
    }
}
