<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MatakuliahKurikulumBatasan;

/**
 * MatakuliahKurikulumBatasanSearch represents the model behind the search form of `app\models\MatakuliahKurikulumBatasan`.
 */
class MatakuliahKurikulumBatasanSearch extends MatakuliahKurikulumBatasan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdbatasan'], 'integer'],
            [['kdprodi', 'tahun_aktif', 'kurikulum'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatakuliahKurikulumBatasan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdbatasan' => $this->kdbatasan,
            'tahun_aktif' => $this->tahun_aktif,
        ]);

        $query->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'kurikulum', $this->kurikulum]);

        return $dataProvider;
    }
}
