<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KrsIpk;

/**
 * KrsIpkSearch represents the model behind the search form of `app\models\KrsIpk`.
 */
class KrsIpkSearch extends KrsIpk
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idipk', 'semester_ini', 'jml_sks', 'jml_nilai'], 'integer'],
            [['npm', 'nama', 'kdprodi', 'tahun_akademik', 'jns_semester'], 'safe'],
            [['ipk'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KrsIpk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idipk' => $this->idipk,
            'semester_ini' => $this->semester_ini,
            'jml_sks' => $this->jml_sks,
            'jml_nilai' => $this->jml_nilai,
            'ipk' => $this->ipk,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun_akademik', $this->tahun_akademik])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
