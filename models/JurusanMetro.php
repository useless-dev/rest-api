<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jurusan_metro".
 *
 * @property string $kdprodi
 * @property string|null $kdprodi_pdpt
 * @property int $idprodi
 * @property string|null $kdjur
 * @property string|null $namaprodi
 * @property string|null $jenjang
 * @property string|null $namajenjang
 * @property string|null $kode_jenjang
 * @property int|null $kdfakultas
 * @property string|null $banpt
 */
class JurusanMetro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jurusan_metro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdfakultas'], 'integer'],
            [['kdprodi'], 'string', 'max' => 35],
            [['kdprodi_pdpt'], 'string', 'max' => 15],
            [['kdjur'], 'string', 'max' => 11],
            [['namaprodi'], 'string', 'max' => 100],
            [['jenjang', 'kode_jenjang'], 'string', 'max' => 20],
            [['namajenjang', 'banpt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdprodi' => 'Kdprodi',
            'kdprodi_pdpt' => 'Kdprodi Pdpt',
            'idprodi' => 'Idprodi',
            'kdjur' => 'Kdjur',
            'namaprodi' => 'Namaprodi',
            'jenjang' => 'Jenjang',
            'namajenjang' => 'Namajenjang',
            'kode_jenjang' => 'Kode Jenjang',
            'kdfakultas' => 'Kdfakultas',
            'banpt' => 'Banpt',
        ];
    }
}
