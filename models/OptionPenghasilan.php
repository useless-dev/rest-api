<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "option_penghasilan".
 *
 * @property string $kdpenghasilan
 * @property string|null $penghasilan
 */
class OptionPenghasilan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'option_penghasilan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdpenghasilan'], 'required'],
            [['kdpenghasilan'], 'string', 'max' => 20],
            [['penghasilan'], 'string', 'max' => 30],
            [['kdpenghasilan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdpenghasilan' => 'Kdpenghasilan',
            'penghasilan' => 'Penghasilan',
        ];
    }
}
