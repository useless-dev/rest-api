<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InfLokasi;

/**
 * InfLokasiSearch represents the model behind the search form of `app\models\InfLokasi`.
 */
class InfLokasiSearch extends InfLokasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lokasi_ID', 'lokasi_propinsi', 'lokasi_kabupatenkota', 'lokasi_kecamatan', 'lokasi_kelurahan'], 'integer'],
            [['lokasi_kode', 'lokasi_nama'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InfLokasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lokasi_ID' => $this->lokasi_ID,
            'lokasi_propinsi' => $this->lokasi_propinsi,
            'lokasi_kabupatenkota' => $this->lokasi_kabupatenkota,
            'lokasi_kecamatan' => $this->lokasi_kecamatan,
            'lokasi_kelurahan' => $this->lokasi_kelurahan,
        ]);

        $query->andFilterWhere(['like', 'lokasi_kode', $this->lokasi_kode])
            ->andFilterWhere(['like', 'lokasi_nama', $this->lokasi_nama]);

        return $dataProvider;
    }
}
