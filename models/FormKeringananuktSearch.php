<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormKeringananukt;

/**
 * FormKeringananuktSearch represents the model behind the search form of `app\models\FormKeringananukt`.
 */
class FormKeringananuktSearch extends FormKeringananukt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['npm', 'semester', 'jml_ukt', 'kdjur', 'kdprodi', 'tahun', 'jns_semester', 'tgl_daftar', 'wkt_daftar', 'bulan_daftar', 'alasan_1', 'ayah_1', 'ibu_2', 'beasiswa_1', 'judul_ta', 'fotosm1', 'linkskripsi', 'ukt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKeringananukt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_daftar' => $this->tgl_daftar,
            'wkt_daftar' => $this->wkt_daftar,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'semester', $this->semester])
            ->andFilterWhere(['like', 'jml_ukt', $this->jml_ukt])
            ->andFilterWhere(['like', 'kdjur', $this->kdjur])
            ->andFilterWhere(['like', 'kdprodi', $this->kdprodi])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester])
            ->andFilterWhere(['like', 'bulan_daftar', $this->bulan_daftar])
            ->andFilterWhere(['like', 'alasan_1', $this->alasan_1])
            ->andFilterWhere(['like', 'ayah_1', $this->ayah_1])
            ->andFilterWhere(['like', 'ibu_2', $this->ibu_2])
            ->andFilterWhere(['like', 'beasiswa_1', $this->beasiswa_1])
            ->andFilterWhere(['like', 'judul_ta', $this->judul_ta])
            ->andFilterWhere(['like', 'fotosm1', $this->fotosm1])
            ->andFilterWhere(['like', 'linkskripsi', $this->linkskripsi])
            ->andFilterWhere(['like', 'ukt', $this->ukt]);

        return $dataProvider;
    }
}
