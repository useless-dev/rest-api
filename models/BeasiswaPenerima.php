<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "beasiswa_penerima".
 *
 * @property int $kdpenerima
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $kdbeasiswa
 * @property string|null $prodi
 * @property string|null $semester
 * @property string|null $tahun
 * @property string|null $no_sk
 * @property string|null $tgl_sk
 */
class BeasiswaPenerima extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beasiswa_penerima';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npm', 'nama', 'prodi', 'semester', 'no_sk', 'tgl_sk'], 'string', 'max' => 30],
            [['kdbeasiswa'], 'string', 'max' => 2],
            [['tahun'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdpenerima' => 'Kdpenerima',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'kdbeasiswa' => 'Kdbeasiswa',
            'prodi' => 'Prodi',
            'semester' => 'Semester',
            'tahun' => 'Tahun',
            'no_sk' => 'No Sk',
            'tgl_sk' => 'Tgl Sk',
        ];
    }
}
