<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KrsAkses;

/**
 * KrsAksesSearch represents the model behind the search form of `app\models\KrsAkses`.
 */
class KrsAksesSearch extends KrsAkses
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdkrsakses', 'kdakademik'], 'integer'],
            [['npm', 'status', 'tgl', 'wkt', 'tahun', 'jns_semester'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KrsAkses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdkrsakses' => $this->kdkrsakses,
            'tgl' => $this->tgl,
            'wkt' => $this->wkt,
            'kdakademik' => $this->kdakademik,
        ]);

        $query->andFilterWhere(['like', 'npm', $this->npm])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'jns_semester', $this->jns_semester]);

        return $dataProvider;
    }
}
