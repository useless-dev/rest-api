<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_ppl".
 *
 * @property int $id
 * @property string $npm
 * @property string $semester
 * @property string $kdjur
 * @property string $kdprodi
 * @property string $tahun
 * @property string $jns_semester
 * @property string $total_sks
 * @property string $ipk
 * @property string $predikat
 * @property string $tgl_daftar
 * @property string $wkt_daftar
 * @property string $kddosen
 * @property int|null $nilai_1
 * @property int|null $nilai_2
 * @property int|null $nilai_3
 * @property int|null $nilai_akhir
 * @property string $no_sertifikat
 */
class FormPpl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_ppl';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar', 'wkt_daftar'], 'safe'],
            [['nilai_1', 'nilai_2', 'nilai_3', 'nilai_akhir'], 'integer'],
            [['npm'], 'string', 'max' => 15],
            [['semester'], 'string', 'max' => 5],
            [['kdjur', 'jns_semester', 'total_sks', 'ipk'], 'string', 'max' => 10],
            [['kdprodi', 'tahun', 'predikat'], 'string', 'max' => 20],
            [['kddosen'], 'string', 'max' => 4],
            [['no_sertifikat'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'total_sks' => 'Total Sks',
            'ipk' => 'Ipk',
            'predikat' => 'Predikat',
            'tgl_daftar' => 'Tgl Daftar',
            'wkt_daftar' => 'Wkt Daftar',
            'kddosen' => 'Kddosen',
            'nilai_1' => 'Nilai  1',
            'nilai_2' => 'Nilai  2',
            'nilai_3' => 'Nilai  3',
            'nilai_akhir' => 'Nilai Akhir',
            'no_sertifikat' => 'No Sertifikat',
        ];
    }
}
