<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PascaBeasiswaInformasi;

/**
 * PascaBeasiswaInformasiSearch represents the model behind the search form of `app\models\PascaBeasiswaInformasi`.
 */
class PascaBeasiswaInformasiSearch extends PascaBeasiswaInformasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kdinformasi', 'kdbeasiswa', 'kdakademik'], 'integer'],
            [['judul', 'isi', 'tgl', 'wkt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PascaBeasiswaInformasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kdinformasi' => $this->kdinformasi,
            'kdbeasiswa' => $this->kdbeasiswa,
            'kdakademik' => $this->kdakademik,
            'tgl' => $this->tgl,
            'wkt' => $this->wkt,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'isi', $this->isi]);

        return $dataProvider;
    }
}
