<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_ujikomp".
 *
 * @property int $id
 * @property string $npm
 * @property string $semester
 * @property string $kdprodi
 * @property string $tahun
 * @property string $periode
 * @property string $kelas
 * @property string $tgl_daftar
 * @property string $alasan
 * @property string $n_word
 * @property string $n_excel
 * @property string $n_ppt
 * @property string $n_akhir
 * @property string $no_sertifikat
 * @property string $no_seri
 * @property string $tgl_sertifikat
 * @property string $qr_seri
 */
class FormUjikomp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_ujikomp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_daftar'], 'safe'],
            [['npm', 'no_seri'], 'string', 'max' => 15],
            [['semester'], 'string', 'max' => 5],
            [['kdprodi', 'tahun'], 'string', 'max' => 20],
            [['periode', 'kelas'], 'string', 'max' => 10],
            [['alasan'], 'string', 'max' => 30],
            [['n_word', 'n_excel', 'n_ppt', 'n_akhir'], 'string', 'max' => 4],
            [['no_sertifikat'], 'string', 'max' => 50],
            [['tgl_sertifikat'], 'string', 'max' => 40],
            [['qr_seri'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'npm' => 'Npm',
            'semester' => 'Semester',
            'kdprodi' => 'Kdprodi',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'kelas' => 'Kelas',
            'tgl_daftar' => 'Tgl Daftar',
            'alasan' => 'Alasan',
            'n_word' => 'N Word',
            'n_excel' => 'N Excel',
            'n_ppt' => 'N Ppt',
            'n_akhir' => 'N Akhir',
            'no_sertifikat' => 'No Sertifikat',
            'no_seri' => 'No Seri',
            'tgl_sertifikat' => 'Tgl Sertifikat',
            'qr_seri' => 'Qr Seri',
        ];
    }
}
