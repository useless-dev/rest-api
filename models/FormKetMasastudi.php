<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_ket_masastudi".
 *
 * @property int $kdketmasastudi
 * @property string|null $jns_ket_masastudi
 * @property string|null $untuk_keperluan
 * @property string|null $npm
 * @property string|null $nama
 * @property string|null $smt
 * @property string|null $kdjur
 * @property string|null $kdprodi
 * @property string|null $no_surat
 * @property string|null $tgl_surat
 * @property string|null $tgl_daftar
 * @property string|null $tahun
 * @property string|null $jns_semester
 * @property string|null $thn
 * @property string|null $nama_pejabat
 * @property string|null $nip_pejabat
 * @property string|null $pangkat_gol_pejabat
 * @property string|null $smt_1
 * @property string|null $tgl_mulai
 * @property string|null $tgl_berakhir
 */
class FormKetMasastudi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_ket_masastudi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_surat', 'tgl_daftar', 'thn', 'tgl_mulai', 'tgl_berakhir'], 'safe'],
            [['jns_ket_masastudi'], 'string', 'max' => 10],
            [['untuk_keperluan'], 'string', 'max' => 50],
            [['npm', 'kdjur', 'kdprodi', 'no_surat', 'nip_pejabat'], 'string', 'max' => 20],
            [['nama', 'smt', 'smt_1'], 'string', 'max' => 35],
            [['tahun'], 'string', 'max' => 9],
            [['jns_semester'], 'string', 'max' => 6],
            [['nama_pejabat'], 'string', 'max' => 30],
            [['pangkat_gol_pejabat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kdketmasastudi' => 'Kdketmasastudi',
            'jns_ket_masastudi' => 'Jns Ket Masastudi',
            'untuk_keperluan' => 'Untuk Keperluan',
            'npm' => 'Npm',
            'nama' => 'Nama',
            'smt' => 'Smt',
            'kdjur' => 'Kdjur',
            'kdprodi' => 'Kdprodi',
            'no_surat' => 'No Surat',
            'tgl_surat' => 'Tgl Surat',
            'tgl_daftar' => 'Tgl Daftar',
            'tahun' => 'Tahun',
            'jns_semester' => 'Jns Semester',
            'thn' => 'Thn',
            'nama_pejabat' => 'Nama Pejabat',
            'nip_pejabat' => 'Nip Pejabat',
            'pangkat_gol_pejabat' => 'Pangkat Gol Pejabat',
            'smt_1' => 'Smt  1',
            'tgl_mulai' => 'Tgl Mulai',
            'tgl_berakhir' => 'Tgl Berakhir',
        ];
    }
}
