<?php

use yii\db\Migration;

class m220627_160206_create_table_admin extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%admin}}',
            [
                'no' => $this->primaryKey(),
                'username' => $this->string(15)->notNull()->defaultValue(''),
                'password' => $this->string(50)->notNull()->defaultValue(''),
                'sessions' => $this->string(50)->notNull()->defaultValue(''),
                'level' => $this->string(5),
                'nama' => $this->string(35),
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%admin}}');
    }
}
