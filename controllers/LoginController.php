<?php 
	namespace app\controllers;

	use yii\rest\ActiveController;
	use yii\filters\auth\CompositeAuth;
	use yii\filters\auth\HttpBasicAuth;
	use yii\filters\auth\HttpBearerAuth;
	use yii\filters\auth\QueryParamAuth;

	class LoginController extends ActiveController
	{
	    public $modelClass = 'app\models\TblLogin';
	    public $serializer = [
	        'class' => 'yii\rest\Serializer',
	        'collectionEnvelope' => 'items',
	    ];

	    public function actions(){
	        $actions = parent::actions();
	        // unset($actions['create']);
	        // unset($actions['update']);
	        // unset($actions['delete']);
	        // unset($actions['view']);
	        // unset($actions['index']);
	        return $actions;
	    }

	    protected function verbs(){
	        return [
	            'create' => ['POST'],
	            'update' => ['PUT', 'PATCH','POST'],
	            'delete' => ['DELETE'],
	            'view' => ['GET'],
	            'index'=>['GET'],
	        ];
	    }

	    public function actionIndex()
	    {
	    	return ['baso'];
	    }

	    public function behaviors() {
	    	$behaviors = parent::behaviors();
	    	$behaviors['authenticator'] = [
		        'class' => CompositeAuth::class,
		        'authMethods' => [
		            HttpBasicAuth::class,
		            HttpBearerAuth::class,
		            QueryParamAuth::class,
		        ],
		    ];
	     // 	$behaviors['authenticator']['except'] = ['options'];
	    	$behaviors['corsFilter'] = [
	        	'class' => \yii\filters\Cors::className(),
	        ];
	    	return $behaviors;
    	}


	}
?>