<?php 
	namespace app\controllers;

	use Yii;
	use yii\web\HttpException;
	use yii\web\NotFoundHttpException;
	use yii\web\BadRequestHttpException;
	use yii\rest\ActiveController;
	use yii\filters\auth\CompositeAuth;
	use yii\filters\auth\HttpBasicAuth;
	use yii\filters\auth\HttpBearerAuth;
	use yii\filters\auth\QueryParamAuth;
	use app\models\Member;

	class MemberController extends ActiveController
	{
	    public $modelClass = 'app\models\Member';
	    public $serializer = [
	        'class' => 'yii\rest\Serializer',
	        'collectionEnvelope' => 'items',
	    ];

	    protected function verbs(){
	        return [
	            'create' => ['POST'],
	            'update' => ['PUT', 'PATCH','POST'],
	            'delete' => ['DELETE'],
	            'view' => ['GET'],
	            'index'=>['GET'],
	            'search'=>['POST'],
	        ];
	    }

	    public function behaviors() {
	    	$behaviors = parent::behaviors();
	    	$behaviors['authenticator'] = [
		        'class' => CompositeAuth::class,
		        'authMethods' => [
		            HttpBasicAuth::class,
		            HttpBearerAuth::class,
		            QueryParamAuth::class,
		        ],
		    ];
	    	$behaviors['corsFilter'] = [
	        	'class' => \yii\filters\Cors::className(),
	        ];
	    	return $behaviors;
    	}

    	public function actionSearch()
    	{
    		$post = Yii::$app->request->post();

    		if(!isset($post) || !isset($post['username'])){
    			throw new BadRequestHttpException("Request Tidak Sesuai");
    		}
    		else{
	    		$member = Member::findOne(['username'=>$post['username']]);
	    		if($member)
	    			return [
	    				'status'=>200,
	    				'data'=>$member
	    			];
	    		else
    				throw new NotFoundHttpException("User Tidak Ditemukan");

    		}
    	}

	}
?>