<?php

namespace app\controllers;

use app\models\Krs;
use app\models\KrsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use Yii;
/**
 * KrsController implements the CRUD actions for Krs model.
 */
class KrsController extends ActiveController
{
    /**
     * @inheritDoc
     */
    public $modelClass = 'app\models\Krs';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions(){
        $actions = parent::actions();
        // unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        // unset($actions['view']);
        // unset($actions['index']);
        $actions['index']['dataFilter'] = [
            'class' => \yii\data\ActiveDataFilter::class,
            // 'attributeMap' => [],
            'searchModel' => (new KrsSearch)
        ];

        return $actions;
    }

    protected function verbs(){
        return [
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH','POST'],
            'delete' => ['DELETE'],
            'view' => ['GET'],
            'index'=>['GET'],
        ];
    }
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
        ];
     //     $behaviors['authenticator']['except'] = ['options'];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        return $behaviors;
    }

    public function actionSksDitempuh($npm,$semester)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            Select
            SUM(sks) as ksk
            from krs where npm = '$npm' and semester = $semester
            ");
        $result = $command->queryOne();
        return $result;
    }


    public function actionRekapSks($npm,$semester)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            Select
            SUM(sks) as ksk
            from krs where npm = '$npm' and semester = $semester
            and keterangan != 'Perbaikan'
            ");
        $result = $command->queryOne();
        return $result;
    }
}
