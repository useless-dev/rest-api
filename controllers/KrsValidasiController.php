<?php 
	namespace app\controllers;

	use yii\rest\ActiveController;
	use yii\filters\auth\CompositeAuth;
	use yii\filters\auth\HttpBasicAuth;
	use yii\filters\auth\HttpBearerAuth;
	use yii\filters\auth\QueryParamAuth;
	use app\models\KrsValidasi;

	class KrsValidasiController extends ActiveController
	{
	    public $modelClass = 'app\models\KrsValidasi';
	    public $serializer = [
	        'class' => 'yii\rest\Serializer',
	        'collectionEnvelope' => 'items',
	    ];

	    public function __construct($id, $module, $config = []){

	        // $this->classSearch = 'app\models\KrsValidasiSearch';

	        parent::__construct($id, $module, $config);

	    }
	    public function actions(){
	        $actions = parent::actions();
	        // unset($actions['create']);
	        // unset($actions['update']);
	        // unset($actions['delete']);
	        // unset($actions['view']);
	        // unset($actions['index']);
	        $actions['index']['dataFilter'] = [
	        	'class' => \yii\data\ActiveDataFilter::class,
	        	// 'attributeMap' => [],
	        	'searchModel' => (new \app\models\KrsValidasiSearch)
	        ];

	        return $actions;
	    }

	    protected function verbs(){
	        return [
	            'create' => ['POST'],
	            'update' => ['PUT', 'PATCH','POST'],
	            'delete' => ['DELETE'],
	            'view' => ['GET'],
	            'index'=>['GET'],
	        ];
	    }

	    public function behaviors() {
	    	$behaviors = parent::behaviors();
	    	$behaviors['authenticator'] = [
		        'class' => CompositeAuth::class,
		        'authMethods' => [
		            HttpBasicAuth::class,
		            HttpBearerAuth::class,
		            QueryParamAuth::class,
		        ],
		    ];
	     // 	$behaviors['authenticator']['except'] = ['options'];
	    	$behaviors['corsFilter'] = [
	        	'class' => \yii\filters\Cors::className(),
	        ];
	    	return $behaviors;
    	}

    }