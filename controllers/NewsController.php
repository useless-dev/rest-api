<?php 
	namespace app\controllers;

	use Yii;
	use yii\web\HttpException;
	use yii\web\NotFoundHttpException;
	use yii\web\BadRequestHttpException;
	use yii\rest\ActiveController;
	use yii\filters\auth\CompositeAuth;
	use yii\filters\auth\HttpBasicAuth;
	use yii\filters\auth\HttpBearerAuth;
	use yii\filters\auth\QueryParamAuth;
	use app\models\News;
	use app\models\NewsSearch;

	class NewsController extends ActiveController
	{
	    public $modelClass = 'app\models\News';
	    public $serializer = [
	        'class' => 'yii\rest\Serializer',
	        'collectionEnvelope' => 'items',
	    ];

	    protected function verbs(){
	        return [
	            'create' => ['POST'],
	            'update' => ['PUT', 'PATCH','POST'],
	            'delete' => ['DELETE'],
	            'view' => ['GET'],
	            'index'=>['GET'],
	            'search'=>['POST'],
	        ];
	    }

	    public function behaviors() {
	    	$behaviors = parent::behaviors();
	    	$behaviors['authenticator'] = [
		        'class' => CompositeAuth::class,
		        'authMethods' => [
		            HttpBasicAuth::class,
		            HttpBearerAuth::class,
		            QueryParamAuth::class,
		        ],
		    ];
	    	$behaviors['corsFilter'] = [
	        	'class' => \yii\filters\Cors::className(),
	        ];
	    	return $behaviors;
    	}

		public function actions()
		{
		    $actions = parent::actions();

		    // disable the "delete" and "create" actions
		    unset($actions['delete'], $actions['create'], $actions['update']);

		    // customize the data provider preparation with the "prepareDataProvider()" method
		    $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

		    return $actions;
		}

		public function prepareDataProvider()
		{
			$searchModel = new NewsSearch;
        	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        	return $dataProvider;
		}

	}
?>