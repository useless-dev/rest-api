<?php

namespace app\controllers;

use app\models\Nilai;
use app\models\NilaiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use Yii;

/**
 * NilaiController implements the CRUD actions for Nilai model.
 */
class NilaiController extends ActiveController
{
    /**
     * @inheritDoc
     */
    public $modelClass = 'app\models\Nilai';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions(){
        $actions = parent::actions();
        // unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        // unset($actions['view']);
        // unset($actions['index']);
        $actions['index']['dataFilter'] = [
            'class' => \yii\data\ActiveDataFilter::class,
            // 'attributeMap' => [],
            'searchModel' => (new NilaiSearch)
        ];

        return $actions;
    }

    protected function verbs(){
        return [
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH','POST'],
            'delete' => ['DELETE'],
            'view' => ['GET'],
            'index'=>['GET'],
        ];
    }
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
        ];
     //     $behaviors['authenticator']['except'] = ['options'];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        return $behaviors;
    }

    public function actionRekapSks($npm,$semester)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            Select
            SUM(IF(angka < 1,sks,0)) AS sks_gagal,
            SUM(IF(angka >= 1,sks,0)) AS sks_lulus,
            SUM(IF(keterangan != 'Perbaikan',jumlah,0)) AS ip
            from nilai where npm = '$npm' and semester = $semester
            ");
        $result = $command->queryOne();
        return $result;
    }

    public function actionRekapNilai($npm,$semester)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT SUM(sks) AS ksk,SUM(angka) AS ak,SUM(jumlah) AS jml 
            FROM nilai 
            WHERE semester='$semester' AND npm='$npm' ORDER BY jumlah DESC
            ");
        $result = $command->queryOne();
        return $result;
    }


    public function actionKrsSementara($npm,$kdprodi)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
           SET SESSION sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))
            ");
        $result = $command->queryAll();
        $command = $connection->createCommand("
            SELECT MIN(huruf) AS huruf, MAX(nilai.angka) AS angka, MAX(nilai.jumlah) AS jumlah, nilai.kdmk, nilai.sks, matakuliah.namamk FROM nilai 
                LEFT JOIN matakuliah on matakuliah.kdmk = nilai.kdmk and matakuliah.kdprodi = '$kdprodi'
                WHERE npm='$npm' GROUP BY kdmk ORDER BY nilai.kdmk,nilai.semester ASC
            ");
        $result = $command->queryAll();
        return $result;
    }
        
}
