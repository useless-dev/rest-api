<?php 
	namespace app\controllers;

	use yii\rest\ActiveController;
	use yii\filters\auth\CompositeAuth;
	use yii\filters\auth\HttpBasicAuth;
	use yii\filters\auth\HttpBearerAuth;
	use yii\filters\auth\QueryParamAuth;	
	use yii\data\ActiveDataProvider;
	use yii\data\ActiveDataFilter;
	use Yii;

	class ApiController extends ActiveController{

		public $classSearch;

	}
